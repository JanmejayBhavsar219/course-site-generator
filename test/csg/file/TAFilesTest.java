/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.file;

import static com.sun.org.apache.bcel.internal.util.SecuritySupport.getResourceAsStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
/**
 *
 * @author jay
 */
public class TAFilesTest {
    static final String JSON_COURSE_INFO = "course_info";
    static final String JSON_SUBJECT = "subject";
    static final String JSON_NUMBER = "number";
    static final String JSON_SEMESTER = "semester";
    static final String JSON_YEAR = "year";
    static final String JSON_COURSE_TITLE = "title";
    static final String JSON_INSTRUCTOR_NAME = "instructor_name";
    static final String JSON_INSTRUCTOR_HOME = "instructor_home";
    static final String JSON_EXPORT_DIRECTORY = "export_directory";
    static final String JSON_SITE_TEMPLATE = "site_template";
    static final String JSON_TEMPLATE_DIRECTORY = "template_directory";
    static final String JSON_SITE_PAGES = "site_pages";
    static final String JSON_USE = "use";
    static final String JSON_NAVBAR_TITLE = "navbar_title";
    static final String JSON_FILE_NAME = "file_name";
    static final String JSON_SCRIPT = "script";
    static final String JSON_PAGE_STYLE = "page_style";
    static final String JSON_BANNER_SCHOOL_IMAGE = "banner_school_image_path";
    static final String JSON_LEFT_FOOTER_IMAGE = "left_footer_image";
    static final String JSON_RIGHT_FOOTER_IMAGE = "right_footer";
    static final String JSON_STYLESHEET = "stylesheet";
    static final String JSON_START_HOUR = "startHour";
    static final String JSON_END_HOUR = "endHour";
    static final String JSON_OFFICE_HOURS = "officeHours";
    static final String JSON_DAY = "day";
    static final String JSON_TIME = "time";
    static final String JSON_NAME = "name";
    static final String JSON_UNDERGRAD_TAS = "undergrad_tas";
    static final String JSON_EMAIL = "email";
    static final String JSON_SECTION = "section";
    static final String JSON_INSTRUCTOR = "instructor";
    static final String JSON_DAY_TIME = "day_time";
    static final String JSON_LOCATION = "location";
    static final String JSON_TA1 = "ta_1";
    static final String JSON_TA2 = "ta_2";
    static final String JSON_RECITATIONS = "recitations";
    static final String JSON_MONDAY = "monday";
    static final String JSON_FRIDAY = "friday";
    static final String JSON_TYPE = "type";
    static final String JSON_DATE = "date";
    static final String JSON_TITLE = "title";
    static final String JSON_TOPIC = "topic";
    static final String JSON_SCHEDULES = "schedules";
    static final String JSON_TEAM_NAME = "team_name";
    static final String JSON_COLOR = "color";
    static final String JSON_TEXT_COLOR = "text_color";
    static final String JSON_LINK = "link";
    static final String JSON_FIRST_NAME = "first_name";
    static final String JSON_LAST_NAME = "last_name";
    static final String JSON_TEAM = "team";
    static final String JSON_ROLE = "role";
    static final String JSON_TEAMS = "teams";
    static final String JSON_STUDENTS = "students";
    
    public TAFilesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of loadData method, of class TAFiles.
     */
    @Test
    public void testLoadData() throws Exception {
        System.out.println("Test loadData");
        
        String fileName = "/Users/jay/Desktop/Jay Work/CSE 219 Homework 5/CourseSiteGeneratorApp/work/SiteSaveTest.json";
        //System.out.println(new File(fileName).getCanonicalPath());
        JsonObject jsonObject = loadJSONFile(fileName);
        String subject = jsonObject.getString(JSON_SUBJECT);
        assertEquals("CSE", subject);
        String number = jsonObject.getString(JSON_NUMBER);
        assertEquals("219", number);
        String semester = jsonObject.getString(JSON_SEMESTER);
        assertEquals("Spring", semester);
        String year = jsonObject.getString(JSON_YEAR);
        assertEquals("2017", year);
        String title = jsonObject.getString(JSON_COURSE_TITLE);
        assertEquals("Computer Science III", title);
        String instructorName = jsonObject.getString(JSON_INSTRUCTOR_NAME);
        assertEquals("Richard McKenna", instructorName);
        String instructorHome = jsonObject.getString(JSON_INSTRUCTOR_HOME);
        assertEquals("http://www.cs.stonybrook.edu/~richard", instructorHome);
        String exportDirectory = jsonObject.getString(JSON_EXPORT_DIRECTORY);
        assertEquals("..\\..\\..\\Courses\\CSE219\\Summer2017\\public", exportDirectory);
        String templateDirectory = jsonObject.getString(JSON_TEMPLATE_DIRECTORY);
        assertEquals(".\\templates\\CSE219", templateDirectory);
        Object sitePagesObject = jsonObject.get(JSON_SITE_PAGES);
        JsonArray sitePagesArray = (JsonArray) sitePagesObject;
        Assert.assertTrue(sitePagesObject instanceof JsonArray);
        JsonObject sitePage1 = sitePagesArray.getJsonObject(0);
        //System.out.println(sitePage1);
        assertEquals("Home", sitePage1.getString(JSON_NAVBAR_TITLE));
        assertEquals("index.html", sitePage1.getString(JSON_FILE_NAME));
        assertEquals("HomeBuilder.js", sitePage1.getString(JSON_SCRIPT));
        JsonObject sitePage2 = sitePagesArray.getJsonObject(1);
        assertEquals("Syllabus", sitePage2.getString(JSON_NAVBAR_TITLE));
        assertEquals("syllabus.html", sitePage2.getString(JSON_FILE_NAME));
        assertEquals("SyllabusBuilder.js", sitePage2.getString(JSON_SCRIPT));
        JsonObject sitePage3 = sitePagesArray.getJsonObject(2);
        assertEquals("Schedule", sitePage3.getString(JSON_NAVBAR_TITLE));
        assertEquals("schedule.html", sitePage3.getString(JSON_FILE_NAME));
        assertEquals("ScheduleBuilder.js", sitePage3.getString(JSON_SCRIPT));
        JsonObject sitePage4 = sitePagesArray.getJsonObject(3);
        assertEquals("HWs", sitePage4.getString(JSON_NAVBAR_TITLE));
        assertEquals("hws.html", sitePage4.getString(JSON_FILE_NAME));
        assertEquals("HWsBuilder.js", sitePage4.getString(JSON_SCRIPT));
        JsonObject sitePage5 = sitePagesArray.getJsonObject(4);
        assertEquals("Projects", sitePage5.getString(JSON_NAVBAR_TITLE));
        assertEquals("projects.html", sitePage5.getString(JSON_FILE_NAME));
        assertEquals("ProjectsBuilder.js", sitePage5.getString(JSON_SCRIPT));
        String bannerSchoolImage = jsonObject.getString(JSON_BANNER_SCHOOL_IMAGE);
        assertEquals("http://www3.cs.stonybrook.edu/~cse219/Section02/images/SBUDarkRedShieldLogo.png", bannerSchoolImage);
        String leftFooterImage = jsonObject.getString(JSON_LEFT_FOOTER_IMAGE);
        assertEquals("http://www3.cs.stonybrook.edu/~cse219/Section02/images/SBUWhiteShieldLogo.jpg", leftFooterImage);
        String rightFooterImage = jsonObject.getString(JSON_RIGHT_FOOTER_IMAGE);
        assertEquals("http://www3.cs.stonybrook.edu/~cse219/Section02/images/CSLogo.png", rightFooterImage);
        String stylesheet = jsonObject.getString(JSON_STYLESHEET);
        assertEquals("sea_wolf.css", stylesheet);
        System.out.println("Course Details Loaded Properly");
        String startHour = jsonObject.getString(JSON_START_HOUR);
        assertEquals("9", startHour);
        String endHour = jsonObject.getString(JSON_END_HOUR);
        assertEquals("20", endHour);
        Object undergradTAObject = jsonObject.get(JSON_UNDERGRAD_TAS);
        JsonArray undergradTAArray = (JsonArray) undergradTAObject;
        Assert.assertTrue(undergradTAObject instanceof JsonArray);
        JsonObject TA1 = undergradTAArray.getJsonObject(0);
        assertEquals("Dhruv Bhawsar", TA1.getString(JSON_NAME));
        assertEquals("dhmi.bhch@gmail.com", TA1.getString(JSON_EMAIL));
        JsonObject TA2 = undergradTAArray.getJsonObject(1);
        assertEquals("Janmejay Bhavsar", TA2.getString(JSON_NAME));
        assertEquals("janmejay.bhaw@gmail.com", TA2.getString(JSON_EMAIL));
        JsonObject TA3 = undergradTAArray.getJsonObject(2);
        assertEquals("Joe Shmo", TA3.getString(JSON_NAME));
        assertEquals("joe.shmo@yale.edu", TA3.getString(JSON_EMAIL));
        JsonObject TA4 = undergradTAArray.getJsonObject(3);
        assertEquals("John Doe", TA4.getString(JSON_NAME));
        assertEquals("john.doe@yahoo.com", TA4.getString(JSON_EMAIL));
        Object officeHoursObject = jsonObject.get(JSON_OFFICE_HOURS);
        JsonArray officeHoursArray = (JsonArray) officeHoursObject;
        Assert.assertTrue(officeHoursObject instanceof JsonArray);
        JsonObject officeHour1 = officeHoursArray.getJsonObject(0);
        assertEquals("MONDAY", officeHour1.getString(JSON_DAY));
        assertEquals("10_00am", officeHour1.getString(JSON_TIME));
        assertEquals("Jane Doe", officeHour1.getString(JSON_NAME));
        JsonObject officeHour2 = officeHoursArray.getJsonObject(1);
        assertEquals("MONDAY", officeHour2.getString(JSON_DAY));
        assertEquals("10_30am", officeHour2.getString(JSON_TIME));
        assertEquals("Jane Doe", officeHour2.getString(JSON_NAME));
        JsonObject officeHour3 = officeHoursArray.getJsonObject(2);
        assertEquals("TUESDAY", officeHour3.getString(JSON_DAY));
        assertEquals("9_00am", officeHour3.getString(JSON_TIME));
        assertEquals("Joe Shmo", officeHour3.getString(JSON_NAME));
        JsonObject officeHour4 = officeHoursArray.getJsonObject(3);
        assertEquals("TUESDAY", officeHour4.getString(JSON_DAY));
        assertEquals("9_30am", officeHour4.getString(JSON_TIME));
        assertEquals("Joe Shmo", officeHour4.getString(JSON_NAME));
        JsonObject officeHour5 = officeHoursArray.getJsonObject(4);
        assertEquals("WEDNESDAY", officeHour5.getString(JSON_DAY));
        assertEquals("11_00am", officeHour5.getString(JSON_TIME));
        assertEquals("Jane Doe", officeHour5.getString(JSON_NAME));
        JsonObject officeHour6 = officeHoursArray.getJsonObject(5);
        assertEquals("WEDNESDAY", officeHour6.getString(JSON_DAY));
        assertEquals("11_30am", officeHour6.getString(JSON_TIME));
        assertEquals("Jane Doe", officeHour6.getString(JSON_NAME));
        JsonObject officeHour7 = officeHoursArray.getJsonObject(6);
        assertEquals("WEDNESDAY", officeHour7.getString(JSON_DAY));
        assertEquals("12_00pm", officeHour7.getString(JSON_TIME));
        assertEquals("Jane Doe", officeHour7.getString(JSON_NAME));
        JsonObject officeHour8 = officeHoursArray.getJsonObject(7);
        assertEquals("THURSDAY", officeHour8.getString(JSON_DAY));
        assertEquals("9_00am", officeHour8.getString(JSON_TIME));
        assertEquals("Joe Shmo", officeHour8.getString(JSON_NAME));
        JsonObject officeHour9 = officeHoursArray.getJsonObject(8);
        assertEquals("THURSDAY", officeHour9.getString(JSON_DAY));
        assertEquals("9_30am", officeHour9.getString(JSON_TIME));
        assertEquals("Joe Shmo", officeHour9.getString(JSON_NAME));
        JsonObject officeHour10 = officeHoursArray.getJsonObject(9);
        assertEquals("FRIDAY", officeHour10.getString(JSON_DAY));
        assertEquals("12_00pm", officeHour10.getString(JSON_TIME));
        assertEquals("Jane Doe", officeHour10.getString(JSON_NAME));
        JsonObject officeHour11 = officeHoursArray.getJsonObject(10);
        assertEquals("FRIDAY", officeHour11.getString(JSON_DAY));
        assertEquals("12_30pm", officeHour11.getString(JSON_TIME));
        assertEquals("Jane Doe", officeHour11.getString(JSON_NAME));
        System.out.println("TAs Loaded Properly");
        Object recitationObject = jsonObject.get(JSON_RECITATIONS);
        JsonArray recitationArray = (JsonArray) recitationObject;
        Assert.assertTrue(recitationObject instanceof JsonArray);
        JsonObject recitation1 = recitationArray.getJsonObject(0);
        assertEquals("R02", recitation1.getString(JSON_SECTION));
        assertEquals("McKenna", recitation1.getString(JSON_INSTRUCTOR));
        assertEquals("Wed 3:30pm-4:23pm", recitation1.getString(JSON_DAY_TIME));
        assertEquals("Old CS 2114", recitation1.getString(JSON_LOCATION));
        assertEquals("Jane Doe", recitation1.getString(JSON_TA1));
        assertEquals("Joe Shmo", recitation1.getString(JSON_TA2));
        JsonObject recitation2 = recitationArray.getJsonObject(1);
        assertEquals("R05", recitation2.getString(JSON_SECTION));
        assertEquals("Banerjee", recitation2.getString(JSON_INSTRUCTOR));
        assertEquals("Tues 5:30pm-6:23pm", recitation2.getString(JSON_DAY_TIME));
        assertEquals("Old CS 2114", recitation2.getString(JSON_LOCATION));
        assertEquals("Janmejay Bhavsar", recitation2.getString(JSON_TA1));
        assertEquals("Dhruv Bhawsar", recitation2.getString(JSON_TA2));
        System.out.println("Recitations Loaded Properly");
        String startingMonday = jsonObject.getString(JSON_MONDAY);  
        assertEquals("2017-01-23", startingMonday);
        String endingFriday = jsonObject.getString(JSON_FRIDAY);
        assertEquals("2017-04-28", endingFriday);
        Object scheduleObject = jsonObject.get(JSON_SCHEDULES);
        JsonArray scheduleArray = (JsonArray) scheduleObject;
        Assert.assertTrue(scheduleObject instanceof JsonArray);
        JsonObject schedule1 = scheduleArray.getJsonObject(0);
        assertEquals("Holiday", schedule1.getString(JSON_TYPE));
        assertEquals("02/09/2017", schedule1.getString(JSON_DATE));
        assertEquals("Snow Day", schedule1.getString(JSON_TITLE));
        assertEquals("", schedule1.getString(JSON_TOPIC));
        JsonObject schedule2 = scheduleArray.getJsonObject(1);
        assertEquals("Lecture", schedule2.getString(JSON_TYPE));
        assertEquals("02/14/2017", schedule2.getString(JSON_DATE));
        assertEquals("Lecture 3", schedule2.getString(JSON_TITLE));
        assertEquals("Event Programming", schedule2.getString(JSON_TOPIC));
        JsonObject schedule3 = scheduleArray.getJsonObject(2);
        assertEquals("Holiday", schedule3.getString(JSON_TYPE));
        assertEquals("03/13/2017", schedule3.getString(JSON_DATE));
        assertEquals("Spring Break", schedule3.getString(JSON_TITLE));
        assertEquals("", schedule3.getString(JSON_TOPIC));
        JsonObject schedule4 = scheduleArray.getJsonObject(3);
        assertEquals("Recitation", schedule4.getString(JSON_TYPE));
        assertEquals("02/01/2017", schedule4.getString(JSON_DATE));
        assertEquals("Recitation 1", schedule4.getString(JSON_TITLE));
        assertEquals("NetBeans & Version Control", schedule4.getString(JSON_TOPIC));
        JsonObject schedule5 = scheduleArray.getJsonObject(4);
        assertEquals("HW", schedule5.getString(JSON_TYPE));
        assertEquals("03/27/2017", schedule5.getString(JSON_DATE));
        assertEquals("HW3", schedule5.getString(JSON_TITLE));
        assertEquals("UML", schedule5.getString(JSON_TOPIC));
        System.out.println("Schedules Loaded Properly");
        Object teamObject = jsonObject.get(JSON_TEAMS);
        JsonArray teamArray = (JsonArray) teamObject;
        Assert.assertTrue(teamObject instanceof JsonArray);
        JsonObject team1 = teamArray.getJsonObject(0);
        assertEquals("Atomic Comics", team1.getString(JSON_TEAM_NAME));
        assertEquals("552211", team1.getString(JSON_COLOR));
        assertEquals("000000", team1.getString(JSON_TEXT_COLOR));
        assertEquals("http://atomiccomic.com", team1.getString(JSON_LINK));
        JsonObject team2 = teamArray.getJsonObject(1);
        assertEquals("C4 Comics", team2.getString(JSON_TEAM_NAME));
        assertEquals("235399", team2.getString(JSON_COLOR));
        assertEquals("000000", team2.getString(JSON_TEXT_COLOR));
        assertEquals("http://c4-comics.appshot.com", team2.getString(JSON_LINK));
        Object studentObject = jsonObject.get(JSON_STUDENTS);
        JsonArray studentArray = (JsonArray) studentObject;
        Assert.assertTrue(studentObject instanceof JsonArray);
        JsonObject student1 = studentArray.getJsonObject(0);
        assertEquals("Beau", student1.getString(JSON_FIRST_NAME));
        assertEquals("Brummell", student1.getString(JSON_LAST_NAME));
        assertEquals("Atomic Comics", student1.getString(JSON_TEAM));
        assertEquals("Lead Designer", student1.getString(JSON_ROLE));
        JsonObject student2 = studentArray.getJsonObject(1);
        assertEquals("Jane", student2.getString(JSON_FIRST_NAME));
        assertEquals("Doe", student2.getString(JSON_LAST_NAME));
        assertEquals("C4 Comics", student2.getString(JSON_TEAM));
        assertEquals("Lead Programmer", student2.getString(JSON_ROLE));
        JsonObject student3 = studentArray.getJsonObject(2);
        assertEquals("Noonian", student3.getString(JSON_FIRST_NAME));
        assertEquals("Soong", student3.getString(JSON_LAST_NAME));
        assertEquals("Atomic Comics", student3.getString(JSON_TEAM));
        assertEquals("Data Designer", student3.getString(JSON_ROLE));
        JsonObject student4 = studentArray.getJsonObject(3);
        assertEquals("Janmejay", student4.getString(JSON_FIRST_NAME));
        assertEquals("Bhavsar", student4.getString(JSON_LAST_NAME));
        assertEquals("Atomic Comics", student4.getString(JSON_TEAM));
        assertEquals("Lead Programmer", student4.getString(JSON_ROLE));
        JsonObject student5 = studentArray.getJsonObject(4);
        assertEquals("Dhruv", student5.getString(JSON_FIRST_NAME));
        assertEquals("Bhawsar", student5.getString(JSON_LAST_NAME));
        assertEquals("Atomic Comics", student5.getString(JSON_TEAM));
        assertEquals("Project Manager", student5.getString(JSON_ROLE));
        JsonObject student6 = studentArray.getJsonObject(5);
        assertEquals("John", student6.getString(JSON_FIRST_NAME));
        assertEquals("Doe", student6.getString(JSON_LAST_NAME));
        assertEquals("C4 Comics", student6.getString(JSON_TEAM));
        assertEquals("Project Manager", student6.getString(JSON_ROLE));
        JsonObject student7 = studentArray.getJsonObject(6);
        assertEquals("Ezio", student7.getString(JSON_FIRST_NAME));
        assertEquals("Auditore", student7.getString(JSON_LAST_NAME));
        assertEquals("C4 Comics", student7.getString(JSON_TEAM));
        assertEquals("Lead Designer", student7.getString(JSON_ROLE));
        JsonObject student8 = studentArray.getJsonObject(7);
        assertEquals("Yusuf", student8.getString(JSON_FIRST_NAME));
        assertEquals("Tazim", student8.getString(JSON_LAST_NAME));
        assertEquals("C4 Comics", student8.getString(JSON_TEAM));
        assertEquals("Data Designer", student8.getString(JSON_ROLE));
        System.out.println("Projects Loaded Properly");
        }
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    }