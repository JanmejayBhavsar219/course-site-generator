package csg.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import csg.TAManagerApp;
import csg.data.CourseDetail;
import csg.data.Recitation;
import csg.data.ScheduleItem;
import csg.data.Student;
import csg.data.TAData;
import csg.data.TeachingAssistant;
import csg.data.Team;
import csg.workspace.CourseDetailWorkspace;
import csg.workspace.ScheduleWorkspace;
import djf.ui.AppGUI;
import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class serves as the file component for the TA manager app. It provides
 * all saving and loading services for the application.
 *
 * @author Richard McKenna
 */
public class TAFiles implements AppFileComponent {

    // THIS IS THE APP ITSELF
    TAManagerApp app;

    // THESE ARE USED FOR IDENTIFYING JSON TYPES
    static final String JSON_COURSE_INFO = "course_info";
    static final String JSON_SUBJECT = "subject";
    static final String JSON_NUMBER = "number";
    static final String JSON_SEMESTER = "semester";
    static final String JSON_YEAR = "year";
    static final String JSON_COURSE_TITLE = "title";
    static final String JSON_INSTRUCTOR_NAME = "instructor_name";
    static final String JSON_INSTRUCTOR_HOME = "instructor_home";
    static final String JSON_EXPORT_DIRECTORY = "export_directory";
    static final String JSON_SITE_TEMPLATE = "site_template";
    static final String JSON_TEMPLATE_DIRECTORY = "template_directory";
    static final String JSON_SITE_PAGES = "site_pages";
    static final String JSON_USE = "use";
    static final String JSON_NAVBAR_TITLE = "navbar_title";
    static final String JSON_FILE_NAME = "file_name";
    static final String JSON_SCRIPT = "script";
    static final String JSON_PAGE_STYLE = "page_style";
    static final String JSON_BANNER_SCHOOL_IMAGE = "banner_school_image_path";
    static final String JSON_LEFT_FOOTER_IMAGE = "left_footer_image";
    static final String JSON_RIGHT_FOOTER_IMAGE = "right_footer";
    static final String JSON_STYLESHEET = "stylesheet";
    static final String JSON_START_HOUR = "startHour";
    static final String JSON_END_HOUR = "endHour";
    static final String JSON_OFFICE_HOURS = "officeHours";
    static final String JSON_DAY = "day";
    static final String JSON_TIME = "time";
    static final String JSON_UNDERGRAD = "is_undergrad";
    static final String JSON_NAME = "name";
    static final String JSON_GRAD_TAS = "grad_tas";
    static final String JSON_UNDERGRAD_TAS = "undergrad_tas";
    static final String JSON_EMAIL = "email";
    static final String JSON_SECTION = "section";
    static final String JSON_INSTRUCTOR = "instructor";
    static final String JSON_DAY_TIME = "day_time";
    static final String JSON_LOCATION = "location";
    static final String JSON_TA1 = "ta_1";
    static final String JSON_TA2 = "ta_2";
    static final String JSON_RECITATIONS = "recitations";
    static final String JSON_MONDAY = "monday";
    static final String JSON_FRIDAY = "friday";
    static final String JSON_TYPE = "type";
    static final String JSON_DATE = "date";
    static final String JSON_TITLE = "title";
    static final String JSON_TOPIC = "topic";
    static final String JSON_SCHEDULE_TIME = "time";
    static final String JSON_CRITERIA = "criteria";
    static final String JSON_SCHEDULES = "schedules";
    static final String JSON_TEAM_NAME = "team_name";
    static final String JSON_COLOR = "color";
    static final String JSON_TEXT_COLOR = "text_color";
    static final String JSON_LINK = "link";
    static final String JSON_FIRST_NAME = "first_name";
    static final String JSON_LAST_NAME = "last_name";
    static final String JSON_TEAM = "team";
    static final String JSON_ROLE = "role";
    static final String JSON_TEAMS = "teams";
    static final String JSON_STUDENTS = "students";

    //EXPORT FORMAT FOR SCHEDULE DATA TAB
    static final String JSON_EXPORT_STARTING_MONDAY_MONTH = "startingMondayMonth";
    static final String JSON_EXPORT_STARTING_MONDAY_DAY = "startingMondayDay";
    static final String JSON_EXPORT_ENDING_FRIDAY_MONTH = "endingFridayMonth";
    static final String JSON_EXPORT_ENDING_FRIDAY_DAY = "endingFridayDay";
    static final String JSON_EXPORT_HOLIDAYS = "holidays";
    static final String JSON_EXPORT_LECTURES = "lectures";
    static final String JSON_EXPORT_HWS = "hws";
    static final String JSON_EXPORT_RECITATIONS = "recitations";
    static final String JSON_EXPORT_MONTH = "month";
    static final String JSON_EXPORT_DAY = "day";
    static final String JSON_EXPORT_TITLE = "title";
    static final String JSON_EXPORT_TOPIC = "topic";
    static final String JSON_EXPORT_LINK = "link";
    //EXPORT FORMAT FOR THE PROJECT DATA TAB
    static final String JSON_EXPORT_TEAM_NAME = "name";
    static final String JSON_EXPORT_RED = "red";
    static final String JSON_EXPORT_GREEN = "green";
    static final String JSON_EXPORT_BLUE = "blue";
    static final String JSON_EXPORT_FIRST_NAME = "firstName";
    static final String JSON_EXPORT_LAST_NAME = "lastName";
    static final String JSON_EXPORT_PROJECTS = "projects";
    static final String JSON_EXPORT_WORK = "work";
    static final String JSON_EXPORT_TIME = "time";
    static final String JSON_EXPORT_CRITERIA = "criteria";

    public TAFiles(TAManagerApp initApp) {
        app = initApp;
    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        // CLEAR THE OLD DATA OUT
        TAData dataManager = (TAData) data;

        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(filePath);
//        System.out.println(filePath);
        // NOW RELOAD THE WORKSPACE WITH THE LOADED DATA
        app.getWorkspaceComponent().reloadWorkspace(app.getDataComponent());
        String subject = json.getString(JSON_SUBJECT);
//        System.out.println(subject);
        dataManager.setSubject(subject);
        String number = json.getString(JSON_NUMBER);
        dataManager.setNumber(number);
        String semester = json.getString(JSON_SEMESTER);
        dataManager.setSemester(semester);
        String year = json.getString(JSON_YEAR);
        dataManager.setYear(year);
        String courseTitle = json.getString(JSON_COURSE_TITLE);
        dataManager.setTitle(courseTitle);
        String instructorName = json.getString(JSON_INSTRUCTOR_NAME);
        dataManager.setInstructorName(instructorName);
        String instructorHome = json.getString(JSON_INSTRUCTOR_HOME);
        dataManager.setInstructorHome(instructorHome);
        String exportDirectory = json.getString(JSON_EXPORT_DIRECTORY);
        dataManager.setExportedDirectory(exportDirectory);
        String templateDirectory = json.getString(JSON_TEMPLATE_DIRECTORY);
        dataManager.setTemplateDirectory(templateDirectory);
        JsonArray jsonSitePagesArray = json.getJsonArray(JSON_SITE_PAGES);
        for (int i = 0; i < jsonSitePagesArray.size(); i++) {
            JsonObject jsonSitePage = jsonSitePagesArray.getJsonObject(i);
            boolean use = jsonSitePage.getBoolean(JSON_USE);
            String navbarTitle = jsonSitePage.getString(JSON_NAVBAR_TITLE);
            String fileName = jsonSitePage.getString(JSON_FILE_NAME);
            String script = jsonSitePage.getString(JSON_SCRIPT);
            dataManager.addSitePage(use, navbarTitle, fileName, script);
        }
        String bannerSchoolImage = json.getString(JSON_BANNER_SCHOOL_IMAGE);
        dataManager.setBannerSchoolImage(bannerSchoolImage);
        
        String leftFooterImage = json.getString(JSON_LEFT_FOOTER_IMAGE);
        dataManager.setLeftFooterImage(leftFooterImage);
        String rightFooterImage = json.getString(JSON_RIGHT_FOOTER_IMAGE);
        dataManager.setRightFooterImage(rightFooterImage);
        String stylesheet = json.getString(JSON_STYLESHEET);
        dataManager.setStylesheet(stylesheet);
        CourseDetailWorkspace cdw = CourseDetailWorkspace.getCourseDetailWorkspace();
        cdw.getSubjectComboBox().getSelectionModel().select(subject);
        cdw.getNumberComboBox().getSelectionModel().select(number);
        cdw.getSemesterComboBox().getSelectionModel().select(semester);
        cdw.getYearComboBox().getSelectionModel().select(year);
        cdw.getTitleTextField().setText(courseTitle);
        cdw.getInstructorNameTextField().setText(instructorName);
        cdw.getInstructorHomeTextField().setText(instructorHome);
        cdw.getExportDirectorySelectedLabel().setText(exportDirectory);
        cdw.getSelectedTemplateLabel().setText(templateDirectory);
        cdw.setBannerSchoolImage(bannerSchoolImage);
        cdw.setLeftFooterImage(leftFooterImage);
        cdw.setRightFooterImage(rightFooterImage);
        cdw.getStyleSheetComboBox().getSelectionModel().select(stylesheet);
        // LOAD THE START AND END HOURS
        String startHour = json.getString(JSON_START_HOUR);
        String endHour = json.getString(JSON_END_HOUR);
        dataManager.initHours(startHour, endHour);
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsExported(gui);
        // NOW LOAD ALL THE GRAD TAs
        JsonArray jsonGradTAArray = json.getJsonArray(JSON_GRAD_TAS);
        for (int i = 0; i < jsonGradTAArray.size(); i++) {
            JsonObject jsonTA = jsonGradTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            Boolean isUndergrad = jsonTA.getBoolean(JSON_UNDERGRAD);
//            System.out.println(isUndergrad);
            dataManager.addTA(isUndergrad, name, email);
        }

        // NOW LOAD ALL THE UNDERGRAD TAs
        JsonArray jsonTAArray = json.getJsonArray(JSON_UNDERGRAD_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            Boolean isUndergrad = jsonTA.getBoolean(JSON_UNDERGRAD);
//            System.out.println(isUndergrad);
            dataManager.addTA(isUndergrad, name, email);
        }

        // AND THEN ALL THE OFFICE HOURS
        JsonArray jsonOfficeHoursArray = json.getJsonArray(JSON_OFFICE_HOURS);
        for (int i = 0; i < jsonOfficeHoursArray.size(); i++) {
            JsonObject jsonOfficeHours = jsonOfficeHoursArray.getJsonObject(i);
            String day = jsonOfficeHours.getString(JSON_DAY);
            String time = jsonOfficeHours.getString(JSON_TIME);
            String name = jsonOfficeHours.getString(JSON_NAME);
            dataManager.addOfficeHoursReservation(day, time, name);
        }
        JsonArray jsonRecitationArray = json.getJsonArray(JSON_RECITATIONS);
        for (int i = 0; i < jsonRecitationArray.size(); i++) {
            JsonObject jsonRecitation = jsonRecitationArray.getJsonObject(i);
            String section = jsonRecitation.getString(JSON_SECTION);
            String instructor = jsonRecitation.getString(JSON_INSTRUCTOR);
            String day_time = jsonRecitation.getString(JSON_DAY_TIME);
            String location = jsonRecitation.getString(JSON_LOCATION);
            String TA1 = jsonRecitation.getString(JSON_TA1);
            String TA2 = jsonRecitation.getString(JSON_TA2);
            dataManager.addRecitation(section, instructor, day_time, location, TA1, TA2);
        }
        String mondayDate = json.getString(JSON_MONDAY);
        String fridayDate = json.getString(JSON_FRIDAY);
        ScheduleWorkspace sw = ScheduleWorkspace.getScheduleWorkspace();
        LocalDate monday = LocalDate.parse(mondayDate);
        dataManager.setStartingMonday(monday);
        LocalDate friday = LocalDate.parse(fridayDate);
        dataManager.setEndingFriday(friday);
        sw.getStartingMondayPicker().setValue(monday);
        sw.getEndingFridayPicker().setValue(friday);
        JsonArray jsonScheduleArray = json.getJsonArray(JSON_SCHEDULES);
        for (int i = 0; i < jsonScheduleArray.size(); i++) {
            JsonObject jsonSchedule = jsonScheduleArray.getJsonObject(i);
            if (jsonSchedule.getJsonString(JSON_TYPE).getString().equals("HW")) {
                String type = jsonSchedule.getString(JSON_TYPE);
                String date = jsonSchedule.getString(JSON_DATE);
                String title = jsonSchedule.getString(JSON_TITLE);
                String topic = jsonSchedule.getString(JSON_TOPIC);
                String time = jsonSchedule.getString(JSON_TIME);
                String criteria = jsonSchedule.getString(JSON_CRITERIA);
                try {
                    dataManager.addHWScheduleItem(type, date, title, topic, time, criteria);
                } catch (ParseException ex) {
                    Logger.getLogger(TAFiles.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                String type = jsonSchedule.getString(JSON_TYPE);
                String date = jsonSchedule.getString(JSON_DATE);
                String title = jsonSchedule.getString(JSON_TITLE);
                String topic = jsonSchedule.getString(JSON_TOPIC);
                try {
                    dataManager.addScheduleItem(type, date, title, topic);
                } catch (ParseException ex) {
                    Logger.getLogger(TAFiles.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        JsonArray jsonTeamArray = json.getJsonArray(JSON_TEAMS);
        for (int i = 0; i < jsonTeamArray.size(); i++) {
            JsonObject jsonTeam = jsonTeamArray.getJsonObject(i);
            String teamName = jsonTeam.getString(JSON_TEAM_NAME);
            String color = jsonTeam.getString(JSON_COLOR);
            String textColor = jsonTeam.getString(JSON_TEXT_COLOR);
            String link = jsonTeam.getString(JSON_LINK);
            dataManager.addTeam(teamName, color, textColor, link);
        }
        JsonArray jsonStudentArray = json.getJsonArray(JSON_STUDENTS);
        for (int i = 0; i < jsonStudentArray.size(); i++) {
            JsonObject jsonStudent = jsonStudentArray.getJsonObject(i);
            String firstName = jsonStudent.getString(JSON_FIRST_NAME);
            String lastName = jsonStudent.getString(JSON_LAST_NAME);
            String team = jsonStudent.getString(JSON_TEAM);
            String role = jsonStudent.getString(JSON_ROLE);
            dataManager.addStudent(firstName, lastName, team, role);
        }
        dataManager.populateTAComboBox();
        dataManager.populateTeamComboBox();
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        // GET THE DATA
        TAData dataManager = (TAData) data;
        JsonArrayBuilder sitePagesArrayBuilder = Json.createArrayBuilder();
        ObservableList<CourseDetail> sitePages = dataManager.getSitePages();
        for (CourseDetail cd : sitePages) {
            JsonObject sitePageJson = Json.createObjectBuilder()
                    .add(JSON_USE, cd.use())
                    .add(JSON_NAVBAR_TITLE, cd.getNavbarTitle())
                    .add(JSON_FILE_NAME, cd.getFileName())
                    .add(JSON_SCRIPT, cd.getScript()).build();
            sitePagesArrayBuilder.add(sitePageJson);
        }
        JsonArray sitePagesArray = sitePagesArrayBuilder.build();
        // NOW BUILD THE TA JSON OBJCTS TO SAVE
        JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder gradTAArrayBuilder = Json.createArrayBuilder();
        ObservableList<TeachingAssistant> tas = dataManager.getTeachingAssistants();
        for (TeachingAssistant ta : tas) {
            if(ta.getUndergrad() == true){
            JsonObject taJson = Json.createObjectBuilder()
                    .add(JSON_UNDERGRAD, ta.getUndergrad())
                    .add(JSON_NAME, ta.getName())
                    .add(JSON_EMAIL, ta.getEmail()).build();
            taArrayBuilder.add(taJson);
            }
            else{
                JsonObject taJson = Json.createObjectBuilder()
                    .add(JSON_UNDERGRAD, ta.getUndergrad())
                    .add(JSON_NAME, ta.getName())
                    .add(JSON_EMAIL, ta.getEmail()).build();
            gradTAArrayBuilder.add(taJson);
            }
        }
        JsonArray undergradTAsArray = taArrayBuilder.build();
        JsonArray gradTAsArray = gradTAArrayBuilder.build();

        // NOW BUILD THE TIME SLOT JSON OBJCTS TO SAVE
        JsonArray timeSlotsArray;
        try {
            JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
            ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(dataManager);
            for (TimeSlot ts : officeHours) {
                JsonObject tsJson = Json.createObjectBuilder()
                        .add(JSON_DAY, ts.getDay())
                        .add(JSON_TIME, ts.getTime())
                        .add(JSON_NAME, ts.getName()).build();
                timeSlotArrayBuilder.add(tsJson);
            }
            timeSlotsArray = timeSlotArrayBuilder.build();
        } catch (NullPointerException npe) {
            JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
            ArrayList<TimeSlot> officeHours = dataManager.getOfficeHoursTimeSlots();
            for (TimeSlot ts : officeHours) {
                JsonObject tsJson = Json.createObjectBuilder()
                        .add(JSON_DAY, ts.getDay())
                        .add(JSON_TIME, ts.getTime())
                        .add(JSON_NAME, ts.getName()).build();
                timeSlotArrayBuilder.add(tsJson);
            }
            timeSlotsArray = timeSlotArrayBuilder.build();
        }
        JsonArrayBuilder recitationArrayBuilder = Json.createArrayBuilder();
        ObservableList<Recitation> recs = dataManager.getRecitations();
//        System.out.println(dataManager.getRecitations());
        for (Recitation rec : recs) {
            JsonObject recitationJson = Json.createObjectBuilder()
                    .add(JSON_SECTION, rec.getSection())
                    .add(JSON_INSTRUCTOR, rec.getInstructor())
                    .add(JSON_DAY_TIME, rec.getDay_Time())
                    .add(JSON_LOCATION, rec.getLocation())
                    .add(JSON_TA1, rec.getTA1())
                    .add(JSON_TA2, rec.getTA2()).build();
            recitationArrayBuilder.add(recitationJson);
        }
        JsonArray recitationsArray = recitationArrayBuilder.build();
        JsonArrayBuilder scheduleArrayBuilder = Json.createArrayBuilder();
        ObservableList<ScheduleItem> sItems = dataManager.getSchedules();
//        System.out.println(dataManager.getSchedules());
        for (ScheduleItem si : sItems) {
            if (si.getType().equals("HW")) {
                JsonObject scheduleJson = Json.createObjectBuilder()
                        .add(JSON_TYPE, si.getType())
                        .add(JSON_DATE, si.getDate())
                        .add(JSON_TITLE, si.getTitle())
                        .add(JSON_TOPIC, si.getTopic())
                        .add(JSON_SCHEDULE_TIME, si.getTime())
                        .add(JSON_CRITERIA, si.getCriteria()).build();
                scheduleArrayBuilder.add(scheduleJson);
            } else {
                JsonObject scheduleJson = Json.createObjectBuilder()
                        .add(JSON_TYPE, si.getType())
                        .add(JSON_DATE, si.getDate())
                        .add(JSON_TITLE, si.getTitle())
                        .add(JSON_TOPIC, si.getTopic()).build();
                scheduleArrayBuilder.add(scheduleJson);
            }
        }
        JsonArray schedulesArray = scheduleArrayBuilder.build();
        JsonArrayBuilder teamArrayBuilder = Json.createArrayBuilder();
        ObservableList<Team> teams = dataManager.getTeams();
        for (Team t : teams) {
            JsonObject teamJson = Json.createObjectBuilder()
                    .add(JSON_TEAM_NAME, t.getName())
                    .add(JSON_COLOR, t.getColor())
                    .add(JSON_TEXT_COLOR, t.getTextColor())
                    .add(JSON_LINK, t.getLink()).build();
            teamArrayBuilder.add(teamJson);
        }
        JsonArray teamsArray = teamArrayBuilder.build();
        JsonArrayBuilder studentArrayBuilder = Json.createArrayBuilder();
        ObservableList<Student> students = dataManager.getStudents();
        for (Student s : students) {
            JsonObject studentJson = Json.createObjectBuilder()
                    .add(JSON_FIRST_NAME, s.getFirstName())
                    .add(JSON_LAST_NAME, s.getLastName())
                    .add(JSON_TEAM, s.getTeam())
                    .add(JSON_ROLE, s.getRole()).build();
            studentArrayBuilder.add(studentJson);
        }
        JsonArray studentsArray = studentArrayBuilder.build();
        // THEN PUT IT ALL TOGETHER IN A JsonObject
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_SUBJECT, dataManager.getSubject())
                .add(JSON_NUMBER, dataManager.getNumber())
                .add(JSON_SEMESTER, dataManager.getSemester())
                .add(JSON_YEAR, dataManager.getYear())
                .add(JSON_COURSE_TITLE, dataManager.getTitle())
                .add(JSON_INSTRUCTOR_NAME, dataManager.getInstructorName())
                .add(JSON_INSTRUCTOR_HOME, dataManager.getInstructorHome())
                .add(JSON_EXPORT_DIRECTORY, dataManager.getExportedDirectory())
                .add(JSON_TEMPLATE_DIRECTORY, dataManager.getTemplateDirectory())
                .add(JSON_SITE_PAGES, sitePagesArray)
                .add(JSON_BANNER_SCHOOL_IMAGE, dataManager.getBannerSchoolImagePath())
                .add(JSON_LEFT_FOOTER_IMAGE, dataManager.getLeftFooterImagePath())
                .add(JSON_RIGHT_FOOTER_IMAGE, dataManager.getRightFooterImagePath())
                .add(JSON_STYLESHEET, dataManager.getStylesheet())
                .add(JSON_START_HOUR, "" + dataManager.getStartHour())
                .add(JSON_END_HOUR, "" + dataManager.getEndHour())
                .add(JSON_GRAD_TAS, gradTAsArray)
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_OFFICE_HOURS, timeSlotsArray)
                .add(JSON_RECITATIONS, recitationsArray)
                .add(JSON_MONDAY, dataManager.getStartingMonday())
                .add(JSON_FRIDAY, dataManager.getEndingFriday())
                .add(JSON_SCHEDULES, schedulesArray)
                .add(JSON_TEAMS, teamsArray)
                .add(JSON_STUDENTS, studentsArray)
                .build();
        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }
    // IMPORTING/EXPORTING DATA IS USED WHEN WE READ/WRITE DATA IN AN
    // ADDITIONAL FORMAT USEFUL FOR ANOTHER PURPOSE, LIKE ANOTHER APPLICATION

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        try {
            // GET THE DATA
            TAData dataManager = (TAData) data;

            // NOW BUILD THE TA JSON OBJCTS TO SAVE
            JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder gradTAArrayBuilder = Json.createArrayBuilder();
        ObservableList<TeachingAssistant> tas = dataManager.getTeachingAssistants();
        for (TeachingAssistant ta : tas) {
            if(ta.getUndergrad() == true){
            JsonObject taJson = Json.createObjectBuilder()
                    .add(JSON_UNDERGRAD, ta.getUndergrad())
                    .add(JSON_NAME, ta.getName())
                    .add(JSON_EMAIL, ta.getEmail()).build();
            taArrayBuilder.add(taJson);
            }
            else{
                JsonObject taJson = Json.createObjectBuilder()
                    .add(JSON_UNDERGRAD, ta.getUndergrad())
                    .add(JSON_NAME, ta.getName())
                    .add(JSON_EMAIL, ta.getEmail()).build();
            gradTAArrayBuilder.add(taJson);
            }
        }
        JsonArray undergradTAsArray = taArrayBuilder.build();
        JsonArray gradTAsArray = gradTAArrayBuilder.build();

            // NOW BUILD THE TIME SLOT JSON OBJCTS TO SAVE
            JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
            ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(dataManager);
            for (TimeSlot ts : officeHours) {
                JsonObject tsJson = Json.createObjectBuilder()
                        .add(JSON_DAY, ts.getDay())
                        .add(JSON_TIME, ts.getTime())
                        .add(JSON_NAME, ts.getName()).build();
                timeSlotArrayBuilder.add(tsJson);
            }
            JsonArray timeSlotsArray = timeSlotArrayBuilder.build();

            // THEN PUT IT ALL TOGETHER IN A JsonObject
            JsonObject dataManagerJSO = Json.createObjectBuilder()
                    .add(JSON_START_HOUR, "" + dataManager.getStartHour())
                    .add(JSON_END_HOUR, "" + dataManager.getEndHour())
                    .add(JSON_GRAD_TAS, gradTAsArray)
                    .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                    .add(JSON_OFFICE_HOURS, timeSlotsArray)
                    .build();

            // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
            Map<String, Object> properties = new HashMap<>(1);
            properties.put(JsonGenerator.PRETTY_PRINTING, true);
            JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
            StringWriter sw = new StringWriter();
            JsonWriter jsonWriter = writerFactory.createWriter(sw);
            jsonWriter.writeObject(dataManagerJSO);
            jsonWriter.close();

            // INIT THE WRITER
            OutputStream os = new FileOutputStream(filePath + "/public_html/js/OfficeHoursGridData.json");
            JsonWriter jsonFileWriter = Json.createWriter(os);
            jsonFileWriter.writeObject(dataManagerJSO);
            String prettyPrinted = sw.toString();
            PrintWriter pw = new PrintWriter(filePath + "/public_html/js/OfficeHoursGridData.json");
            pw.write(prettyPrinted);
            pw.close();
            
            JsonArrayBuilder sitePagesArrayBuilder = Json.createArrayBuilder();
        ObservableList<CourseDetail> sitePages = dataManager.getSitePages();
        for (CourseDetail cd : sitePages) {
            JsonObject sitePageJson = Json.createObjectBuilder()
                    .add(JSON_USE, cd.use())
                    .add(JSON_NAVBAR_TITLE, cd.getNavbarTitle())
                    .add(JSON_FILE_NAME, cd.getFileName())
                    .add(JSON_SCRIPT, cd.getScript()).build();
            sitePagesArrayBuilder.add(sitePageJson);
        }
        JsonArray sitePagesArray = sitePagesArrayBuilder.build();
        JsonObject courseDataManagerJSO = Json.createObjectBuilder()
                .add(JSON_SUBJECT, dataManager.getSubject())
                .add(JSON_NUMBER, dataManager.getNumber())
                .add(JSON_SEMESTER, dataManager.getSemester())
                .add(JSON_YEAR, dataManager.getYear())
                .add(JSON_COURSE_TITLE, dataManager.getTitle())
                .add(JSON_INSTRUCTOR_NAME, dataManager.getInstructorName())
                .add(JSON_INSTRUCTOR_HOME, dataManager.getInstructorHome())
                .add(JSON_EXPORT_DIRECTORY, dataManager.getExportedDirectory())
                .add(JSON_TEMPLATE_DIRECTORY, dataManager.getTemplateDirectory())
                .add(JSON_SITE_PAGES, sitePagesArray)
                .add(JSON_BANNER_SCHOOL_IMAGE, dataManager.getBannerSchoolImagePath())
                .add(JSON_LEFT_FOOTER_IMAGE, dataManager.getLeftFooterImagePath())
                .add(JSON_RIGHT_FOOTER_IMAGE, dataManager.getRightFooterImagePath())
                .add(JSON_STYLESHEET, dataManager.getStylesheet()).build();
        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> cProperties = new HashMap<>(1);
        cProperties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory cWriterFactory = Json.createWriterFactory(cProperties);
        StringWriter csw = new StringWriter();
        JsonWriter cJsonWriter = cWriterFactory.createWriter(csw);
        cJsonWriter.writeObject(courseDataManagerJSO);
        cJsonWriter.close();

        // INIT THE WRITER
        OutputStream cos = new FileOutputStream(filePath + "/public_html/js/HomeData.json");
        JsonWriter cJsonFileWriter = Json.createWriter(cos);
        cJsonFileWriter.writeObject(courseDataManagerJSO);
        String cPrettyPrinted = csw.toString();
        PrintWriter cpw = new PrintWriter(filePath + "/public_html/js/HomeData.json");
        cpw.write(cPrettyPrinted);
        cpw.close();

            JsonArrayBuilder recitationArrayBuilder = Json.createArrayBuilder();
            ObservableList<Recitation> recs = dataManager.getRecitations();
            for (Recitation rec : recs) {
                JsonObject recitationJson = Json.createObjectBuilder()
                        .add(JSON_SECTION, rec.getSection())
                        .add(JSON_INSTRUCTOR, rec.getInstructor())
                        .add(JSON_DAY_TIME, rec.getDay_Time())
                        .add(JSON_LOCATION, rec.getLocation())
                        .add(JSON_TA1, rec.getTA1())
                        .add(JSON_TA2, rec.getTA2()).build();
                recitationArrayBuilder.add(recitationJson);
            }
            JsonArray recitationsArray = recitationArrayBuilder.build();
            // THEN PUT IT ALL TOGETHER IN A JsonObject
            JsonObject dataManagerRJSO = Json.createObjectBuilder()
                    .add(JSON_RECITATIONS, recitationsArray)
                    .build();
            // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
            Map<String, Object> rProperties = new HashMap<>(1);
            rProperties.put(JsonGenerator.PRETTY_PRINTING, true);
            JsonWriterFactory rWriterFactory = Json.createWriterFactory(rProperties);
            StringWriter rsw = new StringWriter();
            JsonWriter rJsonWriter = rWriterFactory.createWriter(rsw);
            rJsonWriter.writeObject(dataManagerRJSO);
            rJsonWriter.close();

            // INIT THE WRITER
            OutputStream ros = new FileOutputStream(filePath + "/public_html/js/RecitationsData.json");
            JsonWriter rJsonFileWriter = Json.createWriter(ros);
            rJsonFileWriter.writeObject(dataManagerRJSO);
            String rPrettyPrinted = rsw.toString();
            PrintWriter rpw = new PrintWriter(filePath + "/public_html/js/RecitationsData.json");
            rpw.write(rPrettyPrinted);
            rpw.close();

            JsonArrayBuilder holidaysArrayBuilder = Json.createArrayBuilder();
            ObservableList<ScheduleItem> schedules = dataManager.getSchedules();
//            System.out.println(dataManager.getSchedules());
            for (ScheduleItem si : schedules) {
                if (si.getType().equals("Holiday")) {
                    int month = si.getScheduleDate().getMonth() + 1;
                    int date = si.getScheduleDate().getDate() + 1;
                    JsonObject holidayItemJson = Json.createObjectBuilder()
                            .add(JSON_EXPORT_MONTH, month + "")
                            .add(JSON_EXPORT_DAY, date + "")
                            .add(JSON_EXPORT_TITLE, si.getTitle())
                            .add(JSON_EXPORT_LINK, "")
                            .build();
                    holidaysArrayBuilder.add(holidayItemJson);
                }
            }
            JsonArray holidaysArray = holidaysArrayBuilder.build();
            JsonArrayBuilder lecturesArrayBuilder = Json.createArrayBuilder();
            for (ScheduleItem si : schedules) {
                if (si.getType().equals("Lecture")) {
                    int month = si.getScheduleDate().getMonth() + 1;
                    int date = si.getScheduleDate().getDate() + 1;
                    JsonObject lectureItemJson = Json.createObjectBuilder()
                            .add(JSON_EXPORT_MONTH, month + "")
                            .add(JSON_EXPORT_DAY, date + "")
                            .add(JSON_EXPORT_TITLE, si.getTitle())
                            .add(JSON_EXPORT_TOPIC, si.getTopic())
                            .add(JSON_EXPORT_LINK, "")
                            .build();
                    lecturesArrayBuilder.add(lectureItemJson);
                }
            }
            JsonArray lecturesArray = lecturesArrayBuilder.build();
            JsonArrayBuilder recitationsArrayBuilder = Json.createArrayBuilder();
            for (ScheduleItem si : schedules) {
                if (si.getType().equals("Recitation")) {
                    int month = si.getScheduleDate().getMonth() + 1;
                    int date = si.getScheduleDate().getDate() + 1;
                    JsonObject recitationItemJson = Json.createObjectBuilder()
                            .add(JSON_EXPORT_MONTH, month + "")
                            .add(JSON_EXPORT_DAY, date + "")
                            .add(JSON_EXPORT_TITLE, si.getTitle())
                            .add(JSON_EXPORT_TOPIC, si.getTopic())
                            .build();
                    recitationsArrayBuilder.add(recitationItemJson);
                }
            }
            JsonArray recitationsItemArray = recitationsArrayBuilder.build();
            JsonArrayBuilder hwsArrayBuilder = Json.createArrayBuilder();
            for (ScheduleItem si : schedules) {
                if (si.getType().equals("HW")) {
                    int month = si.getScheduleDate().getMonth() + 1;
                    int date = si.getScheduleDate().getDate() + 1;
                    JsonObject hwItemJson = Json.createObjectBuilder()
                            .add(JSON_EXPORT_MONTH, month + "")
                            .add(JSON_EXPORT_DAY, date + "")
                            .add(JSON_EXPORT_TITLE, si.getTitle())
                            .add(JSON_EXPORT_TOPIC, si.getTopic())
                            .add(JSON_EXPORT_LINK, "")
                            .add(JSON_EXPORT_TIME, si.getTime())
                            .add(JSON_EXPORT_CRITERIA, si.getCriteria())
                            .build();
                    hwsArrayBuilder.add(hwItemJson);
                }
            }
            JsonArray hwsArray = hwsArrayBuilder.build();
            int mondayMonth = dataManager.getMonday().getMonth() + 1;
            int fridayMonth = dataManager.getFriday().getMonth() + 1;
            JsonObject dataManagerSJSO = Json.createObjectBuilder()
                    .add(JSON_EXPORT_STARTING_MONDAY_MONTH, mondayMonth + "")
                    .add(JSON_EXPORT_STARTING_MONDAY_DAY, dataManager.getMonday().getDate() + "")
                    .add(JSON_EXPORT_ENDING_FRIDAY_MONTH, fridayMonth + "")
                    .add(JSON_EXPORT_ENDING_FRIDAY_DAY, dataManager.getFriday().getDate() + "")
                    .add(JSON_EXPORT_HOLIDAYS, holidaysArray)
                    .add(JSON_EXPORT_LECTURES, lecturesArray)
                    .add(JSON_EXPORT_RECITATIONS, recitationsItemArray)
                    .add(JSON_EXPORT_HWS, hwsArray)
                    .build();
            // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
            Map<String, Object> sProperties = new HashMap<>(1);
            sProperties.put(JsonGenerator.PRETTY_PRINTING, true);
            JsonWriterFactory sWriterFactory = Json.createWriterFactory(sProperties);
            StringWriter ssw = new StringWriter();
            JsonWriter sJsonWriter = sWriterFactory.createWriter(ssw);
            sJsonWriter.writeObject(dataManagerSJSO);
            sJsonWriter.close();

            // INIT THE WRITER
            OutputStream sos = new FileOutputStream(filePath + "/public_html/js/ScheduleData.json");
            JsonWriter sJsonFileWriter = Json.createWriter(sos);
            sJsonFileWriter.writeObject(dataManagerSJSO);
            String sPrettyPrinted = ssw.toString();
            PrintWriter spw = new PrintWriter(filePath + "/public_html/js/ScheduleData.json");
            spw.write(sPrettyPrinted);
            spw.close();
            JsonArrayBuilder teamArrayBuilder = Json.createArrayBuilder();
            ObservableList<Team> teams = dataManager.getTeams();
            for (Team t : teams) {
                JsonObject teamJson = Json.createObjectBuilder()
                        .add(JSON_EXPORT_TEAM_NAME, t.getName())
                        .add(JSON_EXPORT_RED, t.getRed())
                        .add(JSON_EXPORT_GREEN, t.getGreen())
                        .add(JSON_EXPORT_BLUE, t.getBlue())
                        .add(JSON_TEXT_COLOR, t.getTextC()).build();
                teamArrayBuilder.add(teamJson);
            }
            JsonArray teamsArray = teamArrayBuilder.build();
            JsonArrayBuilder studentArrayBuilder = Json.createArrayBuilder();
            ObservableList<Student> students = dataManager.getStudents();
            for (Student s : students) {
                JsonObject studentJson = Json.createObjectBuilder()
                        .add(JSON_EXPORT_LAST_NAME, s.getLastName())
                        .add(JSON_EXPORT_FIRST_NAME, s.getFirstName())
                        .add(JSON_TEAM, s.getTeam())
                        .add(JSON_ROLE, s.getRole()).build();
                studentArrayBuilder.add(studentJson);
            }
            JsonArray studentsArray = studentArrayBuilder.build();
            JsonObject dataManagerPJSO = Json.createObjectBuilder()
                    .add(JSON_TEAMS, teamsArray)
                    .add(JSON_STUDENTS, studentsArray)
                    .build();
            // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
            Map<String, Object> pProperties = new HashMap<>(1);
            pProperties.put(JsonGenerator.PRETTY_PRINTING, true);
            JsonWriterFactory pWriterFactory = Json.createWriterFactory(pProperties);
            StringWriter psw = new StringWriter();
            JsonWriter pJsonWriter = pWriterFactory.createWriter(psw);
            pJsonWriter.writeObject(dataManagerPJSO);
            pJsonWriter.close();

            // INIT THE WRITER
            OutputStream pos = new FileOutputStream(filePath + "/public_html/js/TeamsAndStudents.json");
            JsonWriter pJsonFileWriter = Json.createWriter(pos);
            pJsonFileWriter.writeObject(dataManagerPJSO);
            String pPrettyPrinted = psw.toString();
            PrintWriter ppw = new PrintWriter(filePath + "/public_html/js/TeamsAndStudents.json");
            ppw.write(pPrettyPrinted);
            ppw.close();

            JsonArrayBuilder projectArrayBuilder = Json.createArrayBuilder();
            ObservableList<Team> projects = dataManager.getTeams();
            ObservableList<Student> projectsStudent = dataManager.getStudents();
            for (Team t : projects) {
                JsonArrayBuilder studentsJson = Json.createArrayBuilder();
                for (Student s : projectsStudent) {
                    if (s.getTeam().equals(t.getName())) {
                        studentsJson.add(s.getFullName());
//                        System.out.println(s.getFullName());
                    }
                }
                JsonObject teamsJson = Json.createObjectBuilder()
                        .add(JSON_EXPORT_TEAM_NAME, t.getName())
                        .add(JSON_STUDENTS, studentsJson)
                        .add(JSON_LINK, t.getLink()).build();
                projectArrayBuilder.add(teamsJson);
            }
            JsonArray projectsArray = projectArrayBuilder.build();
            JsonArrayBuilder workArrayBuilder = Json.createArrayBuilder();
            JsonObject workJson = Json.createObjectBuilder()
                    .add(JSON_SEMESTER, dataManager.getSemester())
                    .add(JSON_EXPORT_PROJECTS, projectsArray).build();
            workArrayBuilder.add(workJson);
            JsonArray worksArray = workArrayBuilder.build();
            JsonObject dataManagerWJSO = Json.createObjectBuilder()
                    .add(JSON_EXPORT_WORK, worksArray)
                    .build();
            // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
            Map<String, Object> wProperties = new HashMap<>(1);
            wProperties.put(JsonGenerator.PRETTY_PRINTING, true);
            JsonWriterFactory wWriterFactory = Json.createWriterFactory(wProperties);
            StringWriter wsw = new StringWriter();
            JsonWriter wJsonWriter = wWriterFactory.createWriter(wsw);
            wJsonWriter.writeObject(dataManagerWJSO);
            wJsonWriter.close();

            // INIT THE WRITER
            OutputStream wos = new FileOutputStream(filePath + "/public_html/js/ProjectsData.json");
            JsonWriter wJsonFileWriter = Json.createWriter(wos);
            wJsonFileWriter.writeObject(dataManagerWJSO);
            String wPrettyPrinted = wsw.toString();
            PrintWriter wpw = new PrintWriter(filePath + "/public_html/js/ProjectsData.json");
            wpw.write(wPrettyPrinted);
            wpw.close();
        } catch (ParseException ex) {
            Logger.getLogger(TAFiles.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
