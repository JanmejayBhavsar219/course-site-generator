/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.test_bed;

import csg.TAManagerApp;
import csg.data.CourseDetail;
import csg.data.Recitation;
import csg.data.ScheduleItem;
import csg.data.Student;
import csg.data.TAData;
import csg.data.TeachingAssistant;
import csg.data.Team;
import csg.file.TAFiles;
import csg.file.TimeSlot;
import csg.style.TAStyle;
import csg.workspace.TAWorkspace;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author jay
 */
public class TestSave {
    
    public TestSave(){
        
    }
    public String setCourse(){
        String course = "CSE";
        return course;
    }
    public String setNumber(){
        String number = "219";
        return number;
    }
    public String setSemester(){
        String semester = "Spring";
        return semester;
    }
    public String setYear(){
        String year = "2017";
        return year;
    }
    public String setTitle(){
        String title = "Computer Science III";
        return title;
    }
    public String setInstructorName(){
        String instructorName = "Richard McKenna";
        return instructorName;
    }
    public String setInstructorHome(){
        String instructorHome = "http://www.cs.stonybrook.edu/~richard";
        return instructorHome;
    }
    
    public String setExportedDirectory(){
        String exportDirectory = "..\\..\\..\\Courses\\CSE219\\Summer2017\\public";
        return exportDirectory;
    }
    
    public String setTemplpateDirectory(){
        String templateDirectory = ".\\templates\\CSE219";
        return templateDirectory;
    }
    
    public ObservableList setSitePages(){
        ObservableList<CourseDetail> dummySitePages = FXCollections.observableArrayList(
                new CourseDetail(setHomeSelected(), "Home", "index.html", "HomeBuilder.js"),
                new CourseDetail(setSyllabusSelected(), "Syllabus", "syllabus.html", "SyllabusBuilder.js"),
                new CourseDetail(setScheduleSelected(), "Schedule", "schedule.html", "ScheduleBuilder.js"),
                new CourseDetail(setHWsSelected(), "HWs", "hws.html", "HWsBuilder.js"),
                new CourseDetail(setProjectsSelected(), "Projects", "projects.html", "ProjectsBuilder.js")
        );
        return dummySitePages;
    }
    
    public boolean setHomeSelected(){
        boolean homeSelected = true;
        return homeSelected;
    }
    
    public boolean setSyllabusSelected(){
        boolean syllabusSelected = true;
        return syllabusSelected;
    }
    
    public boolean setScheduleSelected(){
        boolean scheduleSelected = true;
        return scheduleSelected;
    }
    
    public boolean setHWsSelected(){
        boolean hwsSelected = true;
        return hwsSelected;
    }
    
    public boolean setProjectsSelected(){
        boolean projectsSelected = true;
        return projectsSelected;
    }
    
    public String setBannerSchoolImage(){
        String bannerSchoolImage = "http://www3.cs.stonybrook.edu/~cse219/Section02/images/SBUDarkRedShieldLogo.png";
        return bannerSchoolImage;
    }
    
    public String setLeftFooterImage(){
        String leftFooterImage = "http://www3.cs.stonybrook.edu/~cse219/Section02/images/SBUWhiteShieldLogo.jpg";
        return leftFooterImage;
    }
    
    public String setRightFooterImage(){
        String rightFooterImage = "http://www3.cs.stonybrook.edu/~cse219/Section02/images/CSLogo.png";
        return rightFooterImage;
    }
    
    public String setStylesheet(){
        String stylesheet = "sea_wolf.css";
        return stylesheet;
    }
    
    public ObservableList<TeachingAssistant> setTeachingAssistants(){
        ObservableList<TeachingAssistant> dummyTeachingAssistants = FXCollections.observableArrayList(
                new TeachingAssistant(true, "Dhruv Bhawsar", "dhmi.bhch@gmail.com"),
                new TeachingAssistant(false, "Jane Doe", "jane.doe@yahoo.com"),
                new TeachingAssistant(true, "Janmejay Bhavsar", "janmejay.bhaw@gmail.com"),
                new TeachingAssistant(true, "Joe Shmo", "joe.shmo@yale.edu"),
                new TeachingAssistant(true, "John Doe", "john.doe@yahoo.com")
        );
        return dummyTeachingAssistants;
    }
    
    public ArrayList<TimeSlot> setOfficeHours(){
        ArrayList<TimeSlot> officeHours = new ArrayList<>();
        officeHours.add(new TimeSlot("MONDAY", "10_00am", "Jane Doe"));
        officeHours.add(new TimeSlot("MONDAY", "10_30am", "Jane Doe"));
        officeHours.add(new TimeSlot("TUESDAY", "9_00am", "Joe Shmo"));
        officeHours.add(new TimeSlot("TUESDAY", "9_30am", "Joe Shmo"));
        officeHours.add(new TimeSlot("WEDNESDAY", "11_00am", "Jane Doe"));
        officeHours.add(new TimeSlot("WEDNESDAY", "11_30am", "Jane Doe"));
        officeHours.add(new TimeSlot("WEDNESDAY", "12_00pm", "Jane Doe"));
        officeHours.add(new TimeSlot("THURSDAY", "9_00am", "Joe Shmo"));
        officeHours.add(new TimeSlot("THURSDAY", "9_30am", "Joe Shmo"));
        officeHours.add(new TimeSlot("FRIDAY", "12_00pm", "Jane Doe"));
        officeHours.add(new TimeSlot("FRIDAY", "12_30pm", "Jane Doe"));
        return officeHours;
    }
    
    public ObservableList<Recitation> setRecitations(){
        ObservableList<Recitation> dummyRecitations = FXCollections.observableArrayList(
                new Recitation("R02", "McKenna", "Wed 3:30pm-4:23pm", "Old CS 2114", "Jane Doe", "Joe Shmo"),
                new Recitation("R05", "Banerjee", "Tues 5:30pm-6:23pm", "Old CS 2114", "Janmejay Bhavsar", "Dhruv Bhawsar")
        );
        return dummyRecitations;
    }
    public LocalDate setStartingMondayDate(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        String mondayDate = "01/23/2017";
        LocalDate monday = LocalDate.parse(mondayDate, formatter);
        return monday;
    }
    public LocalDate setEndingFridayDate(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        String fridayDate = "04/28/2017";
        LocalDate friday = LocalDate.parse(fridayDate, formatter);
        return friday;
    }
    public ObservableList<ScheduleItem> setScheduleItems() throws ParseException{
        ObservableList<ScheduleItem> dummySchedules = FXCollections.observableArrayList(
                new ScheduleItem("Holiday", "02/09/2017", "Snow Day", ""),
                new ScheduleItem("Lecture", "02/14/2017", "Lecture 3", "Event Programming"),
                new ScheduleItem("Holiday", "03/13/2017", "Spring Break", ""),
                new ScheduleItem("Recitation", "02/01/2017", "Recitation 1", "NetBeans & Version Control")
        );
        ScheduleItem hw = new ScheduleItem("HW", "03/27/2017", "HW3", "UML");
        hw.setTime("11:59pm");
        hw.setCriteria("www.google.com");
        dummySchedules.add(hw);
        return dummySchedules;
    }
    public ObservableList<Team> setTeams(){
        ObservableList<Team> dummyTeams = FXCollections.observableArrayList(
                new Team("Atomic Comics", "552211", "000000", "http://atomiccomic.com"),
                new Team("C4 Comics", "235399", "000000", "http://c4-comics.appshot.com")
        );
        return dummyTeams;
    }
    public ObservableList<Student> setStudents(){
        ObservableList<Student> dummyStudents = FXCollections.observableArrayList(
                new Student("Beau", "Brummell", "Atomic Comics", "Lead Designer"),
                new Student("Jane", "Doe", "C4 Comics", "Lead Programmer"),
                new Student("Noonian", "Soong", "Atomic Comics", "Data Designer"),
                new Student("Janmejay", "Bhavsar", "Atomic Comics", "Lead Programmer"),
                new Student("Dhruv", "Bhawsar", "Atomic Comics", "Project Manager"),
                new Student("John", "Doe", "C4 Comics", "Project Manager"),
                new Student("Ezio", "Auditore", "C4 Comics", "Lead Designer"),
                new Student("Yusuf", "Tazim", "C4 Comics", "Data Designer")
        );
        return dummyStudents;
    }
    
    public static void main(String[] args) throws IOException, ParseException{
        TAManagerApp app = new TAManagerApp();
        app.loadProperties("app_properties.xml");
        TAData data = new TAData(app);
        TAFiles file = new TAFiles(app);
        TestSave dummySave = new TestSave();
        
        data.setSubject(dummySave.setCourse());
        data.setNumber(dummySave.setNumber());
        data.setSemester(dummySave.setSemester());
        data.setYear(dummySave.setYear());
        data.setTitle(dummySave.setTitle());
        data.setInstructorName(dummySave.setInstructorName());
        data.setInstructorHome(dummySave.setInstructorHome());
        data.setExportedDirectory(dummySave.setExportedDirectory());
        data.setTemplateDirectory(dummySave.setTemplpateDirectory());
        data.setSitePages(dummySave.setSitePages());
        data.setBannerSchoolImage(dummySave.setBannerSchoolImage());
        data.setLeftFooterImage(dummySave.setLeftFooterImage());
        data.setRightFooterImage(dummySave.setRightFooterImage());
        data.setStylesheet(dummySave.setStylesheet());
        data.setStartHour(9);
        data.setEndHour(20);
        data.setTeachingAssistants(dummySave.setTeachingAssistants());
        data.setOfficeHoursTimeSlots(dummySave.setOfficeHours());
        data.setRecitations(dummySave.setRecitations());
        data.setStartingMonday(dummySave.setStartingMondayDate());
        data.setEndingFriday(dummySave.setEndingFridayDate());
        data.setSchedules(dummySave.setScheduleItems());
        data.setTeams(dummySave.setTeams());
        data.setStudents(dummySave.setStudents());
        file.saveData(data, "/Users/jay/Desktop/Jay Work/CSE 219 Homework 5/CourseSiteGeneratorApp/work/SiteSaveTest.json");
    }
    
}
