package csg.style;

import djf.AppTemplate;
import djf.components.AppStyleComponent;
import java.util.HashMap;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import csg.data.TeachingAssistant;
import csg.workspace.CourseDetailWorkspace;
import csg.workspace.ProjectWorkspace;
import csg.workspace.RecitationWorkspace;
import csg.workspace.ScheduleWorkspace;
import csg.workspace.TAWorkspace;

/**
 * This class manages all CSS style for this application.
 * 
 * @author Richard McKenna
 * @co-author Janmejay Bhavsar
 * @version 1.0
 */
public class TAStyle extends AppStyleComponent {
    // FIRST WE SHOULD DECLARE ALL OF THE STYLE TYPES WE PLAN TO USE
    //COURSE DETAIL TAB
    public static String COURSE_INFO_LABEL = "course_info_label";
    public static String COURSE_PANE = "course_pane";
    public static String COURSE_INFO_PANE_BUTTON = "course_info_pane_button";
    public static String SITE_TEMPLATE_LABEL = "site_template_label";
    public static String SITE_TEMPLATE_PANE = "site_template_pane";
    public static String SITE_TEMPLATE_PANE_BUTTON = "site_template_pane_button";
    public static String PAGE_STYLE_LABEL = "page_style_label";
    public static String PAGE_STYLE_PANE = "page_style_pane";
    public static String PAGE_STYLE_PANE_BUTTONS = "page_style_pane_buttons";
    //RECITATION DATA TAB
    public static String RECITATION_LABEL = "recitation_label";
    public static String RECITATION_HEADER_BOX = "recitation_header_box";
    public static String RECITATION_TAB_PANES = "recitation_tab_panes";
    public static String REICTATION_BUTTONS = "recitation_buttons";
    public static String ADD_EDIT_LABEL = "add_edit_label";
    public static String ADD_EDIT_BOX = "add_edit_box";
    //SCHEDULE DATA TAB
    public static String SCHEDULE_HEADER_LABEL = "schedule_header_label";
    public static String SCHEDULE_HEADER_BOX = "schedule_header_box";
    public static String CALENDAR_BOX = "calendar_box";
    public static String SCHEDULE_ITEMS_PANE = "schedule_items_pane";
    public static String SCHEDULE_PANE_BUTTONS = "schedule_pane_buttons";
    public static String CALENDAR_BOUNDARIES_LABEL = "calendar_boundaries_label";
    public static String CALENDAR_BOUNDARIES_BOX = "calendar_boundaries_box";
    public static String SCHEDULE_ITEMS_LABEL = "schedule_items_label";
    public static String SCHEDULE_ITEMS_HEADER_BOX = "schedule_items_header_box";
    //PROJECT DATA TAB
    public static String PROJECT_HEADER_LABEL = "project_header_label";
    public static String PROJECT_HEADER_BOX = "project_header_box";
    public static String TEAM_PANE = "team_pane";
    public static String STUDENT_PANE = "student_pane";
    public static String PROJECT_PANE_BUTTONS = "project_pane_buttons";
    public static String TEAMS_LABEL = "teams_label";
    public static String TEAM_HEADER_BOX = "team_header_box";
    public static String STUDENTS_LABEL = "students_label";
    public static String STUDENT_HEADER_BOX = "student_header_box";
    // WE'LL USE THIS FOR ORGANIZING LEFT AND RIGHT CONTROLS
    public static String CLASS_PLAIN_PANE = "plain_pane";
    
    // THESE ARE THE HEADERS FOR EACH SIDE
    public static String CLASS_HEADER_PANE = "header_pane";
    public static String CLASS_HEADER_LABEL = "header_label";

    // ON THE LEFT WE HAVE THE TA ENTRY
    public static String CLASS_TA_TABLE = "ta_table";
    public static String CLASS_TA_TABLE_COLUMN_HEADER = "ta_table_column_header";
    public static String CLASS_ADD_TA_PANE = "add_ta_pane";
    public static String CLASS_ADD_TA_TEXT_FIELD = "add_ta_text_field";
    public static String CLASS_ADD_TA_BUTTON = "add_ta_button";
    public static String CLASS_UPDATE_TA_BUTTON="update_ta_button";
    public static String CLASS_CLEAR_BUTTON="clear_button"; 
    public static String CLASS_CLEAR_BUTTON_1="clear_button_1"; 
    public static String CLASS_REMOVE_BUTTON = "remove_button";

    // ON THE RIGHT WE HAVE THE OFFICE HOURS GRID
    public static String CLASS_OFFICE_HOURS_GRID = "office_hours_grid";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_PANE = "office_hours_grid_time_column_header_pane";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_LABEL = "office_hours_grid_time_column_header_label";
    public static String CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_PANE = "office_hours_grid_day_column_header_pane";
    public static String CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_LABEL = "office_hours_grid_day_column_header_label";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_CELL_PANE = "office_hours_grid_time_cell_pane";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_CELL_LABEL = "office_hours_grid_time_cell_label";
    public static String CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE = "office_hours_grid_ta_cell_pane";
    public static String CLASS_OFFICE_HOURS_GRID_TA_CELL_LABEL = "office_hours_grid_ta_cell_label";
    public static String CLASS_OFFICE_HOURS_GRID_NEW_START_TIME="office_hours_grid_new_start_time"; 
    public static String CLASS_OFFICE_HOURS_GRID_NEW_END_TIME="office_hours_grid_new_end_time";   
    public static String CLASS_OFFICE_HOURS_GRID_UPDATE_TIME_BUTTON="office_hours_grid_update_time_button";  

    // FOR HIGHLIGHTING CELLS, COLUMNS, AND ROWS
    public static String CLASS_HIGHLIGHTED_GRID_CELL = "highlighted_grid_cell";
    public static String CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN = "highlighted_grid_row_or_column";
    
    // THIS PROVIDES ACCESS TO OTHER COMPONENTS
    private final AppTemplate app;
    
    /**
     * This constructor initializes all style for the application.
     * 
     * @param initApp The application to be stylized.
     */
    public TAStyle(AppTemplate initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // LET'S USE THE DEFAULT STYLESHEET SETUP
        super.initStylesheet(app);

        // INIT THE STYLE FOR THE FILE TOOLBAR
        app.getGUI().initFileToolbarStyle();

        // AND NOW OUR WORKSPACE STYLE
        initTAWorkspaceStyle();
        
        initCourseDetailsTabStyle();
        
        initRecitationDataTabStyle();
        
        initScheduleDataTabStyle();
        
        initProjectDataTabStyle();
    }

    /**
     * This function specifies all the style classes for
     * all user interface controls in the workspace.
     */
    private void initTAWorkspaceStyle() {
        // LEFT SIDE - THE HEADER
        TAWorkspace workspaceComponent = (TAWorkspace)app.getWorkspaceComponent();
        workspaceComponent.getTAsHeaderBox().getStyleClass().add(CLASS_HEADER_PANE);
        workspaceComponent.getTAsHeaderLabel().getStyleClass().add(CLASS_HEADER_LABEL);

        // LEFT SIDE - THE TABLE
        TableView<TeachingAssistant> taTable = workspaceComponent.getTATable();
        taTable.getStyleClass().add(CLASS_TA_TABLE);
        taTable.getColumns().forEach((tableColumn) -> {
            tableColumn.getStyleClass().add(CLASS_TA_TABLE_COLUMN_HEADER);
        });

        // LEFT SIDE - THE TA DATA ENTRY
        workspaceComponent.getAddBox().getStyleClass().add(CLASS_ADD_TA_PANE);
        workspaceComponent.getNameTextField().getStyleClass().add(CLASS_ADD_TA_TEXT_FIELD);
        workspaceComponent.getEmailTextField().getStyleClass().add(CLASS_ADD_TA_TEXT_FIELD);
        workspaceComponent.getAddButton().getStyleClass().add(CLASS_ADD_TA_BUTTON);
        workspaceComponent.getUpdateTaButton().getStyleClass().add(CLASS_UPDATE_TA_BUTTON); 
        workspaceComponent.getClearButton().getStyleClass().add(CLASS_CLEAR_BUTTON); 
        workspaceComponent.getClearButton1().getStyleClass().add(CLASS_CLEAR_BUTTON_1); 
        workspaceComponent.getRemoveTAButton().getStyleClass().add(CLASS_REMOVE_BUTTON);
        

        // RIGHT SIDE - THE HEADER
        workspaceComponent.getOfficeHoursSubheaderBox().getStyleClass().add(CLASS_HEADER_PANE);
        workspaceComponent.getOfficeHoursSubheaderLabel().getStyleClass().add(CLASS_HEADER_LABEL);
    }
    
    /**
     * This method initializes the style for all UI components in
     * the office hours grid. Note that this should be called every
     * time a new TA Office Hours Grid is created or loaded.
     */
    public void initOfficeHoursGridStyle() {
        // RIGHT SIDE - THE OFFICE HOURS GRID TIME HEADERS
        TAWorkspace workspaceComponent = (TAWorkspace)app.getWorkspaceComponent();
        workspaceComponent.getOfficeHoursGridPane().getStyleClass().add(CLASS_OFFICE_HOURS_GRID);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTimeHeaderPanes(), CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_PANE);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTimeHeaderLabels(), CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_LABEL);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridDayHeaderPanes(), CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_PANE);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridDayHeaderLabels(), CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_LABEL);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTimeCellPanes(), CLASS_OFFICE_HOURS_GRID_TIME_CELL_PANE);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTimeCellLabels(), CLASS_OFFICE_HOURS_GRID_TIME_CELL_LABEL);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTACellPanes(), CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTACellLabels(), CLASS_OFFICE_HOURS_GRID_TA_CELL_LABEL);
        workspaceComponent.getNewEndBox().getStyleClass().add( CLASS_OFFICE_HOURS_GRID_NEW_END_TIME); 
        workspaceComponent.getNewStartBox().getStyleClass().add(CLASS_OFFICE_HOURS_GRID_NEW_START_TIME);
        workspaceComponent.getChangeTimeButton().getStyleClass().add( CLASS_OFFICE_HOURS_GRID_UPDATE_TIME_BUTTON); 
    }
    
    private void initCourseDetailsTabStyle(){
        CourseDetailWorkspace cdw = CourseDetailWorkspace.getCourseDetailWorkspace();
        cdw.getCourseInfoLabel().getStyleClass().add(COURSE_INFO_LABEL);
        cdw.getCoursePane().getStyleClass().add(COURSE_PANE);
        cdw.getChangeExportDirectoryButton().getStyleClass().add(COURSE_INFO_PANE_BUTTON);
        cdw.getSiteTemplateLabel().getStyleClass().add(SITE_TEMPLATE_LABEL);
        cdw.getSiteTemplateVBox().getStyleClass().add(SITE_TEMPLATE_PANE);
        cdw.getSelectTemplateDirectoryButton().getStyleClass().add(SITE_TEMPLATE_PANE_BUTTON);
        cdw.getPageStyleLabel().getStyleClass().add(PAGE_STYLE_LABEL);
        cdw.getPageStylePane().getStyleClass().add(PAGE_STYLE_PANE);
        cdw.getChangeBannerSchoolImageButton().getStyleClass().add(PAGE_STYLE_PANE_BUTTONS);
        cdw.getChangeLeftFooterImageButton().getStyleClass().add(PAGE_STYLE_PANE_BUTTONS);
        cdw.getChangeRightFooterImageButton().getStyleClass().add(PAGE_STYLE_PANE_BUTTONS);
    }
    
    private void initRecitationDataTabStyle(){
        RecitationWorkspace rw = RecitationWorkspace.getRecitationWorkspace();
        rw.getRecitationLabel().getStyleClass().add(RECITATION_LABEL);
        rw.getRecitationHeaderHBox().getStyleClass().add(RECITATION_HEADER_BOX);
        rw.getRecitationTableVBox().getStyleClass().add(RECITATION_TAB_PANES);
        rw.getAdd_editPane().getStyleClass().add(RECITATION_TAB_PANES);
        rw.getRemoveRecitationButton().getStyleClass().add(REICTATION_BUTTONS);
        rw.getAddButton().getStyleClass().add(REICTATION_BUTTONS);
        rw.getClearButton().getStyleClass().add(REICTATION_BUTTONS);
        rw.getAdd_EditLabel().getStyleClass().add(ADD_EDIT_LABEL);
        rw.getAdd_EditBox().getStyleClass().add(ADD_EDIT_BOX);
    }
    
    private void initScheduleDataTabStyle(){
        ScheduleWorkspace sw = ScheduleWorkspace.getScheduleWorkspace();
        sw.getScheduleLabel().getStyleClass().add(SCHEDULE_HEADER_LABEL);
        sw.getScheduleBox().getStyleClass().add(SCHEDULE_HEADER_BOX);
        sw.getCalendarBox().getStyleClass().add(CALENDAR_BOX);
        sw.getAdd_editSchedulePane().getStyleClass().add(SCHEDULE_ITEMS_PANE);
        sw.getAddScheduleButton().getStyleClass().add(SCHEDULE_PANE_BUTTONS);
        sw.getClearScheduleButton().getStyleClass().add(SCHEDULE_PANE_BUTTONS);
        sw.getRemoveScheduleButton().getStyleClass().add(SCHEDULE_PANE_BUTTONS);
        sw.getCalendarBoundariesLabel().getStyleClass().add(CALENDAR_BOUNDARIES_LABEL);
        sw.getCalendarBoundariesBox().getStyleClass().add(CALENDAR_BOUNDARIES_BOX);
        sw.getAdd_EditScheduleLabel().getStyleClass().add(ADD_EDIT_LABEL);
        sw.getScheduleItemsLabel().getStyleClass().add(SCHEDULE_ITEMS_LABEL);
        sw.getScheduleItemsHeaderBox().getStyleClass().add(SCHEDULE_ITEMS_HEADER_BOX);
    }
    
    private void initProjectDataTabStyle(){
        ProjectWorkspace pw = ProjectWorkspace.getProjectWorkspace();
        pw.getProjectsLabel().getStyleClass().add(PROJECT_HEADER_LABEL);
        pw.getProjectsHeaderBox().getStyleClass().add(PROJECT_HEADER_BOX);
        pw.getTeamsPane().getStyleClass().add(TEAM_PANE);
        pw.getStudentsPane().getStyleClass().add(STUDENT_PANE);
        pw.getRemoveTeamButton().getStyleClass().add(PROJECT_PANE_BUTTONS);
        pw.getAddTeamButton().getStyleClass().add(PROJECT_PANE_BUTTONS);
        pw.getClearTeamButton().getStyleClass().add(PROJECT_PANE_BUTTONS);
        pw.getRemoveStudentButton().getStyleClass().add(PROJECT_PANE_BUTTONS);
        pw.getAddStudentButton().getStyleClass().add(PROJECT_PANE_BUTTONS);
        pw.getClearStudentButton().getStyleClass().add(PROJECT_PANE_BUTTONS);
        pw.getAdd_EditTeamLabel().getStyleClass().add(ADD_EDIT_LABEL);
        pw.getTeamsLabel().getStyleClass().add(TEAMS_LABEL);
        pw.getTeamHeaderBox().getStyleClass().add(TEAM_HEADER_BOX);
        pw.getAdd_EditStudentLabel().getStyleClass().add(ADD_EDIT_LABEL);
        pw.getStudentsLabel().getStyleClass().add(STUDENTS_LABEL);
        pw.getStudentHeaderBox().getStyleClass().add(STUDENT_HEADER_BOX);
    }
    
    /**
     * This helper method initializes the style of all the nodes in the nodes
     * map to a common style, styleClass.
     */
    private void setStyleClassOnAll(HashMap nodes, String styleClass) {
        for (Object nodeObject : nodes.values()) {
            Node n = (Node)nodeObject;
            n.getStyleClass().add(styleClass);
        }
    }
}