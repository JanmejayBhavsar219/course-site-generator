/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Janmejay Bhavsar
 * @param <E>
 */
public class Recitation<E extends Comparable<E>> implements Comparable<E>{
    private final StringProperty section;
    private final StringProperty instructor;
    private final StringProperty day_time;
    private final StringProperty location;
    private final StringProperty TA1;
    private final StringProperty TA2;
    
    public Recitation(String initSection, String initInstructor, String initDay_Time, String initLocation, String initTA1, String initTA2){
        section = new SimpleStringProperty(initSection);
        instructor = new SimpleStringProperty(initInstructor);
        day_time = new SimpleStringProperty(initDay_Time);
        location = new SimpleStringProperty(initLocation);
        TA1 = new SimpleStringProperty(initTA1);
        TA2 = new SimpleStringProperty(initTA2);
    }
    
    public StringProperty sectionProperty() {
        return section;
    }
    
    public String getSection(){
        return section.get();
    }
    
    public void setSection(String newSection){
        section.set(newSection);
    }

    public StringProperty instructorProperty() {
        return instructor;
    }
    
    public String getInstructor(){
        return instructor.get();
    }
    
    public void setInstructor(String newInstructor){
        instructor.set(newInstructor);
    }

    public StringProperty day_timeProperty() {
        return day_time;
    }
    
    public String getDay_Time(){
        return day_time.get();
    }
    
    public void setDay_Time(String newDay_Time){
        day_time.set(newDay_Time);
    }

    public StringProperty locationProperty() {
        return location;
    }
    
    public String getLocation(){
        return location.get();
    }
    
    public void setLocation(String newLocation){
        location.set(newLocation);
    }

    public StringProperty TA1Property() {
        return TA1;
    }
    
    public String getTA1(){
        return TA1.get();
    }
    
    public void setTA1(String newTA1){
        TA1.set(newTA1);
    }

    public StringProperty TA2Property() {
        return TA2;
    }
    
    public String getTA2(){
        return TA2.get();
    }
    
    public void setTA2(String newTA2){
        TA2.set(newTA2);
    }
    
    @Override
    public String toString(){
        return section.getValue();
    }

    @Override
    public int compareTo(E otherRecitation) {
        return getSection().compareTo(((Recitation)otherRecitation).getSection());
    }
}
