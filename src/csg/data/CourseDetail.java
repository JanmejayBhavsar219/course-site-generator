/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Janmejay Bhavsar
 */
public class CourseDetail {
    
    private final BooleanProperty use;
    private final StringProperty navBarTitle;  
    private final StringProperty fileName;
    private final StringProperty script;
    
    public CourseDetail(Boolean initUse, String initNavBarTitle, String initFileName, String initScript){
        use = new SimpleBooleanProperty(initUse);
        navBarTitle = new SimpleStringProperty(initNavBarTitle);
        fileName = new SimpleStringProperty(initFileName);
        script = new SimpleStringProperty(initScript);
    }
    
    public BooleanProperty useProperty(){
        return use;
    }
    
    public boolean use(){
        return use.get();
    }
    
    public StringProperty navBarTitleProperty() {
        return navBarTitle;
    }
    
    public String getNavbarTitle(){
        return navBarTitle.get();
    }

    public StringProperty fileNameProperty() {
        return fileName;
    }
    
    public String getFileName(){
        return fileName.get();
    }

    public StringProperty scriptProperty() {
        return script;
    }
    
    public String getScript(){
        return script.get();
    }
    public String toString(){
        return navBarTitle.getValue();
}
}
