/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.paint.Color;

/**
 *
 * @author Janmejay Bhavsar
 */
public class Team {
    private final StringProperty name;
    private final StringProperty color;
    private final StringProperty textColor;
    private final StringProperty link;
    
    Color rgb;
    String textC;
    String studentName;

    public Team(String initName, String initColor, String initTextColor, String initLink) {
        name = new SimpleStringProperty(initName);
        color = new SimpleStringProperty(initColor);
        textColor = new SimpleStringProperty(initTextColor);
        link = new SimpleStringProperty(initLink);
        rgb = Color.web("#"+initColor);
        if(initTextColor.equals("000000")){
            textC = "black";
        }
//        if(student.getTeam().equals(initName)){
//            studentName = student.getFullName();
//        }
    }

    public StringProperty nameProperty() {
        return name;
    }
    
    public String getName(){
        return name.get();
    }
    
    public void setName(String newName){
        name.set(newName);
    }

    public StringProperty colorProperty() {
        return color;
    }
    
    public String getColor(){
        return color.get();
    }
    
    public void setColor(String newColor){
        color.set(newColor);
    }

    public StringProperty textColorProperty() {
        return textColor;
    }
    
    public String getTextColor(){
        return textColor.get();
    }
    
    public void setTextColor(String newTextColor){
        textColor.set(newTextColor);
    }

    public StringProperty linkProperty() {
        return link;
    }
    
    public String getLink(){
        return link.get();
    }
    
    public void setLink(String newLink){
        link.set(newLink);
    }
    
    public String getRed(){
        int red = (int) (rgb.getRed()*256);
        return red+"";
    }
    
    public String getGreen(){
        int green = (int) (rgb.getGreen()*256);
        return green+"";
    }
    
    public String getBlue(){
        int blue = (int) (rgb.getBlue()*256);
        return blue+"";
    }
    
    public String getTextC(){
        return textC;
    }
    
//    public String getStudentName(){
//        return studentName;
//    }
    
    @Override
    public String toString(){
        return name.getValue();
}
}
