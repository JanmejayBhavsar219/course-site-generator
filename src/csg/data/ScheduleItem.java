/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Janmejay Bhavsar
 * @param <E>
 */
public class ScheduleItem<E extends Comparable<E>> implements Comparable<E> {
    private final StringProperty type;
    private final StringProperty date;
    private final StringProperty title;
    private final StringProperty topic;
    
    private Date scheduleDate;
    String time;
    String link;
    String criteria;

    public ScheduleItem(String initType, String initDate, String initTitle, String initTopic) throws ParseException {
        type = new SimpleStringProperty(initType);
        date = new SimpleStringProperty(initDate);
        title = new SimpleStringProperty(initTitle);
        topic = new SimpleStringProperty(initTopic);
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        scheduleDate = df.parse(initDate);
        time = "";
        link = "";
        criteria = "";
    }

    public StringProperty typeProperty() {
        return type;
    }
    
    public String getType(){
        return type.get();
    }
    
    public void setType(String newType){
        type.set(newType);
    }

    public StringProperty dateProperty() {
        return date;
    }
    
    public String getDate(){
        return date.get();
    }
    
    public void setDate(String newDate){
        date.set(newDate);
    }

    public StringProperty titleProperty() {
        return title;
    }
    
    public String getTitle(){
        return title.get();
    }
    
    public void setTitle(String newTitle){
        title.set(newTitle);
    }

    public StringProperty topicProperty() {
        return topic;
    }
    
    public String getTopic(){
        return topic.get();
    }
    
    public void setTopic(String newTopic){
        topic.set(newTopic);
    }
    
    public Date getScheduleDate(){
        return scheduleDate;
    }
    
    public void setScheduleDate(String newScheduleDate) throws ParseException{
         DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        scheduleDate = df.parse(newScheduleDate);
    }
    
    @Override
    public String toString(){
        return type.getValue();
    }
    
    public String getTime(){
        return time;
    }
    
    public void setTime(String newTime){
        this.time = newTime;
    }
    
    public String getLink(){
        return link;
    }
    
    public void setLink(String newLink){
        this.link = newLink;
    }
    
    public String getCriteria(){
        return criteria;
    }
    
    public void setCriteria(String newCriteria){
        this.criteria = newCriteria;
    }
    @Override
    public int compareTo(E otherScheduleItem) {
        return getTitle().compareTo(((ScheduleItem)otherScheduleItem).getTitle());
    }
    
}
