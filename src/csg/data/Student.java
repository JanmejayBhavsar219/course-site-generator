/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Janmejay Bhavsar
 */
public class Student {
    private final StringProperty firstName;
    private final StringProperty lastName;
    private final StringProperty team;
    private final StringProperty role;
    String fullName;
    
    public Student(String initFirstName, String initLastName, String initTeam, String initRole){
        firstName = new SimpleStringProperty(initFirstName);
        lastName = new SimpleStringProperty(initLastName);
        team = new SimpleStringProperty(initTeam);
        role = new SimpleStringProperty(initRole);
        fullName = initFirstName+" "+initLastName;
    }

    public StringProperty firstNameProperty() {
        return firstName;
    }
    
    public String getFirstName(){
        return firstName.get();
    }
    
    public void setFirstName(String newFirstName){
        firstName.set(newFirstName);
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }
    
    public String getLastName(){
        return lastName.get();
    }
    
    public void setLastName(String newLastName){
        lastName.set(newLastName);
    }

    public StringProperty teamProperty() {
        return team;
    }
    
    public String getTeam(){
        return team.get();
    }
    
    public void setTeam(String newTeam){
        team.set(newTeam);
    }

    public StringProperty roleProperty() {
        return role;
    }
    
    public String getFullName(){
    return fullName;
    }
    
    public String getRole(){
        return role.get();
    }
    
    public void setRole(String newRole){
        role.set(newRole);
    }
    
}
