/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;


import jtps.jTPS_Transaction;
import csg.TAManagerApp;
import csg.data.Recitation;
import csg.data.TAData;
import csg.data.TeachingAssistant;


/**
 *
 * @author khurr
 */
public class UpdateRecitation_Transaction implements jTPS_Transaction {

    private final String oldSection;
    private final String newSection;
    private final String oldInstructor;
    private final String newInstructor;
    private final String oldDay_Time;
    private final String newDay_Time;
    private final String oldLocation;
    private final String newLocation;
    private final String oldTA1;
    private final String newTA1;
    private final String oldTA2;
    private final String newTA2;
    private final TAData taData;
    private final Recitation rec;
    private final TAManagerApp app; 
    private final TAWorkspace transWorkspace; 
    private final RecitationWorkspace rWorkspace;

    public UpdateRecitation_Transaction(String orgSection, String section, String orgInstructor, String instructor, String orgDay_Time, String day_time, String orgLocation, String location, String orgTA1, String TA1, String orgTA2, String TA2, TAData data, TAManagerApp taApp, TAWorkspace workspace) {
        oldSection = orgSection;
        newSection = section;
        oldInstructor = orgInstructor;
        newInstructor = instructor;
        oldDay_Time = orgDay_Time;
        newDay_Time = day_time;
        oldLocation = orgLocation;
        newLocation = location;
        oldTA1 = orgTA1;
        newTA1 = TA1;
        oldTA2 = orgTA2;
        newTA2 = TA2;
        taData = data;
        rec = data.getRecitation(orgSection);
        app=taApp; 
        transWorkspace=workspace; 
        rWorkspace = RecitationWorkspace.getRecitationWorkspace();
    }

    @Override
    public void doTransaction() {  //Control Y 
        System.out.println("updateTA doTransaction ");
        Recitation r = taData.getRecitation(oldSection);
        r.setSection(newSection);
        r.setInstructor(newInstructor);
        r.setDay_Time(newDay_Time);
        r.setLocation(newLocation);
        r.setTA1(newTA1);
        r.setTA2(newTA2);
        rec.setSection(newSection);                        // MOVED TO TRANSACTION CASE 
        rec.setInstructor(newInstructor);
        rec.setDay_Time(newDay_Time);
        rec.setLocation(newLocation);
        rec.setTA1(newTA1);
        rec.setTA2(newTA2);
        rWorkspace.sectionTextField.setText(newSection);
        rWorkspace.instructorTextField.setText(newInstructor);
        rWorkspace.day_timeTextField.setText(newDay_Time);
        rWorkspace.locationTextField.setText(newLocation);
        rWorkspace.getSupervisingTAComboBox1().getSelectionModel().select(newTA1);
        rWorkspace.getSupervisingTAComboBox2().getSelectionModel().select(newTA2);
       // transWorkspace.taTable.refresh();

    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("updateTA undoTransaction ");
        Recitation r = taData.getRecitation(newSection);
        r.setSection(oldSection);
        r.setInstructor(oldInstructor);
        r.setDay_Time(oldDay_Time);
        r.setLocation(oldLocation);
        r.setTA1(oldTA1);
        r.setTA2(oldTA2);
        rec.setSection(oldSection);                        // MOVED TO TRANSACTION CASE 
        rec.setInstructor(oldInstructor);
        rec.setDay_Time(oldDay_Time);
        rec.setLocation(oldLocation);
        rec.setTA1(oldTA1);
        rec.setTA2(oldTA2);
        rWorkspace.sectionTextField.setText(oldSection);
        rWorkspace.instructorTextField.setText(oldInstructor);
        rWorkspace.day_timeTextField.setText(oldDay_Time);
        rWorkspace.locationTextField.setText(oldLocation);
        rWorkspace.getSupervisingTAComboBox1().getSelectionModel().select(oldTA1);
        rWorkspace.getSupervisingTAComboBox2().getSelectionModel().select(oldTA2);
        //transWorkspace.taTable.refresh();

    }

}
