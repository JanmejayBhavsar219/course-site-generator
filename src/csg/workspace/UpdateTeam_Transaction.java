/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;


import jtps.jTPS_Transaction;
import csg.TAManagerApp;
import csg.data.Recitation;
import csg.data.TAData;
import csg.data.TeachingAssistant;
import csg.data.Team;
import javafx.scene.paint.Color;


/**
 *
 * @author khurr
 */
public class UpdateTeam_Transaction implements jTPS_Transaction {

    private final String oldName;
    private final String newName;
    private final String oldColor;
    private final String newColor;
    private final String oldTextColor;
    private final String newTextColor;
    private final String oldLink;
    private final String newLink;
    private final TAData taData;
    private final Team team;
    private final TAManagerApp app; 
    private final TAWorkspace transWorkspace; 
    private final ProjectWorkspace pWorkspace;

    public UpdateTeam_Transaction(String orgName, String name, String orgColor, String color, String orgTextColor, String textColor, String orgLink, String link, TAData data, TAManagerApp taApp, TAWorkspace workspace) {
        oldName = orgName;
        newName = name;
        oldColor = orgColor;
        newColor = color;
        oldTextColor = orgTextColor;
        newTextColor = textColor;
        oldLink = orgLink;
        newLink = link;
        taData = data;
        team = data.getTeam(orgName);
        app=taApp; 
        transWorkspace=workspace; 
        pWorkspace = ProjectWorkspace.getProjectWorkspace();
    }

    @Override
    public void doTransaction() {  //Control Y 
        System.out.println("updateTA doTransaction ");
        Team t = taData.getTeam(oldName);
        t.setName(newName);
        t.setColor(newColor.substring(2, 8));
        t.setTextColor(newColor.substring(2, 8));
        t.setLink(newLink);
        team.setName(newName);                        // MOVED TO TRANSACTION CASE 
        team.setColor(newColor.substring(2, 8));
        team.setTextColor(newTextColor.substring(2, 8));
        team.setLink(newLink);
        pWorkspace.nameTextField.setText(newName);
        pWorkspace.colorPicker.setValue(Color.web(newColor.substring(2, 8)));
        pWorkspace.textColorPicker.setValue(Color.web(newTextColor.substring(2, 8)));
        pWorkspace.linkTextField.setText(newLink);
       // transWorkspace.taTable.refresh();

    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("updateTA undoTransaction ");
        Team t = taData.getTeam(newName);
        t.setName(oldName);
        t.setColor(oldColor);
        t.setTextColor(oldTextColor);
        t.setLink(oldLink);
        team.setName(oldName);                        // MOVED TO TRANSACTION CASE 
        team.setColor(oldColor);
        team.setTextColor(oldTextColor);
        team.setLink(oldLink);
        pWorkspace.nameTextField.setText(oldName);
        pWorkspace.colorPicker.setValue(Color.web("#"+oldColor));
        pWorkspace.textColorPicker.setValue(Color.web("#"+oldTextColor));
        pWorkspace.linkTextField.setText(oldLink);
        //transWorkspace.taTable.refresh();

    }

}
