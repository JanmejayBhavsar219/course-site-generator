/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import java.util.HashMap;
import javafx.beans.property.StringProperty;
import jtps.jTPS_Transaction;
import csg.data.TAData;

/**
 *
 * @author khurr
 */
public class updateTime_Transaction implements jTPS_Transaction {
    
    private final int start;
    private final int end;
    private final int orgStart;
    private final int orgEnd;
    private final TAData data;    
    private final HashMap<String, String> stringMap;    
    
    private final HashMap<String, StringProperty> officeHours;    
    
    public updateTime_Transaction(int start, int end, TAData data) {
        
        stringMap = new HashMap();        
        this.start = start;
        this.end = end;        
        this.data = data;        
        orgStart = data.getStartHour();
        orgEnd = data.getEndHour();
        officeHours = data.getOfficeHours();
        officeHours.entrySet().stream().map((entry) -> entry.getKey()).forEachOrdered((key) -> {
            StringProperty prop = officeHours.get(key);
            String cellText = prop.getValue();
            stringMap.put(key, cellText);
        });
        System.out.println("testStatment");
        
    }
    
    @Override
    public void doTransaction() {
        data.updateTime(start, end);
        
    }
    
    @Override
    public void undoTransaction() {
        data.updateTime(orgStart, orgEnd);
        stringMap.entrySet().stream().map((entry) -> entry.getKey()).forEachOrdered((key) -> {
            String cellText = stringMap.get(key);
            StringProperty prop = officeHours.get(key);            
            prop.setValue(cellText);
        });
        
    }
    
}
