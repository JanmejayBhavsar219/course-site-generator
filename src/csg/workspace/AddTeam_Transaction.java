/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.data.Recitation;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import csg.data.TAData;
import csg.data.TeachingAssistant;
import csg.data.Team;

/**
 *
 * @author khurr
 */
public class AddTeam_Transaction implements jTPS_Transaction {

    private final String teamName;
    private final String teamColor;
    private final String teamTextColor;
    private final String teamLink;
    private final TAData data;

    public AddTeam_Transaction(String name, String color, String textColor, String link, TAData taData) {
        teamName = name;
        teamColor = color;
        teamTextColor = textColor;
        teamLink = link;        
        data = taData;
    }

    @Override
    public void doTransaction() {  //Control Y 
        data.addTeam(teamName, teamColor, teamTextColor, teamLink);
        //Collections.sort(data.getTeachingAssistants());
        System.out.println("doTransaction");
    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("undo Transaction");
        ObservableList<Team> teamList = data.getTeams();
        for (Team t : teamList) {
            if (teamName.equals(t.getName())) {
                teamList.remove(t);
                return;
            }

        }
        // data.removeTA(taName);

    }

}
