/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import jtps.jTPS;
import properties_manager.PropertiesManager;
import csg.TAManagerApp;
import static csg.TAManagerProp.*;
import csg.data.CourseDetail;
import csg.data.TeachingAssistant;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.cell.CheckBoxTableCell;
import csg.test_bed.TestSave;

/**
 *
 * @author Janmejay Bhavsar
 */
public class CourseDetailWorkspace extends AppWorkspaceComponent {
    TAManagerApp app;
    public jTPS jTPSObject;
    static CourseDetailWorkspace singleton;
    Label courseInfoLabel;
    Label subjectLabel;
    ComboBox subjectComboBox;
    ObservableList subjectComboBoxList;
    Label numberLabel;
    ComboBox numberComboBox;
    ObservableList numberComboBoxList;
    Label semesterLabel;
    ComboBox semesterComboBox;
    ObservableList semesterComboBoxList;
    Label yearLabel;
    ComboBox yearComboBox;
    ObservableList yearComboBoxList;
    Label titleLabel;
    TextField titleTextField;
    Label instructorNameLabel;
    TextField instructorNameTextField;
    Label instructorHomeLabel;
    TextField instructorHomeTextField;
    Label exportDirectoryLabel;
    Label exportDirectorySelectedLabel;
    Button changeExportDirectoryButton;
    HBox subjectNumberHBox;
    HBox semesterYearHBox;
    VBox comboBoxVBox;
    FlowPane courseInfoPane;
    Label siteTemplateLabel;
    Label siteNoticeLabel;
    Label selectedTemplateLabel;
    Button selectTemplateDirectoryButton;
    Label sitePagesLabel;
    TableView sitePagesTable;
    TableColumn useColumn;
    CheckBox checkBox;
    TableColumn navBarTitleColumn;
    TableColumn fileNameColumn;
    TableColumn scriptColumn;
    boolean isHomeSelected;
    boolean isSyllabusSelected;
    boolean isScheduleSelected;
    boolean isHWsSelected;
    boolean isProjectsSelected;
    VBox siteTemplateVBox;
    Label pageStyleLabel;
    Label bannerSchoolImageLabel;
    Image bannerSchoolImage;
    ImageView bannerSchoolIV;
    Button changeBannerSchoolImageButton;
    Label leftFooterImageLabel;
    Image leftFooterImage;
    ImageView leftFooterIV;
    Button changeLeftFooterImageButton;
    Label rightFooterImageLabel;
    Image rightFooterImage;
    ImageView rightFooterIV;
    Button changeRightFooterImageButton;
    Label styleSheetLabel;
    ComboBox styleSheetComboBox;
    ObservableList styleSheetComboBoxList;
    Label noteLabel;
    GridPane coursePane;
    GridPane pageStylePane;
    VBox coursePanel;
//    ObservableList<CourseDetail> data;
    
    
    public CourseDetailWorkspace(){
       
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        courseInfoLabel = new Label(props.getProperty(COURSE_INFO_HEADER.toString()));
        subjectLabel = new Label(props.getProperty(SUBJECT_TEXT.toString()));
        subjectComboBoxList = FXCollections.observableArrayList("CSE", "AMS", "MAT", "PHY");
        subjectComboBox = new ComboBox(subjectComboBoxList);
        numberLabel = new Label(props.getProperty(NUMBER_TEXT.toString()));
        numberComboBoxList = FXCollections.observableArrayList("219", "301", "127", "126");
        numberComboBox = new ComboBox(numberComboBoxList);
        semesterLabel = new Label(props.getProperty(SEMESTER_TEXT.toString()));
        semesterComboBoxList = FXCollections.observableArrayList("Fall", "Spring");
        semesterComboBox = new ComboBox(semesterComboBoxList);
        yearLabel = new Label(props.getProperty(YEAR_TEXT.toString()));
        yearComboBoxList = FXCollections.observableArrayList("2017", "2018", "2019", "2020", "2021");
        yearComboBox = new ComboBox(yearComboBoxList);
        titleLabel = new Label(props.getProperty(TITLE_TEXT.toString()));
        titleTextField = new TextField();
        instructorNameLabel = new Label(props.getProperty(INSTRUCTOR_NAME_TEXT.toString()));
        instructorNameTextField = new TextField();
        instructorHomeLabel = new Label(props.getProperty(INSTRUCTOR_HOME_TEXT.toString()));
        instructorHomeTextField = new TextField();
        exportDirectoryLabel = new Label(props.getProperty(EXPORT_DIRECTORY_TEXT.toString()));
        exportDirectorySelectedLabel = new Label("..\\..\\..\\Courses\\CSE219\\Summer2017\\public");
        changeExportDirectoryButton = new Button(props.getProperty(CHANGE_BUTTON_TEXT.toString()));
        coursePane = new GridPane();
        coursePane.add(courseInfoLabel, 0, 0);
        coursePane.add(subjectLabel, 0, 1);
        coursePane.add(subjectComboBox, 1, 1);
        coursePane.add(numberLabel, 3, 1);
        coursePane.add(numberComboBox, 4, 1);
        coursePane.add(semesterLabel, 0, 2);
        coursePane.add(semesterComboBox, 1, 2);
        coursePane.add(yearLabel, 3, 2);
        coursePane.add(yearComboBox, 4, 2);
        coursePane.add(titleLabel, 0, 3);
        coursePane.add(titleTextField, 1, 3);
        coursePane.add(instructorNameLabel, 0, 4);
        coursePane.add(instructorNameTextField, 1, 4);
        coursePane.add(instructorHomeLabel, 0, 5);
        coursePane.add(instructorHomeTextField, 1, 5);
        coursePane.add(exportDirectoryLabel, 0, 6);
        coursePane.add(exportDirectorySelectedLabel, 1, 6);
        coursePane.add(changeExportDirectoryButton, 2, 6);
        GridPane.setMargin(subjectLabel, new Insets(0, 0, 10, 2));
        GridPane.setMargin(subjectComboBox, new Insets(0, 0, 10, 0));
        GridPane.setMargin(numberLabel, new Insets(0, 0, 10, 10));
        GridPane.setMargin(numberComboBox, new Insets(0, 0, 10, 25));
        GridPane.setMargin(semesterLabel, new Insets(0, 0, 10, 2));
        GridPane.setMargin(semesterComboBox, new Insets(0, 0, 10, 0));
        GridPane.setMargin(yearLabel, new Insets(0, 0, 10, 10));
        GridPane.setMargin(yearComboBox, new Insets(0, 0, 10, 25));
        GridPane.setMargin(titleLabel, new Insets(0, 0, 10, 2));
        GridPane.setMargin(titleTextField, new Insets(0, 0, 10, 0));
        GridPane.setMargin(instructorNameLabel, new Insets(0, 0, 10, 2));
        GridPane.setMargin(instructorNameTextField, new Insets(0, 0, 10, 0));
        GridPane.setMargin(instructorHomeLabel, new Insets(0, 0, 10, 2));
        GridPane.setMargin(instructorHomeTextField, new Insets(0, 0, 10, 0));
        GridPane.setMargin(exportDirectoryLabel, new Insets(0, 0, 10, 2));
        GridPane.setMargin(exportDirectorySelectedLabel, new Insets(0, 0, 10, 0));
        GridPane.setMargin(changeExportDirectoryButton, new Insets(0, 0, 10, 15));
        coursePane.setStyle("-fx-border-style: solid inside;"+"-fx-border-width: 2;");
        siteTemplateLabel = new Label(props.getProperty(SITE_TEMPLATE_HEADER.toString()));
        siteNoticeLabel = new Label(props.getProperty(SITE_TEMPLATE_NOTE_TEXT.toString()));
        selectedTemplateLabel = new Label(".\\templates\\CSE219");
        selectTemplateDirectoryButton = new Button(props.getProperty(SELECT_TEMPLATE_DIRECTORY_BUTTON_TEXT.toString()));
        sitePagesLabel = new Label(props.getProperty(SITE_PAGES_TEXT.toString()));
        sitePagesTable = new TableView();
        sitePagesTable.setMinHeight(75);
        sitePagesTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        TestSave ts = new TestSave();
        isHomeSelected = ts.setHomeSelected();
        isSyllabusSelected = ts.setSyllabusSelected();
        isScheduleSelected = ts.setScheduleSelected();
        isHWsSelected = ts.setHWsSelected();
        isProjectsSelected = ts.setProjectsSelected();
//        data =
//        FXCollections.observableArrayList(
//            new CourseDetail(isHomeSelected, "Home", "index.html", "HomeBuilder.js"),
//            new CourseDetail(isSyllabusSelected, "Syllabus", "syllabus.html", "SyllabusBuilder.js"),
//            new CourseDetail(isScheduleSelected, "Schedule", "schedule.html", "ScheduleBuilder.js"),
//            new CourseDetail(isHWsSelected, "HWs", "hws.html", "HWsBuilder.js"),
//            new CourseDetail(isProjectsSelected, "Projects", "projects.html", "ProjectsBuilder.js")
//        );
        //sitePagesTable.setItems(data);
        useColumn = new TableColumn<CourseDetail, CheckBox>(props.getProperty(USE_COLUMN_TEXT.toString()));
        navBarTitleColumn = new TableColumn(props.getProperty(NAVBAR_TITLE_COLUMN_TEXT.toString()));
        fileNameColumn = new TableColumn(props.getProperty(FILE_NAME_COLUMN_TEXT.toString()));
        scriptColumn = new TableColumn(props.getProperty(SCRIPT_COLUMN_TEXT.toString()));
        useColumn.setCellValueFactory(new PropertyValueFactory("use"));
        useColumn.setCellFactory(column -> new CheckBoxTableCell());
//        useColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<CourseDetail, CheckBox>, ObservableValue<CheckBox>>() {
//
//            @Override
//            public ObservableValue<CheckBox> call(
//                    TableColumn.CellDataFeatures<CourseDetail, CheckBox> arg0) {
//                CheckBox checkBox = new CheckBox();
//                return new SimpleObjectProperty<CheckBox>(checkBox);
//            }
//        });

        
        navBarTitleColumn.setCellValueFactory(
                new PropertyValueFactory<CourseDetail, String>("navBarTitle")
        );
        fileNameColumn.setCellValueFactory(
                new PropertyValueFactory<CourseDetail, String>("fileName")
        );
        scriptColumn.setCellValueFactory(
                new PropertyValueFactory<CourseDetail, String>("script")
        );
        sitePagesTable.getColumns().addAll(useColumn, navBarTitleColumn, fileNameColumn, scriptColumn);
        sitePagesTable.setEditable(true);
        siteTemplateVBox = new VBox();
        siteTemplateVBox.getChildren().addAll(siteTemplateLabel, siteNoticeLabel, selectedTemplateLabel, selectTemplateDirectoryButton, sitePagesLabel, sitePagesTable);
        siteTemplateVBox.setStyle("-fx-border-style: solid inside;"+"-fx-border-width: 2;");
        siteTemplateVBox.setSpacing(2);
        pageStyleLabel = new Label(props.getProperty(PAGE_STYLE_HEADER.toString()));
        bannerSchoolImageLabel = new Label(props.getProperty(BANNER_SCHOOL_IMAGE_TEXT.toString()));
        bannerSchoolImage = new Image("https://www.google.com/search?q=blank+image&espv=2&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiHh-qp1r3TAhWqzIMKHa01AnAQ_AUIBigB&biw=1280&bih=676#tbm=isch&q=blank+image&chips=q:blank%20image,g_1:grey&imgrc=u29WzXjIQMvUoM:");
        bannerSchoolIV = new ImageView(bannerSchoolImage);
        bannerSchoolIV.setFitHeight(20);
        bannerSchoolIV.setFitWidth(100);
        changeBannerSchoolImageButton = new Button(props.getProperty(CHANGE_BUTTON_TEXT.toString()));
        leftFooterImageLabel = new Label(props.getProperty(LEFT_FOOTER_IMAGE_TEXT.toString()));
        leftFooterImage = new Image("https://www.google.com/search?q=blank+image&espv=2&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiHh-qp1r3TAhWqzIMKHa01AnAQ_AUIBigB&biw=1280&bih=676#tbm=isch&q=blank+image&chips=q:blank%20image,g_1:grey&imgrc=u29WzXjIQMvUoM:");
        leftFooterIV = new ImageView(leftFooterImage);
        leftFooterIV.setFitHeight(20);
        leftFooterIV.setFitWidth(100);
        changeLeftFooterImageButton = new Button(props.getProperty(CHANGE_BUTTON_TEXT.toString()));
        rightFooterImageLabel = new Label(props.getProperty(RIGHT_FOOTER_IMAGE_TEXT.toString()));
        rightFooterImage = new Image("https://www.google.com/search?q=blank+image&espv=2&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiHh-qp1r3TAhWqzIMKHa01AnAQ_AUIBigB&biw=1280&bih=676#tbm=isch&q=blank+image&chips=q:blank%20image,g_1:grey&imgrc=u29WzXjIQMvUoM:");
        rightFooterIV = new ImageView(rightFooterImage);
        rightFooterIV.setFitHeight(20);
        rightFooterIV.setFitWidth(100);
        changeRightFooterImageButton = new Button(props.getProperty(CHANGE_BUTTON_TEXT.toString()));
        styleSheetLabel = new Label(props.getProperty(STYLESHEET_TEXT.toString()));
        styleSheetComboBox = new ComboBox();
        noteLabel = new Label(props.getProperty(PAGE_STYLE_NOTE_TEXT.toString()));
        pageStylePane = new GridPane();
        pageStylePane.add(pageStyleLabel, 0, 0);
        pageStylePane.add(bannerSchoolImageLabel, 0, 1);
        pageStylePane.add(bannerSchoolIV, 1, 1);
        pageStylePane.add(changeBannerSchoolImageButton, 2, 1);
        pageStylePane.add(leftFooterImageLabel, 0, 2);
        pageStylePane.add(leftFooterIV, 1, 2);
        pageStylePane.add(changeLeftFooterImageButton, 2, 2);
        pageStylePane.add(rightFooterImageLabel, 0, 3);
        pageStylePane.add(rightFooterIV, 1, 3);
        pageStylePane.add(changeRightFooterImageButton, 2, 3);
        pageStylePane.add(styleSheetLabel, 0, 4);
        pageStylePane.add(styleSheetComboBox, 1, 4);
        pageStylePane.add(noteLabel, 0, 5);
        GridPane.setMargin(bannerSchoolImageLabel, new Insets(0, 0, 10, 2));
        GridPane.setMargin(bannerSchoolIV, new Insets(0, 5, 10, 0));
        GridPane.setMargin(changeBannerSchoolImageButton, new Insets(0, 0, 10, 15));
        GridPane.setMargin(leftFooterImageLabel, new Insets(0, 0, 10, 2));
        GridPane.setMargin(leftFooterIV, new Insets(0, 5, 10, 0));
        GridPane.setMargin(changeLeftFooterImageButton, new Insets(0, 0, 10, 15));
        GridPane.setMargin(rightFooterImageLabel, new Insets(0, 0, 10, 2));
        GridPane.setMargin(rightFooterIV, new Insets(0, 5, 10, 0));
        GridPane.setMargin(changeRightFooterImageButton, new Insets(0, 0, 10, 15));
        pageStylePane.setStyle("-fx-border-style: solid inside;"+"-fx-border-width: 2;");
        coursePanel = new VBox();
        
        coursePanel.getChildren().addAll(coursePane, siteTemplateVBox, pageStylePane);
        
        coursePanel.setSpacing(5);
        coursePanel.setPadding(new Insets(5));
        coursePanel.setStyle("-fx-background-color:#f4b042;");
    }
    
    public static CourseDetailWorkspace getCourseDetailWorkspace(){
        if(singleton == null)
            singleton = new CourseDetailWorkspace();
        return singleton;
    }
    
    public VBox getCourseDetailPane(){
        return coursePanel;
    }

    public Label getCourseInfoLabel() {
        return courseInfoLabel;
    }

    public Button getChangeExportDirectoryButton() {
        return changeExportDirectoryButton;
    }

    public Label getSiteTemplateLabel() {
        return siteTemplateLabel;
    }

    public Button getSelectTemplateDirectoryButton() {
        return selectTemplateDirectoryButton;
    }

    public VBox getSiteTemplateVBox() {
        return siteTemplateVBox;
    }

    public Label getPageStyleLabel() {
        return pageStyleLabel;
    }

    public Button getChangeBannerSchoolImageButton() {
        return changeBannerSchoolImageButton;
    }

    public Button getChangeLeftFooterImageButton() {
        return changeLeftFooterImageButton;
    }

    public Button getChangeRightFooterImageButton() {
        return changeRightFooterImageButton;
    }

    public GridPane getCoursePane() {
        return coursePane;
    }

    public GridPane getPageStylePane() {
        return pageStylePane;
    }

    public ComboBox getSubjectComboBox() {
        return subjectComboBox;
    }

    public ComboBox getNumberComboBox() {
        return numberComboBox;
    }

    public ComboBox getSemesterComboBox() {
        return semesterComboBox;
    }

    public ComboBox getYearComboBox() {
        return yearComboBox;
    }

    public Label getExportDirectorySelectedLabel() {
        return exportDirectorySelectedLabel;
    }

    public Label getSelectedTemplateLabel() {
        return selectedTemplateLabel;
    }

    public Image getBannerSchoolImage() {
        return bannerSchoolImage;
    }
    
    public void setBannerSchoolImage(String path){
        bannerSchoolIV = new ImageView(new Image(path));
        bannerSchoolIV.setFitHeight(20);
        bannerSchoolIV.setFitWidth(100);
        pageStylePane.add(bannerSchoolIV, 1, 1);
    }

    public Image getLeftFooterImage() {
        return leftFooterImage;
    }
    
    public void setLeftFooterImage(String path){
        leftFooterIV = new ImageView(new Image(path));
        leftFooterIV.setFitHeight(20);
        leftFooterIV.setFitWidth(100);
        pageStylePane.add(leftFooterIV, 1, 2);
    }

    public Image getRightFooterImage() {
        return rightFooterImage;
    }
    
    public void setRightFooterImage(String path){
        rightFooterIV = new ImageView(new Image(path));
        rightFooterIV.setFitHeight(20);
        rightFooterIV.setFitWidth(100);
        pageStylePane.add(rightFooterIV, 1, 3);
    }

    public TextField getTitleTextField() {
        return titleTextField;
    }

    public TextField getInstructorNameTextField() {
        return instructorNameTextField;
    }

    public TextField getInstructorHomeTextField() {
        return instructorHomeTextField;
    }

    public ComboBox getStyleSheetComboBox() {
        return styleSheetComboBox;
    }

    public boolean isIsHomeSelected() {
        return isHomeSelected;
    }

    public void setIsHomeSelected(boolean isHomeSelected) {
        this.isHomeSelected = isHomeSelected;
    }

    public boolean isIsSyllabusSelected() {
        return isSyllabusSelected;
    }

    public void setIsSyllabusSelected(boolean isSyllabbusSelected) {
        this.isSyllabusSelected = isSyllabbusSelected;
    }

    public boolean isIsScheduleSelected() {
        return isScheduleSelected;
    }

    public void setIsScheduleSelected(boolean isScheduleSelected) {
        this.isScheduleSelected = isScheduleSelected;
    }

    public boolean isIsHWsSelected() {
        return isHWsSelected;
    }

    public void setIsHWsSelected(boolean isHWsSelected) {
        this.isHWsSelected = isHWsSelected;
    }

    public boolean isIsProjectsSelected() {
        return isProjectsSelected;
    }

    public void setIsProjectsSelected(boolean isProjectsSelected) {
        this.isProjectsSelected = isProjectsSelected;
    }
    
//    public ObservableList getData(){
//        return data;
//    }
    
    public TableView getSitePagesTable(){
        return sitePagesTable;
    }
    

    @Override
    public void resetWorkspace() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

