/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.data.Recitation;
import csg.data.ScheduleItem;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import jtps.jTPS_Transaction;
import csg.data.TAData;
import csg.data.TeachingAssistant;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author khurr
 */
public class DeleteSchedule_Transaction implements jTPS_Transaction {

    private final ScheduleItem si;
    private final TAData data;

    public DeleteSchedule_Transaction(ScheduleItem si, TAData data) {
        this.data = data;
        this.si = si; 

    }

    @Override
    public void doTransaction() {  //control Y 
        String sTitle = si.getTitle();
        data.removeScheduleItem(sTitle);
}

    @Override
    public void undoTransaction() {
        
        try {
            if(si.getType().equals("HW"))
            data.addHWScheduleItem(si.getType(), si.getDate(), si.getTitle(), si.getTopic(), si.getTime(), si.getCriteria());
            else
                data.addScheduleItem(si.getType(), si.getDate(), si.getTitle(), si.getTopic());
        } catch (ParseException ex) {
            Logger.getLogger(DeleteSchedule_Transaction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
