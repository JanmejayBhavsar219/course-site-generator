/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;


import jtps.jTPS_Transaction;
import csg.TAManagerApp;
import csg.data.TAData;
import csg.data.TeachingAssistant;


/**
 *
 * @author khurr
 */
public class UpdateTA_Transaction implements jTPS_Transaction {

    private final String oldName;
    private final String newName;
    private final String oldEmail;
    private final String newEmail;
    private final TAData taData;
    private final TeachingAssistant ta;
    private final TAManagerApp app; 
    private final TAWorkspace transWorkspace; 

    public UpdateTA_Transaction(String orgName, String name, String orgEmail, String email, TAData data, TAManagerApp taApp, TAWorkspace workspace) {
        oldName = orgName;
        newName = name;
        oldEmail = orgEmail;
        newEmail = email;
        taData = data;
        ta = data.getTA(orgName);
        app=taApp; 
        transWorkspace=workspace; 
    }

    @Override
    public void doTransaction() {  //Control Y 
        System.out.println("updateTA doTransaction ");
        taData.getTA(oldName).setName(newName);
        TAController controller = new TAController(app);
        controller.handleUpdateTaGrid(oldName, newName);
        ta.setName(newName);                        // MOVED TO TRANSACTION CASE 
        ta.setEmail(newEmail);
        transWorkspace.nameTextField.setText(newName);
        transWorkspace.emailTextField.setText(newEmail);
       // transWorkspace.taTable.refresh();

    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("updateTA undoTransaction ");
        taData.getTA(newName).setName(oldName);
        TAController controller = new TAController(app);
        controller.handleUpdateTaGrid(newName, oldName);
        ta.setName(oldName);        // MOVED TO TRANSACTION CASE 
        ta.setEmail(oldEmail);
        transWorkspace.nameTextField.setText(oldName);
        transWorkspace.emailTextField.setText(oldEmail);
        //transWorkspace.taTable.refresh();

    }

}
