package csg.workspace;

import djf.ui.AppGUI;
import djf.ui.AppMessageDialogSingleton;
import static csg.TAManagerProp.*;
import java.util.Collections;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;
import csg.TAManagerApp;
import csg.data.Recitation;
import csg.data.ScheduleItem;
import csg.data.Student;
import csg.data.TAData;
import csg.data.TeachingAssistant;
import csg.data.Team;
import static csg.style.TAStyle.CLASS_HIGHLIGHTED_GRID_CELL;
import static csg.style.TAStyle.CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN;
import static csg.style.TAStyle.CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE;
import static djf.settings.AppPropertyType.LOAD_WORK_TITLE;
import static djf.settings.AppStartupConstants.PATH_WORK;
import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file toolbar.
 *
 * @author Richard McKenna
 * @co-author Janmejay Bhavsar
 * @version 1.0
 */
public class TAController {

    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    TAManagerApp app;
    public static jTPS jTPS = new jTPS();

    /**
     * Constructor, note that the app must already be constructed.
     *
     * @param initApp
     */
    public TAController(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }

    /**
     * This helper method should be called every time an edit happens.
     */
    private void markWorkAsEdited() {
        // MARK WORK AS EDITED
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }

    /**
     * This method responds to when the user requests to add a new TA via the
     * UI. Note that it must first do some validation to make sure a unique name
     * and email address has been provided.
     */
    public void handleAddTA() {
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        EmailValidator checkEmail = new EmailValidator();

        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (name.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));
        } // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        else if (email.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));
        } // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else if (data.containsTA(name, email)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));
        } // **********Check the TA Email Address for correct format 
        else if (!checkEmail.validate(email)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_TA_EMAIL_TITLE), props.getProperty(INVALID_TA_EMAIL_MESSAGE));

        } // EVERYTHING IS FINE, ADD A NEW TA
        else {
            // ADD THE NEW TA TO THE DATA
            //data.addTA(name, email);
            jTPS_Transaction transaction1 = new AddTA_Transaction(workspace.isUndergrad(), name, email, data);
            jTPS.addTransaction(transaction1);
            //jTPS.doTransaction();
            // CLEAR THE TEXT FIELDS
            nameTextField.setText("");
            emailTextField.setText("");
            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
        }
    }

    public void handleAddRecitation() {
        RecitationWorkspace rWorkspace = RecitationWorkspace.getRecitationWorkspace();
        TextField sectionTextField = rWorkspace.sectionTextField;
        TextField instructorTextField = rWorkspace.instructorTextField;
        TextField day_timeTextField = rWorkspace.day_timeTextField;
        TextField locationTextField = rWorkspace.locationTextField;
        String section = sectionTextField.getText();
        String instructor = instructorTextField.getText();
        String day_time = day_timeTextField.getText();
        String location = locationTextField.getText();
        String TA1 = rWorkspace.supervisingTAComboBox1.getSelectionModel().getSelectedItem().toString();
        String TA2 = rWorkspace.supervisingTAComboBox2.getSelectionModel().getSelectedItem().toString();
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData) app.getDataComponent();
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if (section.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Recitation Section"), props.getProperty("Recitation section cannot be blank"));
        } else if (instructor.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Recitation Instructor"), props.getProperty("Recitation instructor cannot be blank"));
        } else if (day_time.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Recitation Day/Time"), props.getProperty("Recitation day/time cannot be blank"));
        } else if (location.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Recitation Location"), props.getProperty("Recitation location cannot be blank"));
        } else if (data.containsRecitation(section)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Recitation Section not unique"), props.getProperty("The recitation sections must be unique"));
        } else {
            jTPS_Transaction transaction1 = new AddRecitation_Transaction(section, instructor, day_time, location, TA1, TA2, data);
            jTPS.addTransaction(transaction1);
            sectionTextField.setText("");
            instructorTextField.setText("");
            day_timeTextField.setText("");
            locationTextField.setText("");
            rWorkspace.getSupervisingTAComboBox1().getSelectionModel().clearSelection();
            rWorkspace.getSupervisingTAComboBox2().getSelectionModel().clearSelection();
        }
    }

    public void handleAddSchedule() {
        ScheduleWorkspace sWorkspace = ScheduleWorkspace.getScheduleWorkspace();
        TextField timeTextField = sWorkspace.timeTextField;
        TextField titleTextField = sWorkspace.titleTextField;
        TextField topicTextField = sWorkspace.topicTextField;
        TextField linkTextField = sWorkspace.linkScheduleTextField;
        TextField criteriaTextField = sWorkspace.criteriaTextField;
        String type = sWorkspace.typeComboBox.getSelectionModel().getSelectedItem().toString();
        String date = sWorkspace.datePicker.getValue().format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));

        String time = timeTextField.getText();
        String title = titleTextField.getText();
        String topic = topicTextField.getText();
        String link = linkTextField.getText();
        String criteria = criteriaTextField.getText();
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData) app.getDataComponent();
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if (type.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Schedule Type"), props.getProperty("Schedule type cannot be blank"));
        } else if (date.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Schedule Date"), props.getProperty("Schedule date cannot be blank"));
        } else if (title.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Schedule Title"), props.getProperty("Schedule title cannot be blank"));
        } else if (topic.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Schedule Topic"), props.getProperty("Schedule topic cannot be blank"));
        } else if (data.containsScheduleItem(title)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Schedule Title not unique"), props.getProperty("The schedule titles must be unique"));
        } else {
            jTPS_Transaction transaction1 = new AddSchedule_Transaction(type, date, time, title, topic, link, criteria, data);
            jTPS.addTransaction(transaction1);
            sWorkspace.typeComboBox.getSelectionModel().clearSelection();
            sWorkspace.datePicker.setValue(LocalDate.now());
            timeTextField.setText("");
            titleTextField.setText("");
            topicTextField.setText("");
            linkTextField.setText("");
            criteriaTextField.setText("");
        }
    }

    public void handleAddTeam() {
        ProjectWorkspace pWorkspace = ProjectWorkspace.getProjectWorkspace();
        TextField nameTextField = pWorkspace.nameTextField;
        String name = nameTextField.getText();
        String color = pWorkspace.colorPicker.getValue().toString();
        String textColor = pWorkspace.textColorPicker.getValue().toString();
        TextField linkTextField = pWorkspace.linkTextField;
        String link = linkTextField.getText();
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData) app.getDataComponent();
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if (name.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Team Name"), props.getProperty("Team name cannot be blank"));
        } else if (color.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Team Color"), props.getProperty("Team color cannot be blank"));
        } else if (textColor.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Team Text Color"), props.getProperty("Team text color cannot be blank"));
        } else if (link.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Team Link"), props.getProperty("Team link cannot be blank"));
        } else if (data.containsTeam(name)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Team name not unique"), props.getProperty("The team names must be unique"));
        } else {
            jTPS_Transaction transaction1 = new AddTeam_Transaction(name, color, textColor, link, data);
            jTPS.addTransaction(transaction1);
            nameTextField.setText("");
            pWorkspace.colorPicker.setValue(null);
            pWorkspace.textColorPicker.setValue(null);
            linkTextField.setText("");
        }
    }
    
    public void handleAddStudent(){
        ProjectWorkspace pWorkspace = ProjectWorkspace.getProjectWorkspace();
        TextField firstNameTextField = pWorkspace.firstNameTextField;
        String firstName = firstNameTextField.getText();
        TextField lastNameTextField = pWorkspace.lastNameTextField;
        String lastName = lastNameTextField.getText();
        String teamName = pWorkspace.teamComboBox.getSelectionModel().getSelectedItem().toString();
        TextField roleTextField = pWorkspace.roleTextField;
        String role = roleTextField.getText();
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData) app.getDataComponent();
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if (firstName.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Student First Name"), props.getProperty("Student first name cannot be blank"));
        } else if (lastName.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Student Last Name"), props.getProperty("Student last name cannot be blank"));
        } else if (teamName.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Student Team"), props.getProperty("Student team cannot be blank"));
        } else if (role.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Student Role"), props.getProperty("Student role cannot be blank"));
        } else if (data.containsStudent(firstName)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Student first name not unique"), props.getProperty("The student first names must be unique"));
        } else {
            jTPS_Transaction transaction1 = new AddStudent_Transaction(firstName, lastName, teamName, role, data);
            jTPS.addTransaction(transaction1);
            firstNameTextField.setText("");
            lastNameTextField.setText("");
            pWorkspace.teamComboBox.getSelectionModel().clearSelection();
            roleTextField.setText("");
        }
    }

    /**
     * This function provides a response for when the user presses a keyboard
     * key. Note that we're only responding to Delete, to remove a TA.
     *
     * @param code The keyboard code pressed.
     */
    public void handleKeyPress(KeyCode code) {
        // DID THE USER PRESS THE DELETE KEY?

        if (code == KeyCode.BACK_SPACE) {
            // GET THE TABLE
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            TableView taTable = workspace.getTATable();

            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = taTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                TeachingAssistant ta = (TeachingAssistant) selectedItem;
                String taName = ta.getName();
                TAData data = (TAData) app.getDataComponent();
                HashMap<String, StringProperty> officeHours = data.getOfficeHours();
                jTPS_Transaction transaction1 = new DeleteTA_Transaction(ta, data, officeHours);
                jTPS.addTransaction(transaction1);

                /*data.removeTA(taName);

                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
               
                for (Label label : labels.values()) {
                    if (label.getText().equals(taName)
                            || (label.getText().contains(taName + "\n"))
                            || (label.getText().contains("\n" + taName))) {
                        data.removeTAFromCell(label.textProperty(), taName);
                    }
                } */
                // WE'VE CHANGED STUFF
                markWorkAsEdited();
            }
            TableView rTable = workspace.getRecitationTable();
            Object selectedRItem = rTable.getSelectionModel().getSelectedItem();
            if (selectedRItem != null) {
                Recitation rec = (Recitation) selectedRItem;
                String section = rec.getSection();
                TAData data = (TAData) app.getDataComponent();
                jTPS_Transaction transaction1 = new DeleteRecitation_Transaction(rec, data);
                jTPS.addTransaction(transaction1);
            }
            TableView sTable = workspace.getScheduleTable();
            Object selectedSItem = sTable.getSelectionModel().getSelectedItem();
            if (selectedSItem != null) {
                ScheduleItem si = (ScheduleItem) selectedSItem;
                String title = si.getTitle();
                TAData data = (TAData) app.getDataComponent();
                jTPS_Transaction transaction1 = new DeleteSchedule_Transaction(si, data);
                jTPS.addTransaction(transaction1);
            }
            TableView tTable = workspace.getTeamTable();
            Object selectedTItem = tTable.getSelectionModel().getSelectedItem();
            if (selectedTItem != null) {
                Team t = (Team) selectedTItem;
                String name = t.getName();
                TAData data = (TAData) app.getDataComponent();
                jTPS_Transaction transaction1 = new DeleteTeam_Transaction(t, data);
                jTPS.addTransaction(transaction1);
            }
            TableView studTable = workspace.getStudentTable();
            Object selectedStudItem = studTable.getSelectionModel().getSelectedItem();
            if (selectedStudItem != null) {
                Student s = (Student) selectedStudItem;
                String firstName = s.getFirstName();
                TAData data = (TAData) app.getDataComponent();
                jTPS_Transaction transaction1 = new DeleteStudent_Transaction(s, data);
                jTPS.addTransaction(transaction1);
        }
    }
 }

    public void handleUndoTransaction() {
        System.out.println("Transaction Control Z");
        jTPS.undoTransaction();
        markWorkAsEdited();
    }

    public void handleReDoTransaction() {
        System.out.println("Transaction crlt y");
        jTPS.doTransaction();
        markWorkAsEdited();
    }

    /**
     * This function provides a response for when the user clicks on the office
     * hours grid to add or remove a TA to a time slot.
     *
     * @param pane The pane that was toggled.
     */
    public void handleCellToggle(Pane pane) {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();

        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            // GET THE TA
            jTPS_Transaction transaction = new ToggleTa_Transaction(selectedItem, app, pane);
            jTPS.addTransaction(transaction);

            /*TeachingAssistant ta = (TeachingAssistant) selectedItem;
            String taName = ta.getName();
            TAData data = (TAData) app.getDataComponent();
            String cellKey = pane.getId();

            // AND TOGGLE THE OFFICE HOURS IN THE CLICKED CELL
            data.toggleTAOfficeHours(cellKey, taName);*/
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
        }
    }

    public void handleTaClicked(Pane pane, Pane addBox) {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();

        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            //selectedItem = taTable.getSelectionModel().getSelectedItem();
            TeachingAssistant ta = (TeachingAssistant) selectedItem;
            // workspace.nameTextField.clear();
            //workspace.emailTextField.clear(); 
            System.out.println("TA CLICKED");
            System.out.println(ta.getName());
            // addBox.getChildren().remove(workspace.addButton); 
            // addBox.getChildren().remove(workspace.clearButton1); 
            addBox.getChildren().add(workspace.nameTextField);
            addBox.getChildren().add(workspace.emailTextField);
            addBox.getChildren().add(workspace.updateTaButton);
            addBox.getChildren().add(workspace.clearButton);
            // GET THE TA
            String taName = ta.getName();
            String taEmail = ta.getEmail();
            TAData data = (TAData) app.getDataComponent();

            // SET TextField To TA NAME 
            workspace.nameTextField.setText(taName);
            workspace.emailTextField.setText(taEmail);
            // workspace.updateTaButton.setOnAction(e -> {
            //handleUpdateTA(taName,taEmail,ta);

            // });
            //markWorkAsEdited();   
        }
    }

    public void handleRecitationClicked() {
        RecitationWorkspace rw = RecitationWorkspace.getRecitationWorkspace();
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView rTable = workspace.getRecitationTable();
        Object selectedItem = rTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            Recitation rec = (Recitation) selectedItem;
            String section = rec.getSection();
            String instructor = rec.getInstructor();
            String day_time = rec.getDay_Time();
            String location = rec.getLocation();
            String TA1 = rec.getTA1();
            String TA2 = rec.getTA2();
            rw.sectionTextField.setText(section);
            rw.instructorTextField.setText(instructor);
            rw.day_timeTextField.setText(day_time);
            rw.locationTextField.setText(location);
            rw.getSupervisingTAComboBox1().getSelectionModel().select(TA1);
            rw.getSupervisingTAComboBox2().getSelectionModel().select(TA2);
        }
    }

    public void handleScheduleClicked() {
        ScheduleWorkspace sw = ScheduleWorkspace.getScheduleWorkspace();
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView sTable = workspace.getScheduleTable();
        Object selectedItem = sTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            ScheduleItem si = (ScheduleItem) selectedItem;
            String type = si.getType();
            String date = si.getDate();
            String time = si.getTime();
            String title = si.getTitle();
            String topic = si.getTopic();
            String link = si.getLink();
            String criteria = si.getCriteria();
            sw.typeComboBox.getSelectionModel().select(type);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            LocalDate dateValue = LocalDate.parse(date, formatter);
            sw.datePicker.setValue(dateValue);
            sw.timeTextField.setText(time);
            sw.titleTextField.setText(title);
            sw.topicTextField.setText(topic);
            sw.linkScheduleTextField.setText(link);
            sw.criteriaTextField.setText(criteria);
        }
    }
    
    public void handleTeamClicked(){
        ProjectWorkspace pw = ProjectWorkspace.getProjectWorkspace();
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView tTable = workspace.getTeamTable();
        Object selectedItem = tTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            Team team = (Team) selectedItem;
            String name = team.getName();
            String color = team.getColor();
            String textColor = team.getTextColor();
            String link = team.getLink();
            pw.nameTextField.setText(name);
            pw.colorPicker.setValue(Color.web(color));
            pw.textColorPicker.setValue(Color.web(textColor));
            pw.linkTextField.setText(link);
        }
    }
    
    public void handleStudentClicked(){
        ProjectWorkspace pw = ProjectWorkspace.getProjectWorkspace();
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView sTable = workspace.getStudentTable();
        Object selectedItem = sTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            Student student = (Student) selectedItem;
            String firstName = student.getFirstName();
            String lastName = student.getLastName();
            String team = student.getTeam();
            String role = student.getRole();
            pw.firstNameTextField.setText(firstName);
            pw.lastNameTextField.setText(lastName);
            pw.teamComboBox.getSelectionModel().select(team);
            pw.roleTextField.setText(role);
        }
    }

    public void handleUpdateTA() {
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        TeachingAssistant ta = (TeachingAssistant) selectedItem;
        String orgName = ta.getName();
        String orgEmail = ta.getEmail();

        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();

        String name = nameTextField.getText();
        String email = emailTextField.getText();
        EmailValidator checkEmail = new EmailValidator();

        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if (name.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));
        } // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        else if (email.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));
        } // **********Check the TA Email Address for correct format 
        else if (!checkEmail.validate(email)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_TA_EMAIL_TITLE), props.getProperty(INVALID_TA_EMAIL_MESSAGE));

        } // EVERYTHING IS FINE, ADD A NEW TA
        else {
            // ADD THE NEW TA TO THE DATA
            if (!orgName.equalsIgnoreCase(name)) { //case if only name is changed
                jTPS_Transaction transaction2 = new UpdateTA_Transaction(orgName, name, orgEmail, email, data, app, workspace);
                nameTextField.setText(name);
                emailTextField.setText(email);
                jTPS.addTransaction(transaction2);

                //jTPS.doTransaction();
                /*data.getTA(orgName).setName(name);
                handleUpdateTaGrid(orgName, name);
                ta.setName(name);                        // MOVED TO TRANSACTION CASE 
                taTable.refresh();
                 */
                markWorkAsEdited();
            }
            if (!orgEmail.equalsIgnoreCase(email)) {   //case if only email is changed 
                jTPS_Transaction transaction3 = new UpdateTA_EmailOnly_Transaction(orgName, orgEmail, email, data, workspace);
                jTPS.addTransaction(transaction3);

                // data.getTA(orgName).setEmail(email);     //moved to transaction class 
                //ta.setEmail(email);
                markWorkAsEdited();

            }
            if (orgEmail.equalsIgnoreCase(email) && orgName.equalsIgnoreCase(name)) {                //case if nothing is chagned 
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(NO_UPDATE_TITLE), props.getProperty(NO_UPDATE_MESSAGE));

            }
            taTable.refresh();
            nameTextField.setText(name);
            emailTextField.setText(email);

            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();
            // WE'VE CHANGED STUFF

        }
        // workspace.reloadOfficeHoursGrid(data);
    }

    public void handleUpdateRecitation() {
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        RecitationWorkspace rw = RecitationWorkspace.getRecitationWorkspace();
        TableView rTable = workspace.getRecitationTable();
        Object selectedItem = rTable.getSelectionModel().getSelectedItem();
        Recitation rec = (Recitation) selectedItem;
        String orgSection = rec.getSection();
        String orgInstructor = rec.getInstructor();
        String orgDay_Time = rec.getDay_Time();
        String orgLocation = rec.getLocation();
        String orgTA1 = rec.getTA1();
        String orgTA2 = rec.getTA2();
        TextField sectionTextField = rw.sectionTextField;
        TextField instructorTextField = rw.instructorTextField;
        TextField day_timeTextField = rw.day_timeTextField;
        TextField locationTextField = rw.locationTextField;
        String section = sectionTextField.getText();
        String instructor = instructorTextField.getText();
        String day_time = day_timeTextField.getText();
        String location = locationTextField.getText();
        String TA1 = rw.getSupervisingTAComboBox1().getSelectionModel().getSelectedItem().toString();
        String TA2 = rw.getSupervisingTAComboBox2().getSelectionModel().getSelectedItem().toString();
        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if (section.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Recitation Section"), props.getProperty("Recitation section cannot be blank"));
        } else if (instructor.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Recitation Instructor"), props.getProperty("Recitation instructor cannot be blank"));
        } else if (day_time.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Recitation Day/Time"), props.getProperty("Recitation day/time cannot be blank"));
        } else if (location.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Recitation Location"), props.getProperty("Recitation location cannot be blank"));
        } else if (data.containsRecitation(section)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Recitation Section not unique"), props.getProperty("The recitation sections must be unique"));
        } else {
            // ADD THE NEW TA TO THE DATA
            if (!orgSection.equalsIgnoreCase(section)) { //case if only name is changed
                jTPS_Transaction transaction2 = new UpdateRecitation_Transaction(orgSection, section, orgInstructor, instructor, orgDay_Time, day_time, orgLocation, location, orgTA1, TA1, orgTA2, TA2, data, app, workspace);
                sectionTextField.setText(section);
                instructorTextField.setText(instructor);
                day_timeTextField.setText(day_time);
                locationTextField.setText(location);
                rw.getSupervisingTAComboBox1().getSelectionModel().select(TA1);
                rw.getSupervisingTAComboBox2().getSelectionModel().select(TA2);
                jTPS.addTransaction(transaction2);
                markWorkAsEdited();
            }
            if (orgSection.equalsIgnoreCase(section) && orgInstructor.equalsIgnoreCase(instructor)) {                //case if nothing is chagned 
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(NO_UPDATE_TITLE), props.getProperty(NO_UPDATE_MESSAGE));
            }
            rTable.refresh();
            sectionTextField.setText(section);
            instructorTextField.setText(instructor);
            day_timeTextField.setText(day_time);
            locationTextField.setText(location);
            rw.getSupervisingTAComboBox1().getSelectionModel().select(TA1);
            rw.getSupervisingTAComboBox2().getSelectionModel().select(TA2);
            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            sectionTextField.requestFocus();
            // WE'VE CHANGED STUFF

        }
    }

    public void handleUpdateScheduleItem() {
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        ScheduleWorkspace sw = ScheduleWorkspace.getScheduleWorkspace();
        TableView sTable = workspace.getScheduleTable();
        Object selectedItem = sTable.getSelectionModel().getSelectedItem();
        ScheduleItem si = (ScheduleItem) selectedItem;
        String orgType = si.getType();
        String orgDate = si.getDate();
        String orgTime = si.getTime();
        String orgTitle = si.getTitle();
        String orgTopic = si.getTopic();
        String orgLink = si.getLink();
        String orgCriteria = si.getCriteria();
        TextField titleTextField = sw.titleTextField;
        TextField timeTextField = sw.timeTextField;
        TextField topicTextField = sw.topicTextField;
        TextField linkTextField = sw.linkScheduleTextField;
        TextField criteriaTextField = sw.criteriaTextField;
        String type = sw.typeComboBox.getSelectionModel().getSelectedItem().toString();
        String date = sw.datePicker.getValue().format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
        String time = timeTextField.getText();
        String title = titleTextField.getText();
        String topic = topicTextField.getText();
        String link = linkTextField.getText();
        String criteria = criteriaTextField.getText();
        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if (type.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Schedule Type"), props.getProperty("Schedule type cannot be blank"));
        } else if (date.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Schedule Date"), props.getProperty("Schedule date cannot be blank"));
        } else if (title.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Schedule Title"), props.getProperty("Schedule title cannot be blank"));
        } else if (topic.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Schedule Topic"), props.getProperty("Schedule topic cannot be blank"));
        } else if (data.containsScheduleItem(title)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Schedule title not unique"), props.getProperty("The schedule titles must be unique"));
        } else {
            // ADD THE NEW TA TO THE DATA
            if (!orgTitle.equalsIgnoreCase(title)) { //case if only name is changed
                jTPS_Transaction transaction2 = new UpdateSchedule_Transaction(orgType, type, orgDate, date, orgTime, time, orgTitle, title, orgTopic, topic, orgLink, link, orgCriteria, criteria, data, app, workspace);
                sw.typeComboBox.getSelectionModel().select(type);
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
                LocalDate dateValue = LocalDate.parse(date, formatter);
                sw.datePicker.setValue(dateValue);
                timeTextField.setText(time);
                titleTextField.setText(title);
                topicTextField.setText(topic);
                linkTextField.setText(link);
                criteriaTextField.setText(criteria);
                jTPS.addTransaction(transaction2);
                markWorkAsEdited();
            }
            if (orgTitle.equalsIgnoreCase(title) && orgDate.equalsIgnoreCase(date)) {                //case if nothing is chagned 
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(NO_UPDATE_TITLE), props.getProperty(NO_UPDATE_MESSAGE));
            }
            sTable.refresh();
            sw.typeComboBox.getSelectionModel().select(type);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            LocalDate dateValue = LocalDate.parse(date, formatter);
            sw.datePicker.setValue(dateValue);
            timeTextField.setText(time);
            titleTextField.setText(title);
            topicTextField.setText(topic);
            linkTextField.setText(link);
            criteriaTextField.setText(criteria);
            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            sw.typeComboBox.requestFocus();
            // WE'VE CHANGED STUFF

        }
    }
    
    public void handleUpdateTeam(){
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        ProjectWorkspace pw = ProjectWorkspace.getProjectWorkspace();
        TableView tTable = workspace.getTeamTable();
        Object selectedItem = tTable.getSelectionModel().getSelectedItem();
        Team team = (Team) selectedItem;
        String orgName = team.getName();
        String orgColor = team.getColor();
        String orgTextColor = team.getTextColor();
        String orgLink = team.getLink();
        TextField nameTextField = pw.nameTextField;
        TextField linkTextField = pw.linkTextField;
        String name = nameTextField.getText();
        String color = pw.colorPicker.getValue().toString();
        String textColor = pw.textColorPicker.getValue().toString();
        String link = linkTextField.getText();
        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if (name.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Recitation Section"), props.getProperty("Recitation section cannot be blank"));
        } else if (color.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Recitation Instructor"), props.getProperty("Recitation instructor cannot be blank"));
        } else if (textColor.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Recitation Day/Time"), props.getProperty("Recitation day/time cannot be blank"));
        } else if (link.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Recitation Location"), props.getProperty("Recitation location cannot be blank"));
        } else if (data.containsTeam(name)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Recitation Section not unique"), props.getProperty("The recitation sections must be unique"));
        } else {
            // ADD THE NEW TA TO THE DATA
            if (!orgName.equalsIgnoreCase(name)) { //case if only name is changed
                jTPS_Transaction transaction2 = new UpdateTeam_Transaction(orgName, name, orgColor, color, orgTextColor, textColor, orgLink, link, data, app, workspace);
                nameTextField.setText(name);
                pw.colorPicker.setValue(Color.web(color.substring(2, 8)));
                pw.textColorPicker.setValue(Color.web(textColor.substring(2, 8)));
                linkTextField.setText(link);
                jTPS.addTransaction(transaction2);
                markWorkAsEdited();
            }
            if (orgName.equalsIgnoreCase(name) && orgLink.equalsIgnoreCase(link)) {                //case if nothing is chagned 
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(NO_UPDATE_TITLE), props.getProperty(NO_UPDATE_MESSAGE));
            }
            tTable.refresh();
            nameTextField.setText(name);
            pw.colorPicker.setValue(Color.web(color));
            pw.textColorPicker.setValue(Color.web(textColor));
            linkTextField.setText(link);
            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();
            // WE'VE CHANGED STUFF

        }
    }
    
    public void handleUpdateStudent(){
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        ProjectWorkspace pw = ProjectWorkspace.getProjectWorkspace();
        TableView sTable = workspace.getStudentTable();
        Object selectedItem = sTable.getSelectionModel().getSelectedItem();
        Student student = (Student) selectedItem;
        String orgFirstName = student.getFirstName();
        String orgLastName = student.getLastName();
        String orgTeam = student.getTeam();
        String orgRole = student.getRole();
        TextField firstNameTextField = pw.firstNameTextField;
        TextField lastNameTextField = pw.lastNameTextField;
        TextField roleTextField = pw.roleTextField;
        String firstName = firstNameTextField.getText();
        String lastName = lastNameTextField.getText();
        String team = pw.teamComboBox.getSelectionModel().getSelectedItem().toString();
        String role = roleTextField.getText();
        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if (firstName.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Recitation Section"), props.getProperty("Recitation section cannot be blank"));
        } else if (lastName.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Recitation Instructor"), props.getProperty("Recitation instructor cannot be blank"));
        } else if (team.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Recitation Day/Time"), props.getProperty("Recitation day/time cannot be blank"));
        } else if (role.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Missing Recitation Location"), props.getProperty("Recitation location cannot be blank"));
        } else if (data.containsTeam(firstName)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty("Recitation Section not unique"), props.getProperty("The recitation sections must be unique"));
        } else {
            // ADD THE NEW TA TO THE DATA
            if (!orgFirstName.equalsIgnoreCase(firstName)) { //case if only name is changed
                jTPS_Transaction transaction2 = new UpdateStudent_Transaction(orgFirstName, firstName, orgLastName, lastName, orgTeam, team, orgRole, role, data, app, workspace);
                firstNameTextField.setText(firstName);
                lastNameTextField.setText(lastName);
                pw.teamComboBox.getSelectionModel().select(team);
                roleTextField.setText(role);
                jTPS.addTransaction(transaction2);
                markWorkAsEdited();
            }
            if (orgFirstName.equalsIgnoreCase(firstName) && orgLastName.equalsIgnoreCase(lastName)) {                //case if nothing is chagned 
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(NO_UPDATE_TITLE), props.getProperty(NO_UPDATE_MESSAGE));
            }
            sTable.refresh();
            firstNameTextField.setText(firstName);
            lastNameTextField.setText(lastName);
            pw.teamComboBox.getSelectionModel().select(team);
            roleTextField.setText(role);
            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            firstNameTextField.requestFocus();
            // WE'VE CHANGED STUFF

        }
    }

    public void handleTaTableRefresh() {
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        taTable.refresh();
    }

    public void handleUpdateTaGrid(String taName, String newName) {

        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TAData data = (TAData) app.getDataComponent();
        //data.removeTA(taName);

        // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
        HashMap<String, Label> labels = workspace.getOfficeHoursGridTACellLabels();

        labels.values().stream().filter((label) -> (label.getText().equals(taName)
                || (label.getText().contains(taName + "\n"))
                || (label.getText().contains("\n" + taName)))).forEachOrdered((label) -> {
            data.renameTaCell(label.textProperty(), taName, newName);
        }); //iterates thourhg the hashmap to find all occurences of orgTA in the office hour grid
        TableView taTable = workspace.getTATable();
        Collections.sort(data.getTeachingAssistants());  //sorts the teachingAssistants List 
        taTable.refresh();

        markWorkAsEdited();

    }

    void handleGridCellMouseExited(Pane pane) {
        String cellKey = pane.getId();
        TAData data = (TAData) app.getDataComponent();
        int column = Integer.parseInt(cellKey.substring(0, cellKey.indexOf("_")));
        int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_") + 1));
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();

        Pane mousedOverPane = workspace.getTACellPane(data.getCellKey(column, row));
        mousedOverPane.getStyleClass().clear();
        mousedOverPane.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);

        // THE MOUSED OVER COLUMN HEADER
        Pane headerPane = workspace.getOfficeHoursGridDayHeaderPanes().get(data.getCellKey(column, 0));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // THE MOUSED OVER ROW HEADERS
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(0, row));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(1, row));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // AND NOW UPDATE ALL THE CELLS IN THE SAME ROW TO THE LEFT
        for (int i = 2; i < column; i++) {
            cellKey = data.getCellKey(i, row);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
            cell.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }

        // AND THE CELLS IN THE SAME COLUMN ABOVE
        for (int i = 1; i < row; i++) {
            cellKey = data.getCellKey(column, i);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
            cell.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }
    }

    void handleGridCellMouseEntered(Pane pane) {
        String cellKey = pane.getId();
        TAData data = (TAData) app.getDataComponent();
        int column = Integer.parseInt(cellKey.substring(0, cellKey.indexOf("_")));
        int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_") + 1));
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();

        // THE MOUSED OVER PANE
        Pane mousedOverPane = workspace.getTACellPane(data.getCellKey(column, row));
        mousedOverPane.getStyleClass().clear();
        mousedOverPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_CELL);

        // THE MOUSED OVER COLUMN HEADER
        Pane headerPane = workspace.getOfficeHoursGridDayHeaderPanes().get(data.getCellKey(column, 0));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // THE MOUSED OVER ROW HEADERS
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(0, row));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(1, row));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // AND NOW UPDATE ALL THE CELLS IN THE SAME ROW TO THE LEFT
        for (int i = 2; i < column; i++) {
            cellKey = data.getCellKey(i, row);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        }

        // AND THE CELLS IN THE SAME COLUMN ABOVE
        for (int i = 1; i < row; i++) {
            cellKey = data.getCellKey(column, i);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        }
    }

    void handleChangeTime(String startTime, String endTime) {
        //TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Alert confirmationDialog = new Alert(AlertType.CONFIRMATION);
        confirmationDialog.setTitle(props.getProperty(UPDATE_TIME_TITLE));
        confirmationDialog.setHeaderText("");
        confirmationDialog.setContentText(props.getProperty(UPDATE_TIME_MESSAGE));
        ButtonType buttonTypeOne = new ButtonType("Yes");
        ButtonType buttonTypeTwo = new ButtonType("No");
        confirmationDialog.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo);
        Optional<ButtonType> result = confirmationDialog.showAndWait();

//        AppYesNoDialogSingleton yesNoDialog = AppYesNoDialogSingleton.getSingleton();
//        yesNoDialog.show(props.getProperty(UPDATE_TIME_TITLE), props.getProperty(UPDATE_TIME_MESSAGE));
        // AND NOW GET THE USER'S SELECTION
//        String selection = yesNoDialog.getSelection();
        if (result.get() == buttonTypeOne) {

            int start = convertToMilitaryTime(startTime);
            int end = convertToMilitaryTime(endTime);
            System.out.println(start);

            //TAWorkspace workspace = (TAWorkspace)app.getDataComponent();
            if (start == end || start == -1 || end == -1) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(INVALID_TIME_INPUT_TITLE), props.getProperty(INVALID_TIME_INPUT_MESSAGE));       //REMEMBER TO CHANGE TO PROPER ERROR MESSAGE                              

            } else if (start > end) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(INVALID_TIME_INPUT_TITLE), props.getProperty(INVALID_TIME_INPUT_MESSAGE));       //REMEMBER TO CHANGE TO PROPER ERROR MESSAGE                              

            } else if (end < start) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(INVALID_TIME_INPUT_TITLE), props.getProperty(INVALID_TIME_INPUT_MESSAGE));       //REMEMBER TO CHANGE TO PROPER ERROR MESSAGE                              

            } else {    //At this point the time varialbes are good to go. 
                TAData data = (TAData) app.getDataComponent();

                jTPS_Transaction transaction = new updateTime_Transaction(start, end, data);
                jTPS.addTransaction(transaction);

                //workspace.resetWorkspace(); 
                //workspace.reloadWorkspace(oldData);
                markWorkAsEdited();
                //workspace.reloadOfficeHoursGrid(data);
            }
        }

    }

    public int convertToMilitaryTime(String time) {
        int milTime = 0;
        if (time == null) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_TA_EMAIL_TITLE), props.getProperty(INVALID_TA_EMAIL_MESSAGE));       //REMEMBER TO CHANGE TO PROPER ERROR MESSAGE                              
        } else if (time.equalsIgnoreCase("12:00pm")) {
            milTime = 12;
        } else {
            int index = time.indexOf(":");
            String subStringTime = time.substring(0, index);
            milTime = Integer.parseInt(subStringTime);
            if (time.contains("p")) {
                milTime += 12;
            }
        }
        return milTime;
    }

    public void handleChangeExportDirectory() {
        DirectoryChooser dc = new DirectoryChooser();
        File destination = new File(PATH_WORK);
        dc.setInitialDirectory(destination);
        dc.setTitle("Export Directory");
        destination = dc.showDialog(app.getGUI().getWindow());
        CourseDetailWorkspace cdw = CourseDetailWorkspace.getCourseDetailWorkspace();
        cdw.exportDirectorySelectedLabel.setText(destination.getPath());
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsExported(gui);
        markWorkAsEdited();
    }

    public void handleSelectTemplateDirectory() {
        TAData data = (TAData) app.getDataComponent();

        DirectoryChooser dc = new DirectoryChooser();
        File destination = new File(PATH_WORK);
        dc.setInitialDirectory(destination);
        dc.setTitle("Select Template Directory");
        destination = dc.showDialog(app.getGUI().getWindow());
        CourseDetailWorkspace cdw = CourseDetailWorkspace.getCourseDetailWorkspace();
        cdw.selectedTemplateLabel.setText(destination.getPath());
        File folder = new File(destination.getPath());
        File[] listOfFiles = folder.listFiles();
        data.getSitePages().clear();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                if (listOfFiles[i].getName().equals("index.html")) {
                    data.addSitePage(false, "Home", listOfFiles[i].getName(), "HomeBuilder.js");
                }
                if (listOfFiles[i].getName().equals("syllabus.html")) {
                    data.addSitePage(false, "Syllabus", listOfFiles[i].getName(), "SyllabusBuilder.js");
                }
                if (listOfFiles[i].getName().equals("schedule.html")) {
                    data.addSitePage(false, "Schedule", listOfFiles[i].getName(), "ScheduleBuilder.js");
                }
                if (listOfFiles[i].getName().equals("hws.html")) {
                    data.addSitePage(false, "HWs", listOfFiles[i].getName(), "HWsBuilder.js");
                }
                if (listOfFiles[i].getName().equals("projects.html")) {
                    data.addSitePage(false, "Project", listOfFiles[i].getName(), "ProjectsBuilder.js");
                }
                System.out.println(listOfFiles[i].getName());
            }
        }
        File cssFolder = new File(destination.getPath() + "/css");
        System.out.println(destination.getPath());
        File[] cssListOfFiles = cssFolder.listFiles();
         ObservableList cssFiles = FXCollections.observableArrayList();
//            data.getSitePages().clear();
        for (int i = 0; i < cssListOfFiles.length; i++) {
            if (cssListOfFiles[i].isFile()) {
                cssFiles.add(cssListOfFiles[i].getName());
                cdw.styleSheetComboBox.setItems(cssFiles);
                System.out.println(cssListOfFiles[i].getName());
            }
        }
        markWorkAsEdited();
    }

    public void handleChangeBannerSchoolImage() {
        TAData data = (TAData) app.getDataComponent();
        CourseDetailWorkspace cdw = CourseDetailWorkspace.getCourseDetailWorkspace();
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(PATH_WORK));
        fc.setTitle("Load Banner School Image");
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());
        cdw.bannerSchoolIV.setImage(new Image(selectedFile.toURI().toString()));
        data.setBannerSchoolImage(selectedFile.toURI().toString());
        markWorkAsEdited();
    }

    public void handleChangeLeftFooterImage() {
        TAData data = (TAData) app.getDataComponent();
        CourseDetailWorkspace cdw = CourseDetailWorkspace.getCourseDetailWorkspace();
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(PATH_WORK));
        fc.setTitle("Load Left Footer Image");
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());
        cdw.leftFooterIV.setImage(new Image(selectedFile.toURI().toString()));
        data.setLeftFooterImage(selectedFile.toURI().toString());
        markWorkAsEdited();
    }

    public void handleChangeRightFooterImage() {
        TAData data = (TAData) app.getDataComponent();
        CourseDetailWorkspace cdw = CourseDetailWorkspace.getCourseDetailWorkspace();
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(PATH_WORK));
        fc.setTitle("Load Left Footer Image");
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());
        cdw.rightFooterIV.setImage(new Image(selectedFile.toURI().toString()));
        data.setRightFooterImage(selectedFile.toURI().toString());
        markWorkAsEdited();
    }
}
