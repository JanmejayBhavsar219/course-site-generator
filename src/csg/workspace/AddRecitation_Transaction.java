/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.data.Recitation;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import csg.data.TAData;
import csg.data.TeachingAssistant;

/**
 *
 * @author khurr
 */
public class AddRecitation_Transaction implements jTPS_Transaction {

    private final String recSection;
    private final String recInstructor;
    private final String recDay_Time;
    private final String recLocation;
    private final String recTA1;
    private final String recTA2;
    private final TAData data;

    public AddRecitation_Transaction(String section, String instructor, String day_time, String location, String TA1, String TA2, TAData taData) {
        recSection = section;
        recInstructor = instructor;
        recDay_Time = day_time;
        recLocation = location;
        recTA1 = TA1;
        recTA2 = TA2;        
        data = taData;
    }

    @Override
    public void doTransaction() {  //Control Y 
        data.addRecitation(recSection, recInstructor, recDay_Time, recLocation, recTA1, recTA2);
        //Collections.sort(data.getTeachingAssistants());
        System.out.println("doTransaction");
    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("undo Transaction");
        ObservableList<Recitation> recList = data.getRecitations();
        for (Recitation rec : recList) {
            if (recSection.equals(rec.getSection())) {
                recList.remove(rec);
                return;
            }

        }
        // data.removeTA(taName);

    }

}
