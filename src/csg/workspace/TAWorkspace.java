package csg.workspace;

import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import csg.TAManagerApp;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import properties_manager.PropertiesManager;
import csg.TAManagerProp;
import static csg.TAManagerProp.COURSE_DETAIL_TEXT;
import static csg.TAManagerProp.PROJECT_DATA_TEXT;
import static csg.TAManagerProp.RECITATION_DATA_TEXT;
import static csg.TAManagerProp.SCHEDULE_DATA_TEXT;
import static csg.TAManagerProp.TA_DATA_TEXT;
import csg.data.CourseDetail;
import csg.data.Recitation;
import csg.data.ScheduleItem;
import csg.data.Student;
import csg.style.TAStyle;
import csg.data.TAData;
import csg.data.TeachingAssistant;
import csg.data.Team;
import csg.test_bed.TestSave;
import java.time.format.DateTimeFormatter;
import javafx.geometry.Insets;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.image.Image;

/**
 * This class serves as the workspace component for the TA Manager application.
 * It provides all the user interface controls in the workspace area.
 *
 * @author Richard McKenna
 * @co-author Janmejay Bhavsar
 */
public class TAWorkspace extends AppWorkspaceComponent {

    // THIS PROVIDES US WITH ACCESS TO THE APP COMPONENTS
    TAManagerApp app;

    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    TAController controller;

    TabPane csgTabs;
    Tab CourseDetailTab;
    Tab TADataTab;
    Tab RecitationDataTab;
    Tab ScheduleDataTab;
    Tab ProjectDataTab;
    CourseDetailWorkspace cdw;
    VBox cdwPane;
    RecitationWorkspace rw;
    VBox rwPane;
    ScheduleWorkspace sw;
    VBox swPane;
    ProjectWorkspace pw;
    VBox pwPane;

    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    // FOR THE HEADER ON THE LEFT
    HBox tasHeaderBox;
    Label tasHeaderLabel;
    Button removeTAButton;

    // FOR THE TA TABLE
    TableView<TeachingAssistant> taTable;
    TableColumn undergrad;
    boolean isUndergrad;
    TableColumn<TeachingAssistant, String> nameColumn;
    TableColumn<TeachingAssistant, String> emailColumn;

    // THE TA INPUT
    HBox addBox;
    TextField nameTextField;
    TextField emailTextField;
    Button addButton;
    Button updateTaButton;
    Button clearButton;
    Button clearButton1;

    // THE HEADER ON THE RIGHT
    HBox officeHoursHeaderBox;
    Label officeHoursHeaderLabel;
    //Start and End Time for Office Hours 

    ComboBox newStartTime;
    ComboBox newEndTime;
    Button changeTimeButton;

    // THE OFFICE HOURS GRID
    GridPane officeHoursGridPane;
    HashMap<String, Pane> officeHoursGridTimeHeaderPanes;
    HashMap<String, Label> officeHoursGridTimeHeaderLabels;
    HashMap<String, Pane> officeHoursGridDayHeaderPanes;
    HashMap<String, Label> officeHoursGridDayHeaderLabels;
    HashMap<String, Pane> officeHoursGridTimeCellPanes;
    HashMap<String, Label> officeHoursGridTimeCellLabels;
    HashMap<String, Pane> officeHoursGridTACellPanes;
    HashMap<String, Label> officeHoursGridTACellLabels;

    /**
     * The contstructor initializes the user interface, except for the full
     * office hours grid, since it doesn't yet know what the hours will be until
     * a file is loaded or a new one is created.
     *
     * @param initApp
     */
    public TAWorkspace(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        TAData data = (TAData) app.getDataComponent();
        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        csgTabs = new TabPane();

        CourseDetailTab = new Tab(props.getProperty(COURSE_DETAIL_TEXT.toString()));
        TADataTab = new Tab(props.getProperty(TA_DATA_TEXT.toString()));
        RecitationDataTab = new Tab(props.getProperty(RECITATION_DATA_TEXT.toString()));
        ScheduleDataTab = new Tab(props.getProperty(SCHEDULE_DATA_TEXT.toString()));
        ProjectDataTab = new Tab(props.getProperty(PROJECT_DATA_TEXT.toString()));
        cdw = CourseDetailWorkspace.getCourseDetailWorkspace();
        cdw.getSubjectComboBox().getSelectionModel().select(data.getSubject());
        cdw.getNumberComboBox().getSelectionModel().select(data.getNumber());
//        System.out.println(data.getNumber());
        cdw.getSemesterComboBox().getSelectionModel().select(data.getSemester());
        cdw.getYearComboBox().getSelectionModel().select(data.getYear());
        cdw.getTitleTextField().setText(data.getTitle());
//        cdw.getInstructorNameTextField().setText(data.getInstructorName());
//        cdw.getInstructorHomeTextField().setText(data.getInstructorHome());
//        cdw.getExportDirectorySelectedLabel().setText(data.getExportedDirectory());
//        cdw.getSelectTemplateDirectoryButton().setText(data.getTemplateDirectory());
//        cdw.setBannerSchoolImage(data.getBannerSchoolImagePath());
//        cdw.setLeftFooterImage(data.getLeftFooterImagePath());
//        cdw.setRightFooterImage(data.getRightFooterImagePath());
//        cdw.getStyleSheetComboBox().getSelectionModel().select(data.getStylesheet());
        ObservableList<CourseDetail> cdTableData = data.getSitePages();
        cdw.getSitePagesTable().setItems(cdTableData);
        cdwPane = cdw.getCourseDetailPane();
        CourseDetailTab.setContent(cdwPane);
        rw = RecitationWorkspace.getRecitationWorkspace();
        ObservableList<Recitation> rTableData = data.getRecitations();
        rw.getRecitationTable().setItems(rTableData);
        rwPane = rw.getRecitationPane();
        RecitationDataTab.setContent(rwPane);
        sw = ScheduleWorkspace.getScheduleWorkspace();
//        String monday = data.getStartingMonday();
//        LocalDate startingMonday = LocalDate.parse(monday);
//        sw.getStartingMondayPicker().setValue(startingMonday);
//        String friday = data.getEndingFriday();
//        LocalDate endingFriday = LocalDate.parse(friday);
//        sw.getStartingMondayPicker().setValue(endingFriday);
        ObservableList<ScheduleItem> siTableData = data.getSchedules();
        sw.getScheduleTable().setItems(siTableData);
        swPane = sw.getScheduleDataPane();
        ScheduleDataTab.setContent(swPane);
        pw = ProjectWorkspace.getProjectWorkspace();
        ObservableList<Team> tTableData = data.getTeams();
        pw.getTeamTable().setItems(tTableData);
        ObservableList<Student> sTableData = data.getStudents();
        pw.getStudentTable().setItems(sTableData);
        pwPane = pw.getProjectDataPane();
        ProjectDataTab.setContent(pwPane);

        // INIT THE HEADER ON THE LEFT
        tasHeaderBox = new HBox();
        String tasHeaderText = props.getProperty(TAManagerProp.TAS_HEADER_TEXT.toString());
        tasHeaderLabel = new Label(tasHeaderText);
        removeTAButton = new Button("-");
        tasHeaderBox.getChildren().addAll(tasHeaderLabel, removeTAButton);

        // MAKE THE TABLE AND SETUP THE DATA MODEL
        taTable = new TableView();
        taTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        ObservableList<TeachingAssistant> tableData = data.getTeachingAssistants();
        taTable.setItems(tableData);
        String nameColumnText = props.getProperty(TAManagerProp.NAME_COLUMN_TEXT.toString());
        String emailColumnText = props.getProperty(TAManagerProp.EMAIL_COLUMN_TEXT.toString());
        String undergradColumnText = props.getProperty(TAManagerProp.UNDERGRAD_COLUMN_TEXT.toString());
        undergrad = new TableColumn(undergradColumnText);
        nameColumn = new TableColumn(nameColumnText);
        emailColumn = new TableColumn(emailColumnText);
//        undergrad.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<TeachingAssistant, CheckBox>, ObservableValue<CheckBox>>() {
//
//            @Override
//            public ObservableValue<CheckBox> call(
//                    TableColumn.CellDataFeatures<TeachingAssistant, CheckBox> arg0) {
//                CheckBox checkBox = new CheckBox();
//                checkBox.setOnMouseClicked(e -> {
//                    
//                    if(checkBox.isSelected())
//                    isUndergrad = true;
//                    else
//                        isUndergrad = false;
//                    System.out.println(isUndergrad);
//                });
//                
//                return new SimpleObjectProperty<CheckBox>(checkBox);
//            }
//        }
//        );
        undergrad.setCellValueFactory(new PropertyValueFactory<TeachingAssistant, Boolean>("undergrad"));
        undergrad.setCellFactory(CheckBoxTableCell.forTableColumn(undergrad));
        nameColumn.setCellValueFactory(
                new PropertyValueFactory<TeachingAssistant, String>("name")
        );
        emailColumn.setCellValueFactory(
                new PropertyValueFactory<TeachingAssistant, String>("email")
        );
        taTable.getColumns().add(undergrad);
        taTable.getColumns().add(nameColumn);
        taTable.getColumns().add(emailColumn);
        taTable.setEditable(true);
        taTable.setMinWidth(625);

        // ADD BOX FOR ADDING A TA
        String namePromptText = props.getProperty(TAManagerProp.NAME_PROMPT_TEXT.toString());
        String emailPromptText = props.getProperty(TAManagerProp.EMAIL_PROMPT_TEXT.toString());
        String startHourPromptText = props.getProperty(TAManagerProp.START_HOUR_PROMPT_TEXT.toString());
        String endHourPromptText = props.getProperty(TAManagerProp.END_HOUR_PROMPT_TEXT.toString());
        String addButtonText = props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString());
        String updateTaButtonText = props.getProperty(TAManagerProp.UPDATE_TA_BUTTON_TEXT.toString());
        String clearButtonText = props.getProperty(TAManagerProp.CLEAR_BUTTON_TEXT.toString());
        String changeTimeButtonText = props.getProperty(TAManagerProp.CHANGE_TIME_BUTTON_TEXT.toString());

        changeTimeButton = new Button(changeTimeButtonText);
        newStartTime = new ComboBox();
        newEndTime = new ComboBox();
        for (int i = 0; i < 24; i++) {
            newStartTime.getItems().addAll(buildCellText(i, "00"));
        }
        for (int i = 0; i < 24; i++) {
            newEndTime.getItems().addAll(buildCellText(i, "00"));
        }

        nameTextField = new TextField();
        emailTextField = new TextField();
        nameTextField.setPromptText(namePromptText);
        emailTextField.setPromptText(emailPromptText);
        addButton = new Button(addButtonText);
        updateTaButton = new Button(updateTaButtonText);
        clearButton = new Button(clearButtonText);
        clearButton1 = new Button(clearButtonText);
        addBox = new HBox();
        newStartTime.setPromptText(startHourPromptText);
        newEndTime.setPromptText(endHourPromptText);

        nameTextField.prefWidthProperty().bind(addBox.widthProperty().multiply(.4));
        emailTextField.prefWidthProperty().bind(addBox.widthProperty().multiply(.4));
        addButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        updateTaButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        clearButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        updateTaButton.prefHeightProperty().bind(addBox.heightProperty().multiply(1));
        clearButton.prefHeightProperty().bind(addBox.heightProperty().multiply(1));
        clearButton1.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        clearButton1.prefHeightProperty().bind(addBox.heightProperty().multiply(1));

        addBox.getChildren().add(nameTextField);
        addBox.getChildren().add(emailTextField);
        addBox.getChildren().add(addButton);
        addBox.getChildren().add(clearButton1);

        // INIT THE HEADER ON THE RIGHT
        officeHoursHeaderBox = new HBox();
        String officeHoursGridText = props.getProperty(TAManagerProp.OFFICE_HOURS_SUBHEADER.toString());
        officeHoursHeaderLabel = new Label(officeHoursGridText);
        officeHoursHeaderBox.getChildren().add(officeHoursHeaderLabel);
        officeHoursHeaderBox.getChildren().add(newStartTime);
        officeHoursHeaderBox.getChildren().add(newEndTime);
        officeHoursHeaderBox.getChildren().add(changeTimeButton);

        // THESE WILL STORE PANES AND LABELS FOR OUR OFFICE HOURS GRID
        officeHoursGridPane = new GridPane();
        officeHoursGridTimeHeaderPanes = new HashMap();
        officeHoursGridTimeHeaderLabels = new HashMap();
        officeHoursGridDayHeaderPanes = new HashMap();
        officeHoursGridDayHeaderLabels = new HashMap();
        officeHoursGridTimeCellPanes = new HashMap();
        officeHoursGridTimeCellLabels = new HashMap();
        officeHoursGridTACellPanes = new HashMap();
        officeHoursGridTACellLabels = new HashMap();

        // ORGANIZE THE LEFT AND RIGHT PANES
        VBox leftPane = new VBox();
        leftPane.getChildren().add(tasHeaderBox);
        leftPane.getChildren().add(taTable);
        leftPane.getChildren().add(addBox);

        VBox rightPane = new VBox();
        rightPane.getChildren().add(officeHoursHeaderBox);
        rightPane.getChildren().add(officeHoursGridPane);

        // BOTH PANES WILL NOW GO IN A SPLIT PANE
//        SplitPane sPane = new SplitPane(leftPane, new ScrollPane(rightPane));
        HBox taBox = new HBox();
        taBox.getChildren().addAll(leftPane, new ScrollPane(rightPane));
        taBox.setSpacing(5);
        taBox.setPadding(new Insets(5));
        taBox.setStyle("-fx-background-color:#f4b042");
        workspace = new BorderPane();
        csgTabs.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
        csgTabs.getTabs().add(CourseDetailTab);
        csgTabs.getTabs().add(TADataTab);
        csgTabs.getTabs().add(RecitationDataTab);
        csgTabs.getTabs().add(ScheduleDataTab);
        csgTabs.getTabs().add(ProjectDataTab);
        csgTabs.tabMinWidthProperty().setValue(230);
        TADataTab.setContent(taBox);

        // AND PUT EVERYTHING IN THE WORKSPACE
        ((BorderPane) workspace).setCenter(csgTabs);

        // MAKE SURE THE TABLE EXTENDS DOWN FAR ENOUGH
        taTable.prefHeightProperty().bind(workspace.heightProperty().multiply(1.9));

        // NOW LET'S SETUP THE EVENT HANDLING
        controller = new TAController(app);

        // CONTROLS FOR ADDING TAs
        removeTAButton.setOnAction(e -> {
            controller.handleKeyPress(KeyCode.BACK_SPACE);
        });
        rw.getRemoveRecitationButton().setOnAction(e -> {
            controller.handleKeyPress(KeyCode.BACK_SPACE);
        });
        sw.getRemoveScheduleButton().setOnAction(e -> {
            controller.handleKeyPress(KeyCode.BACK_SPACE);
        });
        pw.getRemoveTeamButton().setOnAction(e -> {
            controller.handleKeyPress(KeyCode.BACK_SPACE);
        });
        pw.getRemoveStudentButton().setOnAction(e -> {
            controller.handleKeyPress(KeyCode.BACK_SPACE);
        });
        nameTextField.setOnAction(e -> {
            controller.handleAddTA();
        });
        emailTextField.setOnAction(e -> {
            controller.handleAddTA();
        });
        addButton.setOnAction(e -> {
            controller.handleAddTA();
            data.populateTAComboBox();
        });
        rw.getAddButton().setOnAction(e -> {
            if(rw.recitationTable.getSelectionModel().getSelectedItem()!=null)
                controller.handleUpdateRecitation();
            else
            controller.handleAddRecitation();
        });
        sw.getAddScheduleButton().setOnAction(e -> {
            if(sw.scheduleTable.getSelectionModel().getSelectedItem()!=null)
                controller.handleUpdateScheduleItem();
            else
                controller.handleAddSchedule();
        });
        pw.getAddTeamButton().setOnAction(e -> {
            if(pw.teamTable.getSelectionModel().getSelectedItem()!=null)
               controller.handleUpdateTeam();
            else
                controller.handleAddTeam();
            data.populateTeamComboBox();
        });
        pw.getAddStudentButton().setOnAction(e -> {
            if(pw.studentTable.getSelectionModel().getSelectedItem()!=null)
                controller.handleUpdateStudent();
            else
                controller.handleAddStudent();
        });
        changeTimeButton.setOnAction(e -> {
            String startTime = (String) newStartTime.getValue();
            String endTime = (String) newEndTime.getValue();
            System.out.println(startTime);
            System.out.println(endTime);
            controller.handleChangeTime(startTime, endTime);

        });
        updateTaButton.setOnAction(e -> {
            controller.handleUpdateTA();
        });
        clearButton.setOnAction(e -> {
            addBox.getChildren().add(addButton);
            addBox.getChildren().add(clearButton1);
            addBox.getChildren().remove(updateTaButton);
            addBox.getChildren().remove(clearButton);
            nameTextField.clear();
            emailTextField.clear();
            nameTextField.setPromptText(namePromptText);
            emailTextField.setPromptText(emailPromptText);

        });
        clearButton1.setOnAction(e -> {
            nameTextField.clear();
            emailTextField.clear();
            nameTextField.setPromptText(namePromptText);
            emailTextField.setPromptText(emailPromptText);
        });
        rw.getClearButton().setOnAction(e -> {
            rw.sectionTextField.clear();
            rw.instructorTextField.clear();
            rw.day_timeTextField.clear();
            rw.locationTextField.clear();
            rw.getSupervisingTAComboBox1().getSelectionModel().clearSelection();
            rw.getSupervisingTAComboBox2().getSelectionModel().clearSelection();
            rw.recitationTable.getSelectionModel().clearSelection();
        });
        sw.getClearScheduleButton().setOnAction(e -> {
            sw.typeComboBox.getSelectionModel().clearSelection();
            sw.datePicker.setValue(null);
            sw.timeTextField.clear();
            sw.titleTextField.clear();
            sw.topicTextField.clear();
            sw.linkScheduleTextField.clear();
            sw.criteriaTextField.clear();
            sw.scheduleTable.getSelectionModel().clearSelection();
        });
        pw.getClearTeamButton().setOnAction(e -> {
            pw.nameTextField.clear();
            pw.colorPicker.setValue(null);
            pw.textColorPicker.setValue(null);
            pw.linkTextField.clear();
            pw.getTeamTable().getSelectionModel().clearSelection();
        });
        pw.getClearStudentButton().setOnAction(e -> {
            pw.firstNameTextField.clear();
            pw.lastNameTextField.clear();
            pw.teamComboBox.getSelectionModel().clearSelection();
            pw.roleTextField.clear();
            pw.studentTable.getSelectionModel().clearSelection();
        });
        app.getGUI().getPrimaryScene().setOnKeyPressed(e -> {
            if (e.isControlDown() && e.getCode() == (KeyCode.Y)) {
                System.out.println("Workspace Control Y");
                controller.handleReDoTransaction();
            } else if (e.isControlDown() && e.getCode() == (KeyCode.Z)) {
                System.out.println("Workspace Control Z Pressed");
                controller.handleUndoTransaction();
            }

        });

        taTable.setFocusTraversable(true);
        taTable.setOnKeyPressed(e -> {
            controller.handleKeyPress(e.getCode());
        });
        rw.recitationTable.setFocusTraversable(true);
        rw.recitationTable.setOnKeyPressed(e -> {
            controller.handleKeyPress(e.getCode());
        });
        sw.scheduleTable.setFocusTraversable(true);
        sw.scheduleTable.setOnKeyPressed(e -> {
            controller.handleKeyPress(e.getCode());
        });
        pw.teamTable.setFocusTraversable(true);
        pw.teamTable.setOnKeyPressed(e -> {
            controller.handleKeyPress(e.getCode());
        });
        pw.studentTable.setFocusTraversable(true);
        pw.studentTable.setOnKeyPressed(e -> {
            controller.handleKeyPress(e.getCode());
        });
        taTable.setOnMousePressed(e -> {
            addBox.getChildren().clear();
            controller.handleTaClicked(workspace, addBox);
            System.out.println("Clicked TA");
        });
        rw.recitationTable.setOnMousePressed(e -> {
            controller.handleRecitationClicked();
        });
        sw.scheduleTable.setOnMousePressed(e -> {
            controller.handleScheduleClicked();
        });
        pw.teamTable.setOnMousePressed(e -> {
            controller.handleTeamClicked();
        });
        pw.studentTable.setOnMousePressed(e -> {
            controller.handleStudentClicked();
        });
        cdw.changeExportDirectoryButton.setOnAction(e -> {
            controller.handleChangeExportDirectory();
            data.setSubject(cdw.subjectComboBox.getSelectionModel().getSelectedItem().toString());
            data.setNumber(cdw.numberComboBox.getSelectionModel().getSelectedItem().toString());
            data.setSemester(cdw.semesterComboBox.getSelectionModel().getSelectedItem().toString());
            data.setYear(cdw.yearComboBox.getSelectionModel().getSelectedItem().toString());
            data.setTitle(cdw.titleTextField.getText());
            data.setInstructorName(cdw.instructorNameTextField.getText());
            data.setInstructorHome(cdw.instructorHomeTextField.getText());
            data.setExportedDirectory(cdw.exportDirectorySelectedLabel.getText());
            data.setTemplateDirectory(cdw.selectedTemplateLabel.getText());
            data.setStylesheet(cdw.styleSheetComboBox.getSelectionModel().getSelectedItem().toString());
            data.setStartingMonday(sw.mondayDate.getValue());
            data.setEndingFriday(sw.fridayDate.getValue());
        });
        cdw.selectTemplateDirectoryButton.setOnAction(e -> {
            controller.handleSelectTemplateDirectory();
        });
        cdw.changeBannerSchoolImageButton.setOnAction(e -> {
            controller.handleChangeBannerSchoolImage();
        });
        cdw.changeLeftFooterImageButton.setOnAction(e -> {
            controller.handleChangeLeftFooterImage();
        });
        cdw.changeRightFooterImageButton.setOnAction(e -> {
            controller.handleChangeRightFooterImage();
        });
    }

    // WE'LL PROVIDE AN ACCESSOR METHOD FOR EACH VISIBLE COMPONENT
    // IN CASE A CONTROLLER OR STYLE CLASS NEEDS TO CHANGE IT
    public HBox getTAsHeaderBox() {
        return tasHeaderBox;
    }

    public Label getTAsHeaderLabel() {
        return tasHeaderLabel;
    }

    public TableView getTATable() {
        return taTable;
    }

    public Boolean isUndergrad() {
        return isUndergrad;
    }

    public HBox getAddBox() {
        return addBox;
    }

    public TextField getNameTextField() {
        return nameTextField;
    }

    public TextField getEmailTextField() {
        return emailTextField;
    }

    public Button getAddButton() {
        return addButton;
    }

    public Button getUpdateTaButton() {
        return updateTaButton;
    }

    public Button getClearButton() {
        return clearButton;
    }

    public Button getClearButton1() {
        return clearButton1;
    }

    public Button getRemoveTAButton() {
        return removeTAButton;
    }

    public Button getChangeTimeButton() {
        return changeTimeButton;
    }

    public ComboBox getNewStartBox() {
        return newStartTime;
    }

    public ComboBox getNewEndBox() {
        return newEndTime;

    }

    public HBox getOfficeHoursSubheaderBox() {
        return officeHoursHeaderBox;
    }

    public TAController getController() {
        return controller;
    }

    public Label getOfficeHoursSubheaderLabel() {
        return officeHoursHeaderLabel;
    }

    public GridPane getOfficeHoursGridPane() {
        return officeHoursGridPane;
    }

    public HashMap<String, Pane> getOfficeHoursGridTimeHeaderPanes() {
        return officeHoursGridTimeHeaderPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTimeHeaderLabels() {
        return officeHoursGridTimeHeaderLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridDayHeaderPanes() {
        return officeHoursGridDayHeaderPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridDayHeaderLabels() {
        return officeHoursGridDayHeaderLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridTimeCellPanes() {
        return officeHoursGridTimeCellPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTimeCellLabels() {
        return officeHoursGridTimeCellLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridTACellPanes() {
        return officeHoursGridTACellPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTACellLabels() {
        return officeHoursGridTACellLabels;
    }

    public String getCellKey(Pane testPane) {
        for (String key : officeHoursGridTACellLabels.keySet()) {
            if (officeHoursGridTACellPanes.get(key) == testPane) {
                return key;
            }
        }
        return null;
    }

    public Label getTACellLabel(String cellKey) {
        return officeHoursGridTACellLabels.get(cellKey);
    }

    public Pane getTACellPane(String cellPane) {
        return officeHoursGridTACellPanes.get(cellPane);
    }

    public String buildCellKey(int col, int row) {
        return "" + col + "_" + row;
    }

    public String buildCellText(int militaryHour, String minutes) {
        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        String cellText = "" + hour + ":" + minutes;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }
    

    @Override
    public void resetWorkspace() {
        // CLEAR OUT THE GRID PANE
        officeHoursGridPane.getChildren().clear();

        // AND THEN ALL THE GRID PANES AND LABELS
        officeHoursGridTimeHeaderPanes.clear();
        officeHoursGridTimeHeaderLabels.clear();
        officeHoursGridDayHeaderPanes.clear();
        officeHoursGridDayHeaderLabels.clear();
        officeHoursGridTimeCellPanes.clear();
        officeHoursGridTimeCellLabels.clear();
        officeHoursGridTACellPanes.clear();
        officeHoursGridTACellLabels.clear();
        CourseDetailWorkspace cdw = CourseDetailWorkspace.getCourseDetailWorkspace();
        cdw.subjectComboBox.getSelectionModel().clearSelection();
        cdw.numberComboBox.getSelectionModel().clearSelection();
        cdw.semesterComboBox.getSelectionModel().clearSelection();
        cdw.yearComboBox.getSelectionModel().clearSelection();
        cdw.titleTextField.clear();
        cdw.instructorNameTextField.clear();
        cdw.instructorHomeTextField.clear();
        cdw.exportDirectorySelectedLabel.setText("");
        cdw.selectedTemplateLabel.setText("");
        
        cdw.bannerSchoolIV.setImage(new Image("https://www.google.com/search?q=blank+image&espv=2&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiHh-qp1r3TAhWqzIMKHa01AnAQ_AUIBigB&biw=1280&bih=676#tbm=isch&q=blank+image&chips=q:blank%20image,g_1:grey&imgrc=u29WzXjIQMvUoM:"));
        cdw.leftFooterIV.setImage(new Image("https://www.google.com/search?q=blank+image&espv=2&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiHh-qp1r3TAhWqzIMKHa01AnAQ_AUIBigB&biw=1280&bih=676#tbm=isch&q=blank+image&chips=q:blank%20image,g_1:grey&imgrc=u29WzXjIQMvUoM:"));
        cdw.rightFooterIV.setImage(new Image("https://www.google.com/search?q=blank+image&espv=2&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiHh-qp1r3TAhWqzIMKHa01AnAQ_AUIBigB&biw=1280&bih=676#tbm=isch&q=blank+image&chips=q:blank%20image,g_1:grey&imgrc=u29WzXjIQMvUoM:"));
        cdw.styleSheetComboBox.getSelectionModel().clearSelection();
        rw.sectionTextField.clear();
        rw.instructorTextField.clear();
        rw.day_timeTextField.clear();
        rw.locationTextField.clear();
        rw.supervisingTAComboBox1.getSelectionModel().clearSelection();
        rw.supervisingTAComboBox2.getSelectionModel().clearSelection();
        sw.mondayDate.setValue(null);
        sw.fridayDate.setValue(null);
        sw.typeComboBox.getSelectionModel().clearSelection();
        sw.datePicker.setValue(null);
        sw.timeTextField.clear();
        sw.titleTextField.clear();
        sw.topicTextField.clear();
        sw.linkScheduleTextField.clear();
        sw.criteriaTextField.clear();
        pw.nameTextField.clear();
        pw.colorPicker.setValue(null);
        pw.textColorPicker.setValue(null);
        pw.linkTextField.clear();
        pw.firstNameTextField.clear();
        pw.lastNameTextField.clear();
        pw.teamComboBox.getSelectionModel().clearSelection();
        pw.roleTextField.clear();
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        TAData taData = (TAData) dataComponent;

        reloadOfficeHoursGrid(taData);
    }

    public void reloadOfficeHoursGrid(TAData dataComponent) {
        ArrayList<String> gridHeaders = dataComponent.getGridHeaders();

        // ADD THE TIME HEADERS
        for (int i = 0; i < 2; i++) {
            addCellToGrid(dataComponent, officeHoursGridTimeHeaderPanes, officeHoursGridTimeHeaderLabels, i, 0);
            dataComponent.getCellTextProperty(i, 0).set(gridHeaders.get(i));
        }

        // THEN THE DAY OF WEEK HEADERS
        for (int i = 2; i < 7; i++) {
            addCellToGrid(dataComponent, officeHoursGridDayHeaderPanes, officeHoursGridDayHeaderLabels, i, 0);
            dataComponent.getCellTextProperty(i, 0).set(gridHeaders.get(i));
        }

        // THEN THE TIME AND TA CELLS
        int row = 1;
        for (int i = dataComponent.getStartHour(); i < dataComponent.getEndHour(); i++) {
            // START TIME COLUMN
            int col = 0;
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row);
            dataComponent.getCellTextProperty(col, row).set(buildCellText(i, "00"));
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row + 1);
            dataComponent.getCellTextProperty(col, row + 1).set(buildCellText(i, "30"));

            // END TIME COLUMN
            col++;
            int endHour = i;
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row);
            dataComponent.getCellTextProperty(col, row).set(buildCellText(endHour, "30"));
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row + 1);
            dataComponent.getCellTextProperty(col, row + 1).set(buildCellText(endHour + 1, "00"));
            col++;

            // AND NOW ALL THE TA TOGGLE CELLS
            while (col < 7) {
                addCellToGrid(dataComponent, officeHoursGridTACellPanes, officeHoursGridTACellLabels, col, row);
                addCellToGrid(dataComponent, officeHoursGridTACellPanes, officeHoursGridTACellLabels, col, row + 1);
                col++;
            }
            row += 2;
        }

        // CONTROLS FOR TOGGLING TA OFFICE HOURS
        officeHoursGridTACellPanes.values().stream().map((p) -> {
            p.setFocusTraversable(true);
            return p;
        }).map((p) -> {
            p.setOnKeyPressed(e -> {
                controller.handleKeyPress(e.getCode());
            });
            return p;
        }).map((p) -> {
            p.setOnMouseClicked(e -> {
                controller.handleCellToggle((Pane) e.getSource());
            });
            return p;
        }).map((p) -> {
            p.setOnMouseExited(e -> {
                controller.handleGridCellMouseExited((Pane) e.getSource());
            });
            return p;
        }).forEachOrdered((p) -> {
            p.setOnMouseEntered(e -> {
                controller.handleGridCellMouseEntered((Pane) e.getSource());
            });
        });

        // AND MAKE SURE ALL THE COMPONENTS HAVE THE PROPER STYLE
        TAStyle taStyle = (TAStyle) app.getStyleComponent();
        taStyle.initOfficeHoursGridStyle();
    }

    public void addCellToGrid(TAData dataComponent, HashMap<String, Pane> panes, HashMap<String, Label> labels, int col, int row) {
        // MAKE THE LABEL IN A PANE
        Label cellLabel = new Label("");
        HBox cellPane = new HBox();
        cellPane.setAlignment(Pos.CENTER);
        cellPane.getChildren().add(cellLabel);

        // BUILD A KEY TO EASILY UNIQUELY IDENTIFY THE CELL
        String cellKey = dataComponent.getCellKey(col, row);
        cellPane.setId(cellKey);
        cellLabel.setId(cellKey);

        // NOW PUT THE CELL IN THE WORKSPACE GRID
        officeHoursGridPane.add(cellPane, col, row);

        // AND ALSO KEEP IN IN CASE WE NEED TO STYLIZE IT
        panes.put(cellKey, cellPane);
        labels.put(cellKey, cellLabel);

        // AND FINALLY, GIVE THE TEXT PROPERTY TO THE DATA MANAGER
        // SO IT CAN MANAGE ALL CHANGES
        dataComponent.setCellProperty(col, row, cellLabel.textProperty());
    }
    
    public TableView getRecitationTable(){
        RecitationWorkspace rw = RecitationWorkspace.getRecitationWorkspace();
        return rw.recitationTable;
    }
    
    public TableView getScheduleTable(){
        ScheduleWorkspace sw = ScheduleWorkspace.getScheduleWorkspace();
        return sw.scheduleTable;
    }
    
    public TableView getTeamTable(){
        ProjectWorkspace pw = ProjectWorkspace.getProjectWorkspace();
        return pw.teamTable;
    }
    
    public TableView getStudentTable(){
        ProjectWorkspace pw = ProjectWorkspace.getProjectWorkspace();
        return pw.studentTable;
    }
    
//    public void initCourseDetails(TAData data){
//        data = (TAData)app.getDataComponent();
//        CourseDetailWorkspace cdw = CourseDetailWorkspace.getCourseDetailWorkspace();
//        String courseName = cdw.subjectComboBox.getSelectionModel().getSelectedItem().toString();
//        data.setSubject(courseName);
//        String courseNumber = cdw.numberComboBox.getSelectionModel().getSelectedItem().toString();
//        data.setNumber(courseNumber);
//        String title = cdw.titleTextField.getText();
//        data.setTitle(title);
//        String instructorName = cdw.instructorNameTextField.getText();
//        data.setInstructorName(instructorName);
//        String instructorHome = cdw.instructorHomeTextField.getText();
//        data.setInstructorHome(instructorHome);
//    }
}
