/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.data.Recitation;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import jtps.jTPS_Transaction;
import csg.data.TAData;
import csg.data.TeachingAssistant;

/**
 *
 * @author khurr
 */
public class DeleteRecitation_Transaction implements jTPS_Transaction {

    private final Recitation rec;
    private final TAData data;

    public DeleteRecitation_Transaction(Recitation rec, TAData data) {
        this.data = data;
        this.rec = rec; 

    }

    @Override
    public void doTransaction() {  //control Y 
        String recSection = rec.getSection();
        data.removeRecitation(recSection);
}

    @Override
    public void undoTransaction() {
        data.addRecitation(rec.getSection(), rec.getInstructor(), rec.getDay_Time(), rec.getLocation(), rec.getTA1(), rec.getTA2());
    }
}
