/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;


import jtps.jTPS_Transaction;
import csg.TAManagerApp;
import csg.data.Recitation;
import csg.data.ScheduleItem;
import csg.data.TAData;
import csg.data.TeachingAssistant;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author khurr
 */
public class UpdateSchedule_Transaction implements jTPS_Transaction {

    private final String oldType;
    private final String newType;
    private final String oldDate;
    private final String newDate;
    private final String oldTime;
    private final String newTime;
    private final String oldTitle;
    private final String newTitle;
    private final String oldTopic;
    private final String newTopic;
    private final String oldLink;
    private final String newLink;
    private final String oldCriteria;
    private final String newCriteria;
    private final TAData taData;
    private final ScheduleItem s;
    private final TAManagerApp app; 
    private final TAWorkspace transWorkspace; 
    private final ScheduleWorkspace sWorkspace;

    public UpdateSchedule_Transaction(String orgType, String type, String orgDate, String date, String orgTime, String time, String orgTitle, String title, String orgTopic, String topic, String orgLink, String link, String orgCriteria, String criteria, TAData data, TAManagerApp taApp, TAWorkspace workspace) {
        oldType = orgType;
        newType = type;
        oldDate = orgDate;
        newDate = date;
        oldTime = orgTime;
        newTime = time;
        oldTitle = orgTitle;
        newTitle = title;
        oldTopic = orgTopic;
        newTopic = topic;
        oldLink = orgLink;
        newLink = link;
        oldCriteria = orgCriteria;
        newCriteria = criteria;
        taData = data;
        s = data.getSchedule(orgType, orgTitle);
        app=taApp; 
        transWorkspace=workspace; 
        sWorkspace = ScheduleWorkspace.getScheduleWorkspace();
    }

    @Override
    public void doTransaction() {  try {
        //Control Y
        System.out.println("updateTA doTransaction ");
        ScheduleItem si = taData.getSchedule(oldType, oldTitle);
        si.setType(newType);
        si.setDate(newDate);
        si.setTime(newTime);
        si.setTitle(newTitle);
        si.setTopic(newTopic);
        si.setLink(newLink);
        si.setCriteria(newCriteria);
        s.setType(newType);                        // MOVED TO TRANSACTION CASE 
        s.setDate(newDate);
        s.setTime(newTime);
        s.setTitle(newTitle);
        s.setTopic(newTopic);
        s.setLink(newLink);
        s.setCriteria(newCriteria);
        sWorkspace.typeComboBox.getSelectionModel().select(newType);
        System.out.println(newDate);
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        Date scheduleDate = df.parse(newDate);
        LocalDate date = scheduleDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        sWorkspace.datePicker.setValue(date);
        sWorkspace.timeTextField.setText(newTime);
        sWorkspace.titleTextField.setText(newTitle);
        sWorkspace.topicTextField.setText(newTopic);
        sWorkspace.linkScheduleTextField.setText(newLink);
        sWorkspace.criteriaTextField.setText(newCriteria);
        // transWorkspace.taTable.refresh();
        } catch (ParseException ex) {
            Logger.getLogger(UpdateSchedule_Transaction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("updateTA undoTransaction ");
        ScheduleItem si = taData.getSchedule(newType, newTitle);
        si.setType(oldType);
        si.setDate(oldDate);
        si.setTime(oldTime);
        si.setTitle(oldTitle);
        si.setTopic(oldTopic);
        si.setLink(oldLink);
        si.setCriteria(oldCriteria);
        s.setType(oldType);                        // MOVED TO TRANSACTION CASE 
        s.setDate(oldDate);
        s.setTime(oldTime);
        s.setTitle(oldTitle);
        s.setTopic(oldTopic);
        s.setLink(oldLink);
        s.setCriteria(oldCriteria);
        sWorkspace.typeComboBox.getSelectionModel().select(oldType);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate dateValue = LocalDate.parse(oldDate, formatter);
        sWorkspace.datePicker.setValue(dateValue);
        sWorkspace.timeTextField.setText(oldTime);
        sWorkspace.titleTextField.setText(oldTitle);
        sWorkspace.topicTextField.setText(oldTopic);
        sWorkspace.linkScheduleTextField.setText(oldLink);
        sWorkspace.criteriaTextField.setText(oldCriteria);
        //transWorkspace.taTable.refresh();

    }

}
