/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import jtps.jTPS_Transaction;
import csg.data.TAData;
import csg.data.TeachingAssistant;
import csg.data.Team;

/**
 *
 * @author khurr
 */
public class DeleteTeam_Transaction implements jTPS_Transaction {

    private final Team team;
    private final TAData data;

    public DeleteTeam_Transaction(Team team, TAData data) {
        this.data = data;
        this.team = team; 
    }

    @Override
    public void doTransaction() {  //control Y  
        String teamName = team.getName();
        data.removeTeam(teamName);
}

    @Override
    public void undoTransaction() {
        data.addTeam(team.getName(), team.getColor(), team.getTextColor(), team.getLink());
    }
}
