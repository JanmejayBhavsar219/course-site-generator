/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import csg.data.TAData;
import csg.data.TeachingAssistant;

/**
 *
 * @author khurr
 */
public class AddTA_Transaction implements jTPS_Transaction {

    private final String taEmail;
    private final boolean taUndergrad;
    private final String taName;
    private final TAData data;

    public AddTA_Transaction(boolean undergrad, String name, String email, TAData taData) {
        taUndergrad = undergrad;
        taEmail = email;
        taName = name;
        data = taData;
    }

    @Override
    public void doTransaction() {  //Control Y 
        data.addTA(taUndergrad, taName, taEmail);
        //Collections.sort(data.getTeachingAssistants());
        System.out.println("doTransaction");
    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("undo Transaction");
        ObservableList<TeachingAssistant> taList = data.getTeachingAssistants();
        for (TeachingAssistant ta : taList) {
            if (taName.equals(ta.getName())) {
                taList.remove(ta);
                return;
            }

        }
        // data.removeTA(taName);

    }

}
