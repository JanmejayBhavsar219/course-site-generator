package csg.workspace;

import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import jtps.jTPS;
import properties_manager.PropertiesManager;
import csg.TAManagerApp;
import static csg.TAManagerProp.*;
import csg.data.Recitation;
import csg.data.TAData;
import javafx.geometry.Insets;

/**
 *
 * @author Janmejay Bhavsar
 */
public class RecitationWorkspace extends AppWorkspaceComponent{
    TAManagerApp app;
    public jTPS jTPSObject;
    static RecitationWorkspace singleton = null;
    Label recitationLabel;
    Button removeRecitationButton;
    HBox recitationHeaderHBox;
    TableView recitationTable;
    TableColumn sectionColumn;
    TableColumn instructorColumn;
    TableColumn day_timeColumn;
    TableColumn locationColumn;
    TableColumn TAColumn1;
    TableColumn TAColumn2;
    VBox recitationTableVBox;
    Label add_editLabel;
    VBox add_editBox;
    Label sectionLabel;
    TextField sectionTextField;
    Label instructorLabel;
    TextField instructorTextField;
    Label day_timeLabel;
    TextField day_timeTextField;
    Label locationLabel;
    TextField locationTextField;
    Label supervisingTALabel1;
    ComboBox supervisingTAComboBox1;
    ObservableList TAList;
    Label supervisingTALabel2;
    ComboBox supervisingTAComboBox2;
    Button addButton;
    Button clearButton; 
    GridPane add_editPane;
    VBox recitationPanel;
    TAData data;
    
    public RecitationWorkspace(){
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        recitationLabel = new Label(props.getProperty(RECITATION_HEADER.toString()));
        removeRecitationButton = new Button("-");
        recitationHeaderHBox = new HBox();
        recitationHeaderHBox.getChildren().addAll(recitationLabel, removeRecitationButton);
        
        recitationTable = new TableView();
        recitationTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
//        FXCollections.observableArrayList(
//            new Recitation("R02", "McKenna", "Wed 3:30pm-4:23pm", "Old CS 2114", "Jane Doe", "Joe Shmo"),
//            new Recitation("R05", "Banerjee", "Tues 5:30pm-6:23pm", "Old CS 2114", "", "")       
//        );
        sectionColumn = new TableColumn(props.getProperty(SECTION_TEXT.toString()));
        instructorColumn = new TableColumn(props.getProperty(INSTRUCTOR_TEXT.toString()));
        day_timeColumn = new TableColumn(props.getProperty(DAY_TIME_TEXT.toString()));
        locationColumn = new TableColumn(props.getProperty(LOCATION_TEXT.toString()));
        TAColumn1 = new TableColumn(props.getProperty(TA_TEXT.toString()));
        TAColumn2 = new TableColumn(props.getProperty(TA_TEXT.toString()));
        sectionColumn.setCellValueFactory(
                new PropertyValueFactory<String, String>("section")
        );
        instructorColumn.setCellValueFactory(
                new PropertyValueFactory<String, String>("instructor")
        );
        day_timeColumn.setCellValueFactory(
                new PropertyValueFactory<String, String>("day_time")
        );
        locationColumn.setCellValueFactory(
                new PropertyValueFactory<String, String>("location")
        );
        TAColumn1.setCellValueFactory(
                new PropertyValueFactory<String, String>("TA1")
        );
        TAColumn2.setCellValueFactory(
                new PropertyValueFactory<String, String>("TA2")
        );
        recitationTable.getColumns().addAll(sectionColumn, instructorColumn, day_timeColumn, locationColumn, TAColumn1, TAColumn2);
        recitationTableVBox = new VBox();
        recitationTableVBox.getChildren().addAll(recitationTable);
        recitationTableVBox.setStyle("-fx-border-style: solid inside;"+"-fx-border-width: 2;");
        add_editLabel = new Label(props.getProperty(ADD_EDIT_TEXT.toString()));
        add_editBox = new VBox();
        sectionLabel = new Label(props.getProperty(SECTION_TEXT.toString()));
        sectionTextField = new TextField();
        instructorLabel = new Label(props.getProperty(INSTRUCTOR_TEXT.toString()));
        instructorTextField = new TextField();
        day_timeLabel = new Label(props.getProperty(DAY_TIME_TEXT.toString()));
        day_timeTextField = new TextField();
        locationLabel = new Label(props.getProperty(LOCATION_TEXT.toString()));
        locationTextField = new TextField();
        supervisingTALabel1 = new Label(props.getProperty(SUPERVISING_TA_TEXT.toString()));
        supervisingTAComboBox1 = new ComboBox();
        supervisingTALabel2 = new Label(props.getProperty(SUPERVISING_TA_TEXT.toString()));
        supervisingTAComboBox2 = new ComboBox();
        addButton = new Button(props.getProperty(ADD_UPDATE_BUTTON_TEXT.toString()));
        clearButton = new Button(props.getProperty(CLEAR_BUTTON_TEXT.toString()));
        add_editPane = new GridPane();
        add_editPane.add(sectionLabel, 0, 1);
        add_editPane.add(sectionTextField, 1, 1);
        add_editPane.add(instructorLabel, 0, 2);
        add_editPane.add(instructorTextField, 1, 2);
        add_editPane.add(day_timeLabel, 0, 3);
        add_editPane.add(day_timeTextField, 1, 3);
        add_editPane.add(locationLabel, 0, 4);
        add_editPane.add(locationTextField, 1, 4);
        add_editPane.add(supervisingTALabel1, 0, 5);
        add_editPane.add(supervisingTAComboBox1, 1, 5);
        add_editPane.add(supervisingTALabel2, 0, 6);
        add_editPane.add(supervisingTAComboBox2, 1, 6);
        add_editPane.add(addButton, 0, 7);
        add_editPane.add(clearButton, 1, 7);
        GridPane.setMargin(sectionLabel, new Insets(0, 0, 5, 3));
        GridPane.setMargin(sectionTextField, new Insets(0, 0, 5, 0));
        GridPane.setMargin(instructorLabel, new Insets(0, 0, 5, 3));
        GridPane.setMargin(instructorTextField, new Insets(0, 0, 5, 0));
        GridPane.setMargin(day_timeLabel, new Insets(0, 0, 5, 3));
        GridPane.setMargin(day_timeTextField, new Insets(0, 0, 5, 0));
        GridPane.setMargin(locationLabel, new Insets(0, 0, 5, 3));
        GridPane.setMargin(locationTextField, new Insets(0, 0, 5, 0));
        GridPane.setMargin(supervisingTALabel1, new Insets(0, 0, 5, 3));
        GridPane.setMargin(supervisingTAComboBox1, new Insets(0, 0, 5, 0));
        GridPane.setMargin(supervisingTALabel2, new Insets(0, 0, 5, 3));
        GridPane.setMargin(supervisingTAComboBox2, new Insets(0, 0, 5, 0));
        GridPane.setMargin(supervisingTAComboBox2, new Insets(0, 0, 5, 0));
        GridPane.setMargin(addButton, new Insets(0, 0, 5, 2));
        GridPane.setMargin(clearButton, new Insets(0, 0, 5, 0));
        add_editBox.getChildren().addAll(add_editLabel, add_editPane);
        add_editBox.setStyle("-fx-border-style: solid inside;"+"-fx-border-width: 2;");
        recitationPanel = new VBox();
        recitationPanel.getChildren().addAll(recitationHeaderHBox, recitationTableVBox, add_editBox);
        recitationPanel.setSpacing(5);
        recitationPanel.setPadding(new Insets(5));
        recitationPanel.setStyle("-fx-background-color:#f4b042;");
    }

    public Label getRecitationLabel() {
        return recitationLabel;
    }

    public Button getRemoveRecitationButton() {
        return removeRecitationButton;
    }

    public HBox getRecitationHeaderHBox() {
        return recitationHeaderHBox;
    }

    public Button getAddButton() {
        return addButton;
    }

    public Button getClearButton() {
        return clearButton;
    }

    public GridPane getAdd_editPane() {
        return add_editPane;
    }

    public VBox getRecitationTableVBox() {
        return recitationTableVBox;
    }
    
    public Label getAdd_EditLabel(){
        return add_editLabel;
    }
    
    public VBox getAdd_EditBox(){
        return add_editBox;
    }
    
    public static RecitationWorkspace getRecitationWorkspace(){
        if(singleton == null)
            singleton = new RecitationWorkspace();
        return singleton;
    }
    
    public VBox getRecitationPane(){
        return recitationPanel;
    }
    
    public TableView getRecitationTable(){
        return recitationTable;
    }

    public ComboBox getSupervisingTAComboBox1() {
        return supervisingTAComboBox1;
    }

    public ComboBox getSupervisingTAComboBox2() {
        return supervisingTAComboBox2;
    }
    
    

    @Override
    public void resetWorkspace() {
        
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}