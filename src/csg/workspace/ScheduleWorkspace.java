package csg.workspace;

import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import jtps.jTPS;
import csg.TAManagerApp;
import static csg.TAManagerProp.*;
import csg.data.ScheduleItem;
import java.security.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import javafx.geometry.Insets;
import javafx.util.StringConverter;
import properties_manager.PropertiesManager;

/**
 *
 * @author Janmejay Bhavsar
 */
public class ScheduleWorkspace extends AppWorkspaceComponent{
    
    TAManagerApp app;
    public jTPS jTPSObject;
    static ScheduleWorkspace singleton = null;
    Label scheduleLabel;
    Label calendarBoundariesLabel;
    Label startingMondayLabel;
    DatePicker mondayDate;
    Label endingFridayLabel;
    DatePicker fridayDate;
    Image tp1;
    HBox scheduleBox;
    HBox calendarBoundariesBox;
    HBox datePickerBox;
    VBox calendarBox;
    Label scheduleItemsLabel;
    Button removeScheduleButton;
    HBox scheduleItemsHeaderBox;
    TableView<ScheduleItem> scheduleTable;
    VBox scheduleTableBox;
    TableColumn typeColumn;
    TableColumn dateColumn;
    TableColumn titleColumn;
    TableColumn topicColumn;
    Label add_editScheduleLabel;
    Label typeLabel;
    ComboBox typeComboBox;
    ObservableList<String> typeList;
    Label dateLabel;
    DatePicker datePicker;
    Label timeLabel;
    TextField timeTextField;
    Label titleLabel;
    TextField titleTextField;
    Label topicLabel;
    TextField topicTextField;
    Label linkScheduleLabel;
    TextField linkScheduleTextField;
    Label criteriaLabel;
    TextField criteriaTextField;
    Button addScheduleButton;
    Button clearScheduleButton;
    GridPane add_editSchedulePane;
    VBox scheduleItemsBox;
    VBox scheduleItemsPane;
    VBox scheduleDataPanel;
    
    public ScheduleWorkspace(){
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        scheduleLabel = new Label(props.getProperty(SCHEDULE_HEADER.toString()));
        calendarBoundariesLabel = new Label(props.getProperty(CALENDAR_BOUNDARIES_TEXT.toString()));
        startingMondayLabel = new Label(props.getProperty(STARTING_MONDAY_TEXT.toString()));
        mondayDate = new DatePicker();
        endingFridayLabel = new Label(props.getProperty(ENDING_FRIDAY_TEXT.toString()));
        fridayDate = new DatePicker();
        scheduleBox = new HBox();
        scheduleBox.getChildren().add(scheduleLabel);
        calendarBoundariesBox = new HBox();
        calendarBoundariesBox.getChildren().add(calendarBoundariesLabel);
        datePickerBox = new HBox();
        datePickerBox.getChildren().addAll(startingMondayLabel, mondayDate, endingFridayLabel, fridayDate);
        calendarBox = new VBox();
        calendarBox.getChildren().addAll(calendarBoundariesBox, datePickerBox);
        calendarBox.setSpacing(5);
        calendarBox.setStyle("-fx-border-style: solid inside;"+"-fx-border-width: 2;");
        scheduleItemsLabel = new Label(props.getProperty(SCHEDULE_ITEMS_TEXT.toString()));
        removeScheduleButton = new Button("-");
        scheduleItemsHeaderBox = new HBox();
        scheduleItemsHeaderBox.getChildren().addAll(scheduleItemsLabel, removeScheduleButton);
        scheduleItemsHeaderBox.setSpacing(5);
        scheduleTable = new TableView<ScheduleItem>();
        scheduleTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
//        ObservableList<ScheduleItem> data =
//        FXCollections.observableArrayList(
//            new ScheduleItem("Holiday", "2/9/2017", "Snow Day", ""),
//            new ScheduleItem("Lecture", "2/14/2017", "Lecture 3", "Event Programming"),
//            new ScheduleItem("Holiday", "3/13/2017", "Spring Break", ""),
//            new ScheduleItem("HW", "3/27/2017", "HW3", "UML")
//        );
//        scheduleTable.setItems(data);
        typeColumn = new TableColumn(props.getProperty(TYPE_TEXT.toString()));
        dateColumn = new TableColumn(props.getProperty(DATE_TEXT.toString()));
        titleColumn = new TableColumn(props.getProperty(TITLE_TEXT.toString()));
        topicColumn = new TableColumn(props.getProperty(TOPIC_TEXT.toString()));
        
        typeColumn.setCellValueFactory(
                new PropertyValueFactory<ScheduleItem, String>("type")
        );
        dateColumn.setCellValueFactory(
                new PropertyValueFactory<ScheduleItem, String>("date")
        );
        titleColumn.setCellValueFactory(
                new PropertyValueFactory<ScheduleItem, String>("title")
        );
        topicColumn.setCellValueFactory(
                new PropertyValueFactory<ScheduleItem, String>("topic")
        );
        scheduleTable.getColumns().addAll(typeColumn, dateColumn, titleColumn, topicColumn);
        scheduleTableBox = new VBox();
        scheduleTableBox.getChildren().addAll(scheduleItemsHeaderBox, scheduleTable);
        
        scheduleTable.setMinWidth(500);
        add_editScheduleLabel = new Label(props.getProperty(ADD_EDIT_TEXT.toString()));
        typeLabel = new Label(props.getProperty(TYPE_TEXT.toString()));
        typeList = FXCollections.observableArrayList("Holiday", "Recitation", "Lecture", "HW");
        typeComboBox = new ComboBox(typeList);
        dateLabel = new Label(props.getProperty(DATE_TEXT.toString()));
        datePicker = new DatePicker();
        datePicker.setConverter(new StringConverter<LocalDate>()
{
    private DateTimeFormatter dateTimeFormatter=DateTimeFormatter.ofPattern("MM/dd/yyyy");

    @Override
    public String toString(LocalDate localDate)
    {
        if(localDate==null)
            return "";
        return dateTimeFormatter.format(localDate);
    }

    @Override
    public LocalDate fromString(String dateString)
    {
        if(dateString==null || dateString.trim().isEmpty())
        {
            return null;
        }
        return LocalDate.parse(dateString,dateTimeFormatter);
    }
});
        timeLabel = new Label(props.getProperty(TIME_TEXT.toString()));
        timeTextField = new TextField();
        titleLabel = new Label(props.getProperty(TITLE_TEXT.toString()));
        titleTextField = new TextField();
        topicLabel = new Label(props.getProperty(TOPIC_TEXT.toString()));
        topicTextField = new TextField();
        linkScheduleLabel = new Label(props.getProperty(LINK_TEXT.toString()));
        linkScheduleTextField = new TextField();
        criteriaLabel = new Label(props.getProperty(CRITERIA_TEXT.toString()));
        criteriaTextField = new TextField();
        addScheduleButton = new Button(props.getProperty(ADD_UPDATE_BUTTON_TEXT.toString()));
        clearScheduleButton = new Button(props.getProperty(CLEAR_BUTTON_TEXT.toString()));
        add_editSchedulePane = new GridPane();
        add_editSchedulePane.add(add_editScheduleLabel, 0, 3);
        add_editSchedulePane.add(typeLabel, 0, 4);
        add_editSchedulePane.add(typeComboBox, 1, 4);
        add_editSchedulePane.add(dateLabel, 0, 5);
        add_editSchedulePane.add(datePicker, 1, 5);
        add_editSchedulePane.add(timeLabel, 0, 6);
        add_editSchedulePane.add(timeTextField, 1, 6);
        add_editSchedulePane.add(titleLabel, 0, 7);
        add_editSchedulePane.add(titleTextField, 1, 7);
        add_editSchedulePane.add(topicLabel, 0, 8);
        add_editSchedulePane.add(topicTextField, 1, 8);
        add_editSchedulePane.add(linkScheduleLabel, 0, 9);
        add_editSchedulePane.add(linkScheduleTextField, 1, 9);
        add_editSchedulePane.add(criteriaLabel, 0, 10);
        add_editSchedulePane.add(criteriaTextField, 1, 10);
        add_editSchedulePane.add(addScheduleButton, 0, 11);
        add_editSchedulePane.add(clearScheduleButton, 1, 11);
        GridPane.setMargin(typeLabel, new Insets(0, 0, 10, 3));
        GridPane.setMargin(typeComboBox, new Insets(0, 0, 10, 0));
        GridPane.setMargin(dateLabel, new Insets(0, 0, 10, 3));
        GridPane.setMargin(datePicker, new Insets(0, 0, 10, 0));
        GridPane.setMargin(timeLabel, new Insets(0, 0, 10, 3));
        GridPane.setMargin(timeTextField, new Insets(0, 0, 10, 0));
        GridPane.setMargin(titleLabel, new Insets(0, 0, 10, 3));
        GridPane.setMargin(titleTextField, new Insets(0, 0, 10, 0));
        GridPane.setMargin(topicLabel, new Insets(0, 0, 10, 3));
        GridPane.setMargin(topicTextField, new Insets(0, 0, 10, 0));
        GridPane.setMargin(linkScheduleLabel, new Insets(0, 0, 10, 3));
        GridPane.setMargin(linkScheduleTextField, new Insets(0, 0, 10, 0));
        GridPane.setMargin(criteriaLabel, new Insets(0, 0, 10, 3));
        GridPane.setMargin(criteriaTextField, new Insets(0, 0, 10, 0));
        scheduleItemsBox = new VBox();
        scheduleItemsBox.getChildren().addAll(scheduleTableBox, add_editSchedulePane);
        scheduleItemsBox.setStyle("-fx-border-style: solid inside;"+"-fx-border-width: 2;");
        scheduleDataPanel = new VBox();
        scheduleDataPanel.getChildren().addAll(scheduleBox, calendarBox, scheduleItemsBox);
        scheduleDataPanel.setSpacing(5);
        scheduleDataPanel.setPadding(new Insets(5));
        scheduleDataPanel.setStyle("-fx-background-color:#f4b042;");
    }

    public Label getScheduleLabel() {
        return scheduleLabel;
    }

    public HBox getScheduleBox() {
        return scheduleBox;
    }

    public VBox getCalendarBox() {
        return calendarBox;
    }

    public Button getAddScheduleButton() {
        return addScheduleButton;
    }

    public Button getClearScheduleButton() {
        return clearScheduleButton;
    }
    public GridPane getAdd_editSchedulePane(){
        return add_editSchedulePane;
    }
    
    public Button getRemoveScheduleButton(){
        return removeScheduleButton;
    }
    
    public Label getCalendarBoundariesLabel(){
        return calendarBoundariesLabel;
    }
    
    public HBox getCalendarBoundariesBox(){
        return calendarBoundariesBox;
    }
    
    public Label getAdd_EditScheduleLabel(){
        return add_editScheduleLabel;
    }
    
    public Label getScheduleItemsLabel(){
        return scheduleItemsLabel;
    }
    
    public HBox getScheduleItemsHeaderBox(){
        return scheduleItemsHeaderBox;
    }
    
    public DatePicker getStartingMondayPicker(){
        return mondayDate;
    }
    
    public DatePicker getEndingFridayPicker(){
        return fridayDate;
    }
    
    public static ScheduleWorkspace getScheduleWorkspace(){
        if(singleton == null)
            singleton = new ScheduleWorkspace();
        return singleton;
    }
    
    public VBox getScheduleDataPane(){
        return scheduleDataPanel;
    }
    
    public TableView getScheduleTable(){
        return scheduleTable;
    }

    
    @Override
    public void resetWorkspace() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
