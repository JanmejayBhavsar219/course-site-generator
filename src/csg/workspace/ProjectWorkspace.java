package csg.workspace;

import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import csg.TAManagerApp;
import static csg.TAManagerProp.*;
import csg.data.Student;
import csg.data.Team;
import javafx.geometry.Insets;
import javafx.scene.control.ColorPicker;
import properties_manager.PropertiesManager;

/**
 *
 * @author Janmejay Bhavsar
 */
public class ProjectWorkspace extends AppWorkspaceComponent{
    TAManagerApp app;
    static ProjectWorkspace singleton = null;
    Label projectsLabel;
    HBox projectsHeaderBox;
    Label teamsLabel;
    Button removeTeamButton;
    HBox teamHeaderBox;
    TableView teamTable;
    TableColumn nameColumn;
    TableColumn colorColumn;
    TableColumn textColorColumn;
    TableColumn linkColumn;
    Label add_editTeamLabel;
    Label nameLabel;
    TextField nameTextField;
    Label colorLabel;
    ColorPicker colorPicker;
    Label textColorLabel;
    ColorPicker textColorPicker;
    Label linkLabel;
    TextField linkTextField;
    Button addTeamButton;
    Button clearTeamButton;
    GridPane add_editTeamPane;
    VBox teamsPane;
    Label studentsLabel;
    Button removeStudentButton;
    HBox studentHeaderBox;
    TableView studentTable;
    TableColumn firstNameColumn;
    TableColumn lastNameColumn;
    TableColumn teamColumn;
    TableColumn roleColumn;
    Label add_editStudentLabel;
    Label firstNameLabel;
    TextField firstNameTextField;
    Label lastNameLabel;
    TextField lastNameTextField;
    Label teamLabel;
    ComboBox teamComboBox;
    Label roleLabel;
    TextField roleTextField;
    Button addStudentButton;
    Button clearStudentButton;
    GridPane add_editStudentPane;
    VBox studentsPane;
    VBox projectPanel;
    
    public ProjectWorkspace(){
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        projectsLabel = new Label(props.getProperty(PROJECT_HEADER.toString()));
        projectsHeaderBox = new HBox();
        projectsHeaderBox.getChildren().add(projectsLabel);
        teamsLabel = new Label(props.getProperty(TEAMS_TEXT.toString()));
        removeTeamButton = new Button("-");
        teamHeaderBox = new HBox();
        teamHeaderBox.getChildren().addAll(teamsLabel, removeTeamButton);
        teamHeaderBox.setSpacing(5);
        teamTable = new TableView<Team>();
        teamTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        ObservableList<Team> data =
        FXCollections.observableArrayList(
            new Team("Atomic Comics", "552211", "ffffff", "http://atomiccomic.com"),
            new Team("C4 Comics", "235399", "ffffff", "http://c4-comics.appshot.com")
        );
        teamTable.setItems(data);
        nameColumn = new TableColumn(props.getProperty(NAME_TEXT.toString()));
        colorColumn = new TableColumn(props.getProperty(COLOR_TEXT.toString()));
        textColorColumn = new TableColumn(props.getProperty(TEXT_COLOR_TEXT.toString()));
        linkColumn = new TableColumn(props.getProperty(LINK_TEXT.toString()));
        nameColumn.setCellValueFactory(
                new PropertyValueFactory<Team, String>("name")
        );
        colorColumn.setCellValueFactory(
                new PropertyValueFactory<Team, String>("color")
        );
        textColorColumn.setCellValueFactory(
                new PropertyValueFactory<Team, String>("textColor")
        );
        linkColumn.setCellValueFactory(
                new PropertyValueFactory<Team, String>("link")
        );
        teamTable.getColumns().addAll(nameColumn, colorColumn, textColorColumn, linkColumn);
        add_editTeamLabel = new Label(props.getProperty(ADD_EDIT_TEXT.toString()));
        nameLabel = new Label(props.getProperty(NAME_TEXT.toString()));
        nameTextField = new TextField();
        colorLabel = new Label(props.getProperty(COLOR_TEXT.toString()));
        colorPicker = new ColorPicker();
        colorPicker.setMinHeight(25);
        textColorLabel = new Label(props.getProperty(TEXT_COLOR_TEXT.toString()));
        textColorPicker = new ColorPicker();
        textColorPicker.setMinHeight(25);
        linkLabel = new Label(props.getProperty(LINK_TEXT.toString()));
        linkTextField = new TextField();
        addTeamButton = new Button(props.getProperty(ADD_UPDATE_BUTTON_TEXT.toString()));
        clearTeamButton = new Button(props.getProperty(CLEAR_BUTTON_TEXT.toString()));
        add_editTeamPane = new GridPane();
        add_editTeamPane.add(add_editTeamLabel, 0, 0);
        add_editTeamPane.add(nameLabel, 0, 1);
        add_editTeamPane.add(nameTextField, 1, 1);
        add_editTeamPane.add(colorLabel, 0, 2);
        add_editTeamPane.add(colorPicker, 1, 2);
        add_editTeamPane.add(textColorLabel, 2, 2);
        add_editTeamPane.add(textColorPicker, 3, 2);
        add_editTeamPane.add(linkLabel, 0, 3);
        add_editTeamPane.add(linkTextField, 1, 3);
        add_editTeamPane.add(addTeamButton, 0, 4);
        add_editTeamPane.add(clearTeamButton, 1, 4);
        GridPane.setMargin(nameLabel, new Insets(0, 0, 10, 3));
        GridPane.setMargin(nameTextField, new Insets(0, 0, 10, 0));
        GridPane.setMargin(colorLabel, new Insets(0, 0, 10, 3));
        GridPane.setMargin(colorPicker, new Insets(0, 0, 10, 0));
        GridPane.setMargin(textColorLabel, new Insets(0, 0, 10, 15));
        GridPane.setMargin(textColorPicker, new Insets(0, 20, 10, 15));
        GridPane.setMargin(linkLabel, new Insets(0, 0, 10, 3));
        GridPane.setMargin(linkTextField, new Insets(0, 0, 10, 0));
        GridPane.setMargin(addTeamButton, new Insets(0, 0, 10, 3));
        GridPane.setMargin(clearTeamButton, new Insets(0, 0, 10, 0));
        teamsPane = new VBox();
        teamsPane.getChildren().addAll(teamHeaderBox, teamTable, add_editTeamPane);
        teamsPane.setStyle("-fx-border-style: solid outside;"+"-fx-border-width: 2;");
        studentsLabel = new Label(props.getProperty(STUDENT_TEXT.toString()));
        removeStudentButton = new Button("-");
        studentHeaderBox = new HBox();
        studentHeaderBox.getChildren().addAll(studentsLabel, removeStudentButton);
        studentHeaderBox.setSpacing(5);
        studentTable = new TableView();
        studentTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        ObservableList<Student> data1 =
        FXCollections.observableArrayList(
            new Student("Beau", "Brummell", "Atomic Comics", "Lead Designer"),
            new Student("Jane", "Doe", "C4 Comics", "Lead Programmer"),
            new Student("Noonian", "Soong", "Atomic Comics", "Data Designer")
        );
        studentTable.setItems(data1);
        firstNameColumn = new TableColumn(props.getProperty(FIRST_NAME_TEXT.toString()));
        lastNameColumn = new TableColumn(props.getProperty(LAST_NAME_TEXT.toString()));
        teamColumn = new TableColumn(props.getProperty(TEAM_TEXT.toString()));
        roleColumn = new TableColumn(props.getProperty(ROLE_TEXT.toString()));
        firstNameColumn.setCellValueFactory(
                new PropertyValueFactory<Student, String>("firstName")
        );
        lastNameColumn.setCellValueFactory(
                new PropertyValueFactory<Student, String>("lastName")
        );
        teamColumn.setCellValueFactory(
                new PropertyValueFactory<Student, String>("team")
        );
        roleColumn.setCellValueFactory(
                new PropertyValueFactory<Student, String>("role")
        );
        studentTable.getColumns().addAll(firstNameColumn, lastNameColumn, teamColumn, roleColumn);
        add_editStudentLabel = new Label(props.getProperty(ADD_EDIT_TEXT.toString()));
        firstNameLabel = new Label(props.getProperty(FIRST_NAME_TEXT.toString()));
        firstNameTextField = new TextField();
        lastNameLabel = new Label(props.getProperty(LAST_NAME_TEXT.toString()));
        lastNameTextField = new TextField();
        teamLabel = new Label(props.getProperty(TEAM_TEXT.toString()));
        teamComboBox = new ComboBox();
        roleLabel = new Label(props.getProperty(ROLE_TEXT.toString()));
        roleTextField = new TextField();
        addStudentButton = new Button(props.getProperty(ADD_UPDATE_BUTTON_TEXT.toString()));
        clearStudentButton = new Button(props.getProperty(CLEAR_BUTTON_TEXT.toString()));
        add_editStudentPane = new GridPane();
        add_editStudentPane.add(add_editStudentLabel, 0, 0);
        add_editStudentPane.add(firstNameLabel, 0, 1);
        add_editStudentPane.add(firstNameTextField, 1, 1);
        add_editStudentPane.add(lastNameLabel, 0, 2);
        add_editStudentPane.add(lastNameTextField, 1, 2);
        add_editStudentPane.add(teamLabel, 0, 3);
        add_editStudentPane.add(teamComboBox, 1, 3);
        add_editStudentPane.add(roleLabel, 0, 4);
        add_editStudentPane.add(roleTextField, 1, 4);
        add_editStudentPane.add(addStudentButton, 0, 5);
        add_editStudentPane.add(clearStudentButton, 1, 5);
        GridPane.setMargin(firstNameLabel, new Insets(0, 0, 10, 3));
        GridPane.setMargin(firstNameTextField, new Insets(0, 0, 10, 0));
        GridPane.setMargin(lastNameLabel, new Insets(0, 0, 10, 3));
        GridPane.setMargin(lastNameTextField, new Insets(0, 0, 10, 0));
        GridPane.setMargin(teamLabel, new Insets(0, 0, 10, 3));
        GridPane.setMargin(teamComboBox, new Insets(0, 0, 10, 0));
        GridPane.setMargin(roleLabel, new Insets(0, 0, 10, 3));
        GridPane.setMargin(roleTextField, new Insets(0, 0, 10, 0));
        GridPane.setMargin(addStudentButton, new Insets(0, 0, 10, 3));
        GridPane.setMargin(clearStudentButton, new Insets(0, 0, 10, 0));
        studentsPane = new VBox();
        studentsPane.getChildren().addAll(studentHeaderBox, studentTable, add_editStudentPane);
        studentsPane.setStyle("-fx-border-style: solid inside;"+"-fx-border-width: 2;");
        projectPanel = new VBox();
        projectPanel.getChildren().addAll(projectsHeaderBox, teamsPane, studentsPane);
        projectPanel.setSpacing(5);
        projectPanel.setPadding(new Insets(5));
        projectPanel.setStyle("-fx-background-color:#f4b042;");
    }

    public Label getProjectsLabel() {
        return projectsLabel;
    }
    
    public HBox getProjectsHeaderBox(){
        return projectsHeaderBox;
    }

    public Label getTeamsLabel() {
        return teamsLabel;
    }

    public Button getRemoveTeamButton() {
        return removeTeamButton;
    }

    public Button getAddTeamButton() {
        return addTeamButton;
    }

    public Button getClearTeamButton() {
        return clearTeamButton;
    }

    public GridPane getAdd_editTeamPane() {
        return add_editTeamPane;
    }
    
    public HBox getTeamHeaderBox(){
        return teamHeaderBox;
    }
    
    public Label getAdd_EditTeamLabel(){
        return add_editTeamLabel;
    }

    public VBox getTeamsPane() {
        return teamsPane;
    }

    public Label getStudentsLabel() {
        return studentsLabel;
    }
    
    public HBox getStudentHeaderBox(){
        return studentHeaderBox;
    }
    
    public Label getAdd_EditStudentLabel(){
        return add_editStudentLabel;
    }

    public Button getRemoveStudentButton() {
        return removeStudentButton;
    }
    
    public ComboBox getTeamComboBox(){
        return teamComboBox;
    }

    public Button getAddStudentButton() {
        return addStudentButton;
    }

    public Button getClearStudentButton() {
        return clearStudentButton;
    }

    public GridPane getAdd_editStudentPane() {
        return add_editStudentPane;
    }

    public VBox getStudentsPane() {
        return studentsPane;
    }
    
    
    public static ProjectWorkspace getProjectWorkspace(){
        if(singleton == null){
            singleton = new ProjectWorkspace();
        }
        return singleton;
    }

    public VBox getProjectDataPane(){
        return projectPanel;
    }
    
    public TableView getTeamTable(){
        return teamTable;
    }
    
    public TableView getStudentTable(){
        return studentTable;
    }
    
    @Override
    public void resetWorkspace() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
