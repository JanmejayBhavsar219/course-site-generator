/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.data.Recitation;
import csg.data.Student;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import csg.data.TAData;
import csg.data.TeachingAssistant;
import csg.data.Team;

/**
 *
 * @author khurr
 */
public class AddStudent_Transaction implements jTPS_Transaction {

    private final String studFirstName;
    private final String studLastName;
    private final String studTeam;
    private final String studRole;
    private final TAData data;

    public AddStudent_Transaction(String firstName, String lastName, String team, String role, TAData taData) {
        studFirstName = firstName;
        studLastName = lastName;
        studTeam = team;
        studRole = role;        
        data = taData;
    }

    @Override
    public void doTransaction() {  //Control Y 
        data.addStudent(studFirstName, studLastName, studTeam, studRole);
        //Collections.sort(data.getTeachingAssistants());
        System.out.println("doTransaction");
    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("undo Transaction");
        ObservableList<Student> studList = data.getStudents();
        for (Student s : studList) {
            if (studFirstName.equals(s.getFirstName())) {
                studList.remove(s);
                return;
            }

        }
        // data.removeTA(taName);

    }

}
