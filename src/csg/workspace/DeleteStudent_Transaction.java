/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.data.Student;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import jtps.jTPS_Transaction;
import csg.data.TAData;
import csg.data.TeachingAssistant;
import csg.data.Team;

/**
 *
 * @author khurr
 */
public class DeleteStudent_Transaction implements jTPS_Transaction {

    private final Student student;
    private final TAData data;

    public DeleteStudent_Transaction(Student student, TAData data) {
        this.data = data;
        this.student = student; 
    }

    @Override
    public void doTransaction() {  //control Y  
        String firstName = student.getFirstName();
        data.removeStudent(firstName);
}

    @Override
    public void undoTransaction() {
        data.addStudent(student.getFirstName(), student.getLastName(), student.getTeam(), student.getRole());
    }
}
