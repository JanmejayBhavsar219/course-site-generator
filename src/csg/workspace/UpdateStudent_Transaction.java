/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;


import jtps.jTPS_Transaction;
import csg.TAManagerApp;
import csg.data.Recitation;
import csg.data.Student;
import csg.data.TAData;
import csg.data.TeachingAssistant;
import csg.data.Team;
import javafx.scene.paint.Color;


/**
 *
 * @author khurr
 */
public class UpdateStudent_Transaction implements jTPS_Transaction {

    private final String oldFirstName;
    private final String newFirstName;
    private final String oldLastName;
    private final String newLastName;
    private final String oldTeam;
    private final String newTeam;
    private final String oldRole;
    private final String newRole;
    private final TAData taData;
    private final Student student;
    private final TAManagerApp app; 
    private final TAWorkspace transWorkspace; 
    private final ProjectWorkspace pWorkspace;

    public UpdateStudent_Transaction(String orgFirstName, String firstName, String orgLastName, String lastName, String orgTeam, String team, String orgRole, String role, TAData data, TAManagerApp taApp, TAWorkspace workspace) {
        oldFirstName = orgFirstName;
        newFirstName = firstName;
        oldLastName = orgLastName;
        newLastName = lastName;
        oldTeam = orgTeam;
        newTeam = team;
        oldRole = orgRole;
        newRole = role;
        taData = data;
        student = data.getStudent(orgFirstName);
        app=taApp; 
        transWorkspace=workspace; 
        pWorkspace = ProjectWorkspace.getProjectWorkspace();
    }

    @Override
    public void doTransaction() {  //Control Y 
        System.out.println("updateTA doTransaction ");
        Student s = taData.getStudent(oldFirstName);
        s.setFirstName(newFirstName);
        s.setLastName(newLastName);
        s.setTeam(newTeam);
        s.setRole(newRole);
        student.setFirstName(newFirstName);                        // MOVED TO TRANSACTION CASE 
        student.setLastName(newLastName);
        student.setTeam(newTeam);
        student.setRole(newRole);
        pWorkspace.firstNameTextField.setText(newFirstName);
        pWorkspace.lastNameTextField.setText(newLastName);
        pWorkspace.teamComboBox.getSelectionModel().select(newTeam);
        pWorkspace.roleTextField.setText(newRole);
       // transWorkspace.taTable.refresh();

    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("updateTA undoTransaction ");
        Student s = taData.getStudent(newFirstName);
        s.setFirstName(oldFirstName);
        s.setLastName(oldLastName);
        s.setTeam(oldTeam);
        s.setRole(oldRole);
        student.setFirstName(oldFirstName);                        // MOVED TO TRANSACTION CASE 
        student.setLastName(oldLastName);
        student.setTeam(oldTeam);
        student.setRole(oldRole);
        pWorkspace.firstNameTextField.setText(oldFirstName);
        pWorkspace.lastNameTextField.setText(oldLastName);
        pWorkspace.teamComboBox.getSelectionModel().select(oldTeam);
        pWorkspace.roleTextField.setText(oldRole);
        //transWorkspace.taTable.refresh();

    }

}
