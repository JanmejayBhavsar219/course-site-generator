/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.data.Recitation;
import csg.data.ScheduleItem;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import csg.data.TAData;
import csg.data.TeachingAssistant;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author khurr
 */
public class AddSchedule_Transaction implements jTPS_Transaction {

    private final String sType;
    private final String sDate;
    private final String sTime;
    private final String sTitle;
    private final String sTopic;
    private final String sLink;
    private final String sCriteria;
    private final TAData data;

    public AddSchedule_Transaction(String type, String date, String time, String title, String topic, String link, String criteria, TAData taData) {
        sType = type;
        sDate = date;
        sTime = time;
        sTitle = title;
        sTopic = topic;
        sLink = link;  
        sCriteria = criteria;
        data = taData;
    }

    @Override
    public void doTransaction() {  try {
        //Control Y
        if(sType.equals("HW"))
        data.addHWScheduleItem(sType, sDate, sTitle, sTopic, sTime, sCriteria);
        else
            data.addScheduleItem(sType, sDate, sTitle, sTopic);
        
        } catch (ParseException ex) {
            Logger.getLogger(AddSchedule_Transaction.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Collections.sort(data.getTeachingAssistants());
        System.out.println("doTransaction");
    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("undo Transaction");
        ObservableList<ScheduleItem> sList = data.getSchedules();
        for (ScheduleItem si : sList) {
            if (sTitle.equals(si.getTitle())) {
                sList.remove(si);
                return;
            }

        }
        // data.removeTA(taName);

    }

}
